<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;  
use app\models\PurchasingSuppliers;

/**
 * PurchasingSuppliersSearch represents the model behind the search form about `app\models\PurchasingSuppliers`.
 */
class PurchasingSuppliersSearch extends PurchasingSuppliers
{
    /** 
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_number','status','arxiv'], 'integer'],
            [['date_creation_application','ptichka','delivery_time', 'manufacture', 'execution_phase', 'notes_order', 'documents'], 'safe'],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class 
        return Model::scenarios(); 
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params , $post,$archive)
    { 
        $query = PurchasingSuppliers::find();
        if($archive == 0) $query = PurchasingSuppliers::find();
        if($archive == 1)  $query = PurchasingSuppliers::find()->where(['arxiv'=>0]);
        if($archive == 2)  $query = PurchasingSuppliers::find()->where(['arxiv'=>1]);
        if($post == null)
        {
            $post['PurchasingSuppliersSearch']['search_manufacture'] = null;
            $post['PurchasingSuppliersSearch']['search_notes_order'] = null;
            $post['PurchasingSuppliersSearch']['search_delivery_time'] = null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'date_creation_application' => $this->date_creation_application,
            'order_number' => $this->order_number,
            'delivery_time' => $this->delivery_time,
            'status' => $this->status,
            'arxiv' => $this->arxiv,
            'ptichka' => $this->ptichka,
        ]);

        $query->andFilterWhere(['like', 'manufacture',$post['PurchasingSuppliersSearch']['search_manufacture']])
            ->andFilterWhere(['like', 'execution_phase', $this->execution_phase])
            ->andFilterWhere(['like', 'order_number', $post['PurchasingSuppliersSearch']['search_notes_order']])
            ->andFilterWhere(['like', 'delivery_time', $post['PurchasingSuppliersSearch']['search_delivery_time']])
            ->andFilterWhere(['like', 'documents', $this->documents]);

        return $dataProvider;
    }
}
