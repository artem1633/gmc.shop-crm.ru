<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;  

/**
 * This is the model class for table "organizer".
 * 
 * @property int $id
 * @property string $name
 * @property int $who
 * @property int $whom
 * @property int $status
 * @property string $date
 * @property string $description
 *
 * @property Users $who0
 * @property Users $whom0
 */
class Organizer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $search_who;
    public $search_whom;
    public $search_status;
    public static function tableName()
    {
        return 'organizer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['who', 'whom', 'status','verification'], 'integer'],
            [['date'], 'safe'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['who'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['who' => 'id']],
            [['whom'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['whom' => 'id']],
            [['status','whom','description','name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'who' => 'Создатель',
            'whom' => 'Kому',
            'status' => 'Статус',
            'date' => 'Дата',
            'description' => 'Описание',
            'dobavit'=>'Перенести задачи',
        ];
    }
    public function beforeSave($insert)
    {
        if($this->date != null ) $this->date = \Yii::$app->formatter->asDate($this->date, 'php:Y-m-d');
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getWho0()
    {
        return $this->hasOne(Users::className(), ['id' => 'who']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhom0()
    {
        return $this->hasOne(Users::className(), ['id' => 'whom']);
    }
    public function getUsers()
    {
        return ArrayHelper::map(Users::find()->all(), 'id', 'fio');
    }

    public function getStatus()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Выполнено',],
            ['id' => '2', 'title' => 'Нe выполнено',],
        ],
        'id', 'title');
    }
    public function getVerification()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Новый',],
            ['id' => '2', 'title' => 'В работе',],
            ['id' => '3', 'title' => 'Готово',],
        ],
        'id', 'title');
    }
}
