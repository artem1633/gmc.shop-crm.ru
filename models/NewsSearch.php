<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;

/**
 * NewsSearch represents the model behind the search form about `app\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc 
     */
    public function rules()
    {
        return [
            [['id','arxiv','user_id', 'fixing'], 'integer'],
            [['header', 'text_post', 'image', 'attached_file', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * 
     * @return ActiveDataProvider
     */
    public function search($params,$archive)
    {
        if($archive == 0) $query = News::find()->where(['>=', 'date', date('Y-01-01')])->orderBy(['fixing' => SORT_DESC]);
        else $query = News::find()->where(['<', 'date', date('Y-01-01')])->orderBy(['fixing' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       // $query->orderBy('id DESC');
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'user_id' => $this->user_id,
            'fixing' => $this->fixing,
        ]);

        $query->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'text_post', $this->text_post])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'attached_file', $this->attached_file]);

        return $dataProvider;
    }
}
