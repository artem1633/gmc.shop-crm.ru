<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Organizer; 

/**
 * OrganizerSearch represents the model behind the search form about `app\models\Organizer`.
 */
class OrganizerSearch extends Organizer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'who', 'whom', 'status'], 'integer'],
            [['name', 'date', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params , $post)
    {
         $query = Organizer::find(); 
        if($post == null)
        {
            $post['OrganizerSearch']['search_who'] = null;
            $post['OrganizerSearch']['search_whom'] = null;
            $post['OrganizerSearch']['search_status'] = null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->orderBy('date DESC');
        $query->andFilterWhere([
            'id' => $this->id,
            'who' => $this->who,
            'whom' => $this->whom,
            'status' => $this->status,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'who', $post['OrganizerSearch']['search_who']])
            ->andFilterWhere(['like', 'whom', $post['OrganizerSearch']['search_whom']])
            ->andFilterWhere(['like', 'status', $post['OrganizerSearch']['search_status']])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
