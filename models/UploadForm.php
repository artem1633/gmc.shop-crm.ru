<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Group;
use app\models\CustomerStatuses;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    public $type;
    /**
     * @return array the validation rules.
     */
    public $group;
    public $status;
    public function rules() 
    {
        return [
            [['file'], 'file'],
            [['group', 'status'], 'integer'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
        ];
    }

    public function getGroup()
    {
        $group = Group::find()->all();
        return ArrayHelper::map($group, 'id', 'name');
    }

    public function getStatuses()
    {
        $customer_statuses = CustomerStatuses::find()->all();
        return ArrayHelper::map($customer_statuses, 'id', 'name');
    }
    public function getType()
    {
        $group = manual\TypeParts::find()->all();
        return ArrayHelper::map($group, 'id', 'name');
    }
}
?>