<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\validators\EmailValidator;
use yii\validators\NumberValidator;
use yii\helpers\Html;


class Call extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'next_contact'], 'safe'],
            [['contact_id', 'users_id', 'status_call_id', 'client_id'], 'integer'],
            [['description'], 'string'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['status_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallStatus::className(), 'targetAttribute' => ['status_call_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['contact_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'contact_id' => 'С кем',
            'description' => 'Описание',
            'users_id' => 'Создатель',
            'next_contact' => 'Следующий контакт',
            'status_call_id' => 'Статус',
            'client_id' => 'Клиент',
        ];
    }
 
    public function beforeSave($insert)
    {  
        if ($this->isNewRecord)
        {
            $this->users_id = \Yii::$app->user->identity->id;
        }
        if($this->date != null) $this->date = date('Y-m-d H:i:s');
        if($this->next_contact != null) $this->next_contact = \Yii::$app->formatter->asDate($this->next_contact, 'php:Y-m-d');
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        $client = Client::findOne($this->client_id);
        $call = Call::find()->where(['client_id' => $client->id])->orderBy(['next_contact' => SORT_DESC])->one();
        if($call != null)
        {
            $client->poslednego_zvonka = $client->sleduyushchego_zvonka;
            $client->sleduyushchego_zvonka = $call->next_contact;
            $client->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusCall()
    {
        return $this->hasOne(CallStatus::className(), ['id' => 'status_call_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    public function getContactList($id)
    {
        $contact = Contacts::find()->where(['client_id' => $id])->all();
        $result = [];
        foreach ($contact as $value) {
            $result [] = [
                'id' => $value->id,
                'title' => 'Фио : ' . $value->fio . '; Email :' . $value->email . '; Телефон :' . $value->phone,
            ];
        }
        return ArrayHelper::map($result,'id', 'title');
    }

    public function getStatusList()
    {
        return ArrayHelper::map(CallStatus::find()->all(), 'id', 'name');
    }
    public function getValuesFromExcel($fileName)
    {
        $filePath = 'uploads/' . $fileName;
        $csvData = \PHPExcel_IOFactory::load($filePath);
        $csvData = $csvData->getActiveSheet()->toArray(null, true, true, false);

        $count = 0;
        foreach ($csvData as $a) {
            if ($count==0) {
                $count ++;
                continue;
            }

            $client = new Call();
            //$client->good_id = $a[0];
            $client->date = date('Y-m-d H:i:s');
            $client->working_title = $a[1];
            $contact = Contacts::find()->where(['fio' => $a[2] ])->one();
            if($contact == null){
                $new_contact = new Contacts();
                $new_contact->fio = $a[2];
                $new_contact->save();
                $client->contact_id = $new_contact->id;
            }
            else $client->contact_id = $contact->id;

            $client->description = $a[3];
            $client->next_contact = $a[4];   
            $client->save();
            $count++;
        }

    }
}
