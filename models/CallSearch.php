<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Call; 

/**
 * CallSearch represents the model behind the search form about `app\models\Call`.
 */
class CallSearch extends Call
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contact_id', 'users_id', 'status_call_id', 'client_id'], 'integer'],
            [['date', 'description', 'next_contact'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Call::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'contact_id' => $this->contact_id,
            'users_id' => $this->users_id,
            'next_contact' => $this->next_contact,
            'status_call_id' => $this->status_call_id,
            'client_id' => $this->client_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function searchByClient($params,$id)
    {
        $client = Client::findOne($id);
        if(Yii::$app->user->identity->type == 1) $query = Call::find()->where(['client_id' => $id]);
        else
        {
            //if()
            $query = Call::find()->where([ 'client_id' => $id, 'users_id' => Yii::$app->user->identity->id ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->orderBy('id DESC');
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'contact_id' => $this->contact_id,
            'users_id' => $this->users_id,
            'next_contact' => $this->next_contact,
            'status_call_id' => $this->status_call_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
