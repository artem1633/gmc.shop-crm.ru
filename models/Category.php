<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public function beforeDelete()
    {
        $products = Products::find()->where(['category_id' => $this->id])->all();
        foreach ($products as $product) {
            $product->category_id = null;
            $product->save();
        }

        return parent::beforeDelete();
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }
    public function getStocks()
    {
        return $this->hasMany(Stock::className(), ['category_id' => 'id']);
    }
}
