<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_setting".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $dashboard_info Визуальный вид рабочего стола
 *
 * @property Users $user
 */
class UserSetting extends \yii\db\ActiveRecord
{
    const DASHBOARD_INFO_CHAT = 0;
    const DASHBOARD_INFO_ORGANIZER = 1;
    const DASHBOARD_INFO_CLIENTS = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'dashboard_info'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'dashboard_info' => 'Тип рабочего стола',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
