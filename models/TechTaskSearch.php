<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TechTask;

/**
 * TechTaskSearch represents the model behind the search form about `app\models\TechTask`.
 */
class TechTaskSearch extends TechTask
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tender_id', 'product_id', 'count'], 'integer'],
            [['price', 'sum'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TechTask::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tender_id' => $this->tender_id,
            'product_id' => $this->product_id,
            'count' => $this->count,
            'price' => $this->price,
            'sum' => $this->sum,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
