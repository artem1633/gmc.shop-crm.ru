<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logistics;

/**
 * LogisticsSearch represents the model behind the search form about `app\models\Logistics`. 
 */
class LogisticsSearch extends Logistics
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'stage'], 'integer'],
            [['date_application_created', 'date_application', 'company_name', 'other'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * 
     * @return ActiveDataProvider
     */
    public function search($params,$archive)
    {
        $query = LogisticsSearch::find();
        if($archive == 0) $query = LogisticsSearch::find();
        if($archive == 1)  $query = LogisticsSearch::find()->where(['arxiv'=>0]);
        if($archive == 2)  $query = LogisticsSearch::find()->where(['arxiv'=>1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_application_created' => $this->date_application_created,
            'date_application' => $this->date_application,
            'order_id' => $this->order_id,
            'stage' => $this->stage,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'other', $this->other]);

        return $dataProvider;
    }
}
