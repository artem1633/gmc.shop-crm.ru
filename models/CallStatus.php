<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "call_status".
 *
 * @property int $id
 * @property string $name
 */
class CallStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название ',
        ];
    }
    public function getCalls()
    {
        return $this->hasMany(Call::className(), ['status_call_id' => 'id']);
    }
}
