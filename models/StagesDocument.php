<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stages_document".
 *
 * @property int $id
 * @property string $name
 */
class StagesDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stages_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название ',
        ];
    }
    public function getDocumentations()
    {
        return $this->hasMany(Documentation::className(), ['stage_id' => 'id']);
    }
}
