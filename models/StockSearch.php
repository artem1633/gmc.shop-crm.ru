<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Stock;

/**
 * StockSearch represents the model behind the search form about `app\models\Stock`.
 */
class StockSearch extends Stock
{
    /** 
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'availability_stock', 'presence_branch', 'reserve', 'order', 'category_id','tovar_id'], 'integer'],
            [['cost_goods'], 'number'],
            [['chekt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'availability_stock' => $this->availability_stock,
            'presence_branch' => $this->presence_branch,
            'reserve' => $this->reserve,
            'order' => $this->order,
            'chekt' => $this->chekt,
            'cost_goods' => $this->cost_goods,
            'tovar_id' => $this->tovar_id,
            'category_id' => $this->category_id,
        ]);

        return $dataProvider;
    }
}
