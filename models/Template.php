<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property int $id
 * @property string $name
 * @property string $key
 * @property string $text
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $number;
    public $data_validate;
    public $dalnomer;
    public $plant_number;
    public $date_calibr;
    public $data_poverki;
    public $basiz;
    public $reg_number;
    public $koleso_dorojnoe;
    public $lineyka;
    public $length;
    public $width;
    public $predel;
    public $nivelir;
    public $complekt;
    public $ugol;
    public $naklon;
    public $shtrix;
    public $stansiya;
    public $km_xod;
    public $pribor_verticalnogo_proyekta;
    public $luch;
    public $reyka_dorojnaya;
    public $uklon;
    public $zalojenie;
    public $reyka;
    public $ruletka;
    public $deleniya;
    public $taxeometr;
    public $gorizontal;
    public $vertical;
    public $teodolit;
    public $uroven;
    public $pogreshnost;
    public $shtangensirkul;

    public static function tableName()
    {
        return 'template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['name', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименовие',
            'key' => 'Ключ',
            'text' => 'Текст',

            'number' => 'СЕРТИФИКАТ КАЛИБРОВКИ №',
            'data_validate' => 'Действительно до',
            'koleso_dorojnoe' => 'Колесо дорожное',
            'lineyka' => 'Линейка',
            'dalnomer' => 'Дальномер',
            'plant_number' => 'Заводской номер',
            'date_calibr' => 'Дата калибровки',
            'data_poverki' => 'Дата поверки',
            'basiz' => 'Средняя погрешность измерения базисов',
            'reg_number' => 'рег. №',
            'length' => 'Средняя погрешность измерения базисов (погрешность)',
            'width' => 'Средняя погрешность измерения базисов (длина)',
            'predel' => 'Погрешность дециметровых делений ',
            'nivelir' => 'Средство измерений (нивелир)',
            'complekt' => 'Средство измерений (комплект)',
            'ugol' => 'Угол I',
            'naklon' => 'Недокомпенсация на 1’ наклона',
            'shtrix' => 'Погрешность штрихов реек',
            'stansiya' => 'На станции',
            'km_xod' => 'На 1 км хода',
            'pribor_verticalnogo_proyekta' => 'Прибор вертикального проектирования',
            'luch' => 'Отклонение луча',
            'reyka_dorojnaya' => 'Рейка дорожная',
            'uklon' => 'Уклон',
            'zalojenie' => 'Заложений',
            'reyka' => 'Рейка',
            'ruletka' => 'Рулетка',
            'deleniya' => 'Средняя погрешность метровых делений',
            'taxeometr' => 'Тахеометр',
            'gorizontal' => 'Горизонтальных углов',
            'vertical' => 'Вертикальных углов',
            'teodolit' => 'Теодолит',
            'uroven' => 'Уровень',
            'pogreshnost' => 'Погрешность измерений',
            'shtangensirkul' => 'Штангенциркуль',
        ];
    }

    public function getDateDescription($date)
    {
        if($date == null) return '';
        else
        {
            $day = date('d', strtotime($date) );
            $month = date('m', strtotime($date) );
            $year = date('Y', strtotime($date) );

            return '«'.$day.'» '.Template::getMonth($month).' ' . $year .' ';
        }
    }

    public function getMonth($month)
    {
        if($month == 1) return 'январь';
        if($month == 2) return 'февраль';
        if($month == 3) return 'март';
        if($month == 4) return 'апрель';
        if($month == 5) return 'май';
        if($month == 6) return 'июнь';
        if($month == 7) return 'июль';
        if($month == 8) return 'август';
        if($month == 9) return 'сентябрь';
        if($month == 10) return 'октябрь';
        if($month == 11) return 'ноябрь';
        if($month == 12) return 'декабрь';
    }
}
