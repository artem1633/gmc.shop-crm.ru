<?php

namespace app\models;

use Yii; 
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property string $manufacturer
 * @property string $name_goods
 * @property int $availability_stock
 * @property int $presence_branch
 * @property int $reserve
 * @property int $order
 * @property string $product_image
 * @property string $short_description
 * @property string $specifications 
 * @property string $full_description
 * @property double $cost_goods
 *
 * @property Orders $order0
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $other_file;
    public $manufacturer_id;
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['availability_stock', 'presence_branch', 'reserve', 'order', 'tovar_id'], 'integer','min'=>0],
            [['cost_goods'], 'number'],
            [['category_id'], 'integer'],
            [['other_file'], 'file'],
            [['chekt'], 'safe'],
            [['availability_stock'], 'required'],
            [['tovar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['tovar_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Kатегории товаров',
            'availability_stock' => 'Наличие на складе',
            'presence_branch' => 'Наличие в филиале',
            'reserve' => 'Резерв ',
            'order' => 'Заказ ',
            'chekt' => 'БУ',
            'cost_goods' => 'Себестоимость',
            'other_file' => 'Изображение товара',
            'tovar_id' => 'Товар',
            'manufacturer_id' => 'Производитель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTovar()
    {
        return $this->hasOne(Products::className(), ['id' => 'tovar_id']);
    }

    public function getProductsList($category)
    {
        return ArrayHelper::map(Products::find()->where(['category_id' => $category])->all(), 'id', 'name');
    }

    public function getManufacturerList()
    {
        return ArrayHelper::map(Manufacturer::find()->all(), 'id', 'name');
    }
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    public function getCategories()
    {
        return ArrayHelper::map(Category::find()->all(), 'id', 'name');
    }
    public function getAllProduct()
    {
        return ArrayHelper::map(Products::find()->all(), 'id', 'name');
    }
}
