<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * @inheritdoc 
     */
    public function rules()
    {
        return [
            [['id', 'who_users_id', 'to_users_id', 'status', 'client_id'], 'integer'],
            [['date', 'time', 'title', 'description', 'attachment', 'terms'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tasks = Tasks::find()->where(['!=', 'status', 1])->all();
        $result = [];
        foreach ($tasks as $task) {
            if( $task->who_users_id == Yii::$app->user->identity->id || $task->to_users_id == Yii::$app->user->identity->id ) $result [] = $task->id;
        }
        if(Yii::$app->user->identity->type != 1) $query = Tasks::find()->where(['id' => $result])->andWhere(['!=', 'status', 1]);
        else $query = Tasks::find()->where(['!=', 'status', 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'time' => $this->time,
            'terms' => $this->terms,
            'status' => $this->status,
            'client_id' => $this->client_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'attachment', $this->attachment]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchArchive($params)
    {
        $tasks = Tasks::find()->where(['status' => 1])->all();
        $result = [];
        foreach ($tasks as $task) {
            if( $task->who_users_id == Yii::$app->user->identity->id || $task->to_users_id == Yii::$app->user->identity->id ) $result [] = $task->id;
        }
        if(Yii::$app->user->identity->type != 1) $query = Tasks::find()->where(['id' => $result])->andWhere(['status' => 1]);
        else $query = Tasks::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'time' => $this->time,
            'terms' => $this->terms,
            'status' => $this->status,
            'client_id' => $this->client_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'attachment', $this->attachment]);

        return $dataProvider;
    }

    public function searchByClient($params,$id)
    {
        $type = Users::find()->where(['id' =>\Yii::$app->user->identity->id])->one();
        if($type->type==2){$tasks = Tasks::find()->where(['who_users_id' =>$type->id])->all(); }
        else{$tasks = Tasks::find()->all();}
        $result = [];
        foreach ($tasks as $task) {
            if( $task->who_users_id == Yii::$app->user->identity->id || $task->to_users_id == Yii::$app->user->identity->id ) $result [] = $task->id;
        }
        if(Yii::$app->user->identity->type == 2) $query = Tasks::find()->where(['id' => $result])->andWhere(['client_id' => $id])->andWhere([ 'status' => [2,3,4] ]);
        else $query = Tasks::find()->where(['client_id' => $id])->andWhere([ 'status' => [2,3,4] ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'time' => $this->time,
            'terms' => $this->terms,
            'status' => $this->status,
            'client_id' => $this->client_id,
        ]);


        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'attachment', $this->attachment]);

        return $dataProvider;
    }
}
