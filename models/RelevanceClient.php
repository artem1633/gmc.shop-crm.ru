<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "relevance_client".
 *
 * @property int $id
 * @property string $name
 * @property string $color
 */
class RelevanceClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relevance_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название ',
            'color' => 'Цвет',
        ];
    }
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['relevance' => 'id']);
    }
}
