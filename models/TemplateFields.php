<?php

namespace app\models;

use Yii;
use yii\base\Model;

class TemplateFields extends Model
{

    public function getDalnomerKalibrovka()
    {
        $dalnomer_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Дальномер {dalnomer}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 280px;">
            методика калибровки</td>
            <td style="border-bottom: 1px solid #000; width: 660px; text-align: center; font-size: 23px; ">с руководством по эксплуатаци</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">лента измерительная 3 разряда № 040105, рег. № 3.2.АВШ.0002.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Средняя погрешность измерения базисов</td>
      <td style="height: 30px; border: 1px solid #000;">{basiz} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';
        return $dalnomer_kalibrovka;
    }

    public function getDalnomerPoverka()
    {
        $dalnomer_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Дальномер {dalnomer} </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; "> руководством по эксплуатации </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: left; font-size: 20px;  ">лента измерительная 3 разряда № 040105 и рег.№ 3.2.АВШ 0002.2016 </td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">работоспособен</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Средняя погрешность измерения базисов:</td>
      <td style="height: 30px; border: 1px solid #000;">{basiz} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';
        return $dalnomer_poverka;
    }

    public function getKolesoDorojnoeKalibrovka()
    {
        $koleso_dorojnoe_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Колесо дорожное  {koleso_dorojnoe}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 480px;">
            калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: center; font-size: 23px; ">МИ1780-87 и руководства по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">лента измерительная 3 разряда № 040105, рег. № 3.2.АВШ.0002.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Средняя погрешность измерения базисов:</td>
      <td style="height: 30px; border: 1px solid #000;">{length} см на {width} м</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $koleso_dorojnoe_kalibrovka;
    }

    public function getKolesoDorojnoePoverka()
    {
        $koleso_dorojnoe_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Колесо дорожное  {koleso_dorojnoe} </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: center; font-size: 20px; "> c руководством по эксплуатации </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: left; font-size: 20px;  ">лента измерительная 3 разряда № 040105 и рег.№ 3.2.АВШ 0002.2016 </td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Средняя погрешность измерения базисов:</td>
      <td style="height: 30px; border: 1px solid #000;">{length} см на {width} м</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $koleso_dorojnoe_poverka;
    }

    public function getLineykaKalibrovka()
    {
        $lineyka_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Линейка  {lineyka}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 480px;"> калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 460px; text-align: center; font-size: 23px; ">ГОСТ 427-75</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">метр штриховой 2 разряда № 0266, рег. № 3.2.АВШ.0001.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность дециметровых делений</td>
      <td style="height: 30px; border: 1px solid #000;">в пределах {predel} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';
        return $lineyka_kalibrovka;
    }

    public function getNiverilReykaPoverka()
    {
        $niveril_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Нивелир {nivelir} в комплекте с {complekt} </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: center; font-size: 20px; "> P50.2.023-2002 и МИ02-00 </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">АК-05У 2 разряда № 790094,   рег.№ 3.2.АВШ.0005.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;">и метр штриховой 2 разряда № 0266, рег. № 3.2.АВШ.0001.2016</td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные, штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Угол I</td>
      <td style="height: 30px; border: 1px solid #000;">{ugol}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Недокомпенсация на 1’ наклона</td>
      <td style="height: 30px; border: 1px solid #000;">{naklon}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность штрихов реек</td>
      <td style="height: 30px; border: 1px solid #000;">в пределах {shtrix} мм </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $niveril_poverka;
    }

    public function getNiverilKalibrovka()
    {
        $niveril_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Нивелир {nivelir}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 440px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; ">P50.2.023-2002 и рук-ва по эксплуатации </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">АК-05У 2 разряда № 790094,   рег.№ 3.2.АВШ.0005.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Угол I</td>
      <td style="height: 30px; border: 1px solid #000;">{ugol}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Недокомпенсация на 1’ наклона</td>
      <td style="height: 30px; border: 1px solid #000;">{naklon}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $niveril_kalibrovka;
    }

    public function getNiverilPoverka()
    {
        $niveril_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Нивелир  {nivelir} </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; "> P50.2.023-2002 и рук-ва по эксплуатации </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">АК-05У 2 разряда № 790094,   рег.№ 3.2.АВШ.0005.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Угол I</td>
      <td style="height: 30px; border: 1px solid #000;">{ugol}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Недокомпенсация на 1’ наклона</td>
      <td style="height: 30px; border: 1px solid #000;">{naklon}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $niveril_poverka;
    }

    public function getNiverilPoverka1()
    {
        $niveril_poverka1 = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Нивелир  {nivelir} </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; "> P50.2.023-2002 и рук-ва по эксплуатации </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">АК-05У 2 разряда № 790094,   рег.№ 3.2.АВШ.0005.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Угол I</td>
      <td style="height: 30px; border: 1px solid #000;">{ugol}”</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">СКП определения превышения:</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- на станции:</td>
      <td style="height: 30px; border: 1px solid #000;">±{stansiya} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- на 1 км хода</td>
      <td style="height: 30px; border: 1px solid #000;">{km_xod} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';
        return $niveril_poverka1;
    }

    public function getPriborVerticalnogoProyektirovanieKalibrovka()
    {
        $priborVerticalnogo = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 21px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Прибор вертикального проектирования  {pribor_verticalnogo_proyekta}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 450px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; ">P50.2.023-2002 и рук-ва по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">ЭГЕМ 2 разряда №131, рег. № 3.2.АВШ.0006.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Отклонение луча</td>
      <td style="height: 30px; border: 1px solid #000;">{luch}</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $priborVerticalnogo;
    }

    public function getPustishkaKalibrovka()
    {
        $pustishkaKalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; "></span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до « &nbsp; &nbsp; &nbsp;» &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;201 &nbsp; &nbsp; &nbsp; г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 21px; border-bottom: 1px solid #000; width: 800px; text-align: center; "></td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; "></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 450px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; "></td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  "></td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  "></td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">« &nbsp; &nbsp; &nbsp;» &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;201 &nbsp; &nbsp; &nbsp;г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Отклонение луча</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">« &nbsp;  &nbsp; &nbsp;» &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;201 &nbsp; &nbsp; &nbsp;г.</td>
       </tr>
    </table>  
      ';

        return $pustishkaKalibrovka;
    }

    public function getPustishkaPoverka()
    {
        $pustishkaPoverka =' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; "></span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до « &nbsp;  &nbsp; &nbsp;» &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;201 &nbsp; &nbsp; &nbsp;г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "></td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № </td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; "></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; "></td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  "></td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  "></td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">« &nbsp;  &nbsp; &nbsp;» &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;201 &nbsp; &nbsp; &nbsp;г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">« &nbsp;  &nbsp; &nbsp;» &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;201 &nbsp; &nbsp; &nbsp;г.</td>
         </tr>
      </table>  
      ';

        return $pustishkaPoverka;
    }

    public function getReykaDorojnayaPoverka()
    {
        $reyka_dorojnaya_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">рейка дорожная {reyka_dorojnaya}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">МП1828-2002,      руководством по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">лента измерительная 3 разряда № 040105 и рег.№ 3.2.АВШ 0002.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;">штангенциркуль ШЦ-1-125 № 048623</td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">стыки  чёткие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность измерений в пределах:</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">-уклонов</td>
      <td style="height: 30px; border: 1px solid #000;">{uklon} ‰</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">-заложений</td>
      <td style="height: 30px; border: 1px solid #000;">{zalojenie} ц.д.</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $reyka_dorojnaya_poverka;
    }

    public function getReykaKalibrovka()
    {
        $reyka_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Рейка {reyka}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 440px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; ">МИ02-00 </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">метр штриховой 2 разряда № 0266, рег. № 3.2.АВШ.0001.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность дециметровых делений</td>
      <td style="height: 30px; border: 1px solid #000;">в пределах {predel} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $reyka_kalibrovka;
    }

    public function getReykaPoverka()
    {
        $reyka_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">рейка {reyka}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">МИ02-00</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">метр штриховой 2 разряда № 0266, рег. № 3.2.АВШ.0001.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность дециметровых делений</td>
      <td style="height: 30px; border: 1px solid #000;">в пределах {predel} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $reyka_poverka;
    }

    public function getRuletkaKalibrovka()
    {
        $ruletka_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Рулетка {ruletka}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 440px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; ">МИ1780-87 </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">лента измерительная 3 разряда № 040105, рег. № 3.2.АВШ.0002.201 6</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">Штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Средняя погрешность метровых делений</td>
      <td style="height: 30px; border: 1px solid #000;">{deleniya} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $ruletka_kalibrovka;
    }

    public function getRuletkaPoverka()
    {
        $ruletka_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">рулетка {ruletka}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">МИ1780-87</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">лента измерительная 3 разряда № 040105, рег. № 3.2.АВШ.0002.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">Штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Средняя погрешность метровых делений</td>
      <td style="height: 30px; border: 1px solid #000;">{deleniya} мм</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $ruletka_poverka;
    }

    public function getTaxeometrPoverka()
    {
        $taxeometr_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">Тахеометр {taxeometr}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">МИ15-03 и руководства по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">УК1 2 разряда  №074, рег. № 3.2.АВШ.0003.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">753 мм рт. ст</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Ср. кв. ПГ измерений:</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- горизонтальных углов</td>
      <td style="height: 30px; border: 1px solid #000;">±{gorizontal} ’’  </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- вертикальных углов</td>
      <td style="height: 30px; border: 1px solid #000;">±{vertical} ’’  </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- базисов</td>
      <td style="height: 30px; border: 1px solid #000;">±{basiz} мм  </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';
        return $taxeometr_poverka;
    }

    public function getTeodolitPoverka()
    {
        $teodolit_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">Теодолит  {teodolit}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">Р 50.2.024-2002 и рук-ва по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">2Т2А 2 разряда № 39565, рег. № 3.2.АВШ.0004.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С </td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Ср. кв. ПГ измерений:</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- горизонтальных углов</td>
      <td style="height: 30px; border: 1px solid #000;">±{gorizontal} ’’   </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- вертикальных углов</td>
      <td style="height: 30px; border: 1px solid #000;">±{vertical} ’’  </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $teodolit_poverka;
    }

    public function getTeodolitKalibrovka()
    {
        $teodolit_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Теодолит  {teodolit}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 440px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; ">Р 50.2.024-2002 и рук-ва по эксплуатации </td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">2Т2А 2 разряда № 39565, рег. № 3.2.АВШ.0004.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">вращения плавные</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Ср.кв. ПГ измерения:</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- горизонтальных углов</td>
      <td style="height: 30px; border: 1px solid #000;">±{gorizontal}’’  </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">- вертикальных углов</td>
      <td style="height: 30px; border: 1px solid #000;">±{vertical} ’’  </td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $teodolit_kalibrovka;

    }

    public function getUrovenKalibrovka()
    {
        $uroven_kalibrovka = '<table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 26px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СЕРТИФИКАТ КАЛИБРОВКИ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; "> Уровень   {uroven}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 440px;">калибровка проведена в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 500px; text-align: center; font-size: 23px; ">руководством по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена калибровка</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 22px;  ">ЭГЕМ 2 разряда №131, рег. № 3.2.АВШ.0006.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 16px; text-align: left; width: 100%; margin-top: 15px;"> и на основании результатов калибровки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>
<br>
<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<br>
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата калибровки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты калибровки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по калибровке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">работоспособность хорошая</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность измерений</td>
      <td style="height: 30px; border: 1px solid #000;">{pogreshnost}</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>
    <table style="margin-top: 330px; width: 100%;">
        <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
        </tr>
    </table> 

    <table style="margin-top: 50px;">
       <tr>
          <td style="padding-left: 0;">Дата калибровки &nbsp;&nbsp;</td>
          <td style="text-align: left; border-bottom: 1px solid #000;">{date_calibr}г.</td>
       </tr>
    </table>  
      ';

        return $uroven_kalibrovka;
    }

    public function getUrovenPoverka()
    {
        $uroven_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">Уровень  {uroven}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">руководством по эксплуатации</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">ЭГЕМ 2 разряда №131, рег. № 3.2.АВШ.0006.2016</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С </td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">работоспособность хорошая</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Погрешность измерений</td>
      <td style="height: 30px; border: 1px solid #000;">{pogreshnost}</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $uroven_poverka;
    }

    public function getShtangensirkulPoverka()
    {
        $shtangensirkul_poverka = ' 
      <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">{logo}</td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 21px; text-transform: uppercase;">
               <b style="font-weight: bold;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № <span style="border-bottom: 1px solid #000; width: 80px; text-align: left; font-size: 24px; ">{number}</span></b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до {data_validate}г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: center; ">Штангенциркуль   {shtangensirkul}</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. № {reg_number}</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; "></td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: center; font-size: 24px; ">{plant_number}</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 20px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 20px; ">ГОСТ 8-113-85, методика поверки</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;"></td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: center; font-size: 20px;  ">концевые меры</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; text-align:center; width: 100%;"></td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: center; font-size: 22px;  ">20°С </td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>
<br>
<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">состояние хорошее</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">штрихи четкие</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">ПГ делений</td>
      <td style="height: 30px; border: 1px solid #000;">{deleniya}</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">{data_poverki}г.</td>
         </tr>
      </table>  
      ';

        return $shtangensirkul_poverka;
    }


}
