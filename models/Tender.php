<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tender".
 *
 * @property int $id
 * @property string $datetime Дата и время
 * @property int $status 
 * @property string $number
 * @property string $procedure_name
 * @property string $customer
 * @property integer $customer_id
 * @property string $term
 * @property int $users_id
 * @property int $client_id
 * @property int $curator_id
 * @property string $link Ссылка
 * @property string $description Описание
 * @property string $comment Комментарий
 * @property integer $form_action_id Форма заявления
 * @property integer $placement_id Площадка размещения
 * @property integer $company_name_id Наименование компании
 * @property integer $auction_price_id Цена по аукционам
 * @property double $provision Обеспечение
 * @property double $provision_contract Обеспечение по контракту
 * @property integer $waiting_stage_id Статус ожидания
 * @property double $take_price Цена по чем покупали
 * @property string $time_delivery Дата поставки
 * @property string $date_payment Срок оплаты
 *
 * @property TechTask[] $techTasks
 * @property TenderFile[] $tenderFiles
 * @property Client $client 
 * @property Users $users
 */
class Tender extends \yii\db\ActiveRecord
{
    const STATUS_ACTUAL = '1';
    const STATUS_ARCHIVE = '2';
    const STATUS_WORKING = '3';
    const STATUS_WAIT = '4';
    const STATUS_WON = '5';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tender';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status','curator_id'], 'required'],
            [['datetime', 'term', 'date_payment'], 'safe'],
            [['status', 'users_id', 'client_id', 'form_action_id', 'placement_id', 'auction_price_id', 'waiting_stage_id', 'curator_id', 'time_delivery'], 'integer'],
            [['provision', 'provision_contract', 'take_price', 'customer_id'], 'number'],
            [['number', 'procedure_name', 'customer', 'link'], 'string', 'max' => 255],
            [['description', 'comment'], 'string'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['curator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['curator_id' => 'id']],
            [['form_action_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormAction::className(), 'targetAttribute' => ['form_action_id' => 'id']],
            [['placement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Placement::className(), 'targetAttribute' => ['placement_id' => 'id']],
            [['company_name_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyNames::className(), 'targetAttribute' => ['company_name_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Срок окончания подачи заявки',
            'status' => 'Статус',
            'number' => 'Номер',
            'procedure_name' => 'Название процедуры',
            'customer' => 'Заказчик',
            'customer_id' => 'Клиент',
            'term' => 'Дата аукциона (Итоги)',
            'users_id' => 'Кто',
            'client_id' => 'Куратор',
            'curator_id' => 'Куратор',
            'link' => 'Ссылка',
            'description' => 'Описание',
            'comment' => 'Комментарий',
            'form_action_id' => 'Форма заявления',
            'placement_id' => 'Площадка размещения',
            'company_name_id' => 'От кого играем',
            'provision' => 'Обеспечение заявки',
            'provision_contract' => 'Обеспечение контракта',
            'auction_price_id' => 'Цена по аукционам',
            'waiting_stage_id' => 'Статус (ожидания)',
            'take_price' => 'Начальная максимальная цена',
//            'date_delivery' => 'Срок поставки',
            'time_delivery' => 'Срок поставки',
            'date_payment' => 'Срок оплаты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechTasks()
    {
        return $this->hasMany(TechTask::className(), ['tender_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
     public function getClients()
    {
        return ArrayHelper::map(Client::find()->all(), 'id', 'working_title');
    }

    public function getFilesPaths()
    {
        return ArrayHelper::getColumn(TenderFile::find()->where(['tender_id' => $this->id])->all(), 'path');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }
    public function beforeSave($insert)
    {  
        if($this->datetime != null) $this->datetime = \Yii::$app->formatter->asDate($this->datetime, 'php:Y-m-d H:i:s');
        if($this->term != null) $this->term = \Yii::$app->formatter->asDate($this->term, 'php:Y-m-d H:i:s');
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenderFiles()
    {
        return $this->hasMany(TenderFile::className(), ['tender_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Актуальная',],
            ['id' => '2', 'title' => 'Архив',],
            ['id' => '3', 'title' => 'Разбор тендеров',],
            ['id' => '4', 'title' => 'Подались ждем',],
            ['id' => '5', 'title' => 'Выиграли',],
        ],
        'id', 'title');
    }
}
