<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;

/**
 * ClientSearch represents the model behind the search form about `app\models\Client`.
 */
class ClientSearch extends Client
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'curator', 'relevance','city_id'], 'integer'],
            [['working_title', 'name_floor', 'name_juice', 'tin_cat','kpp', 'jur_address', 'phiz_address', 'bic', 'bank', 'r_c', 'k_c', 'ogrn', 'head_of', 'comment', 'contacts','poslednego_zvonka','sleduyushchego_zvonka'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        $dataProvider = new ActiveDataProvider([ 
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
            'sort' => [
                'defaultOrder' => [
                    'sleduyushchego_zvonka' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        //$query->orderBy('id DESC');
        $query->joinWith('contacts');
        $query->joinWith('curator0');
        $query->joinWith('relevance0');
        $query->joinWith('city');
        $query->andFilterWhere([
            'id' => $this->id,
            'relevance' => $this->relevance,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'working_title', $this->working_title])
            ->andFilterWhere(['like', 'name_floor', $this->name_floor])
            ->andFilterWhere(['like', 'name_juice', $this->name_juice])
            ->andFilterWhere(['like', 'tin_cat', $this->tin_cat])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'city_id', $this->city_id])
            ->andFilterWhere(['like', 'poslednego_zvonka', $this->poslednego_zvonka])
            ->andFilterWhere(['like', 'jur_address', $this->jur_address])
            ->andFilterWhere(['like', 'sleduyushchego_zvonka', $this->sleduyushchego_zvonka])
            ->andFilterWhere(['like', 'phiz_address', $this->phiz_address])
            ->andFilterWhere(['like', 'bic', $this->bic])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'r_c', $this->r_c])
            ->andFilterWhere(['like', 'k_c', $this->k_c])
            ->andFilterWhere(['like', 'ogrn', $this->ogrn])
            ->andFilterWhere(['like', 'head_of', $this->head_of])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'contacts', $this->contacts]);


        if($this->curator == null) {
            $this->curator = Yii::$app->user->getId();
        } else if($this->curator != -1) {
            $query->andWhere(['curator' => $this->curator]);
        }

        return $dataProvider;
    }
}
