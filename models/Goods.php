<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "goods".
 *
 * @property int $id
 * @property int $order_id
 * @property string $name
 * @property int $count 
 * @property double $cost
 * @property double $sum
 * @property int $nds_protsent
 * @property int $product_id
 * @property double $nds_sum
 * @property double $total_sum
 * @property string $provider Поставщик
 * @property string $provide_time Срок поставки
 * @property string $contract_time Срок по договору
 * @property double $price_one_client Цена за ед. для заказчика
 * @property double $price_client Цена для заказчика
 * @property double $price_for_us Стоимость товара для нас
 * @property double $profit Прибыль
 * @property double $sum_additional Сумма, на которую надо доказать
 * @property Orders $order
 */
class Goods extends \yii\db\ActiveRecord
{ 
    /**
     * @inheritdoc
     */ 
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc 
     */
    public function rules()
    {
        return [
            [['product_id',], 'required'],
            [['order_id','nds_protsent','product_id'], 'integer'],
            [['cost', 'sum', 'nds_sum', 'total_sum', 'price_one_client', 'price_client', 'price_for_us', 'profit', 'sum_additional'], 'number'],
            [['name', 'provider', 'provide_time', 'contract_time'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['count'], 'integer', 'min' => 0, 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'name' => 'Номенклатура',
            'count' => 'Количество',
            'cost' => 'Цена',
            'sum' => 'Сумма',
            'nds_protsent' => '% НДС',
            'nds_sum' => 'НДС',
            'total_sum' => 'Всего',
            'product_id' => 'Название продукта',
            'provider' => 'Поставщик',
            'provide_time' => 'Срок поставки',
            'contract_time' => 'Срок по договору',
            'price_one_client' => 'Цена за ед. для заказчика',
            'price_client' => 'Цена для заказчика',
            'price_for_us' => 'Стоимость товара для нас',
            'profit' => 'Прибыль',
            'sum_additional' => 'Сумма, на которую надо доказать'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
    public function getProducts()
    {
        return ArrayHelper::map(Products::find()->all(), 'id', 'name');
    }
}
