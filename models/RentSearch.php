<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rent;

/**
 * RentSearch represents the model behind the search form about `app\models\Rent`.
 */
class RentSearch extends Rent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['start_date_lease', 'name_equipment', 'serial_number', 'name_lessee_company'], 'safe'],
            [['stage_transaction','client_id', 'term_lease'], 'integer'],
            [['rent_price', 'paid_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class 
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$archive)
    {
        if($archive == 0) $query = Rent::find();
        if($archive == 1)  $query = Rent::find()->where(['>=', 'start_date_lease', date('Y-01-01')]);
        if($archive == 2)  $query = Rent::find()->where(['<', 'start_date_lease', date('Y-01-01')]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'start_date_lease' => $this->start_date_lease,
            'term_lease' => $this->term_lease,
            'rent_price' => $this->rent_price,
            'paid_amount' => $this->paid_amount,
        ]);

        $query->andFilterWhere(['like', 'name_equipment', $this->name_equipment])
            ->andFilterWhere(['like', 'serial_number', $this->serial_number])
            ->andFilterWhere(['like', 'name_lessee_company', $this->name_lessee_company])
            ->andFilterWhere(['like', 'client_id', $this->client_id])
            ->andFilterWhere(['like', 'stage_transaction', $this->stage_transaction]);

        return $dataProvider;
    }
}
