<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ListUsed;

/**
 * ListUsedSearch represents the model behind the search form about `app\models\ListUsed`.
 */
class ListUsedSearch extends ListUsed
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','product_id'], 'integer'],
            [['manufacturer_country_production', 'model', 'description', 'production_year', 'owner_contact_inform', 'photo_equipment'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ListUsed::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'production_year' => $this->production_year,
            'price' => $this->price,
            'product_id' => $this->product_id,
        ]);

        $query->andFilterWhere(['like', 'manufacturer_country_production', $this->manufacturer_country_production])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'owner_contact_inform', $this->owner_contact_inform])
            ->andFilterWhere(['like', 'photo_equipment', $this->photo_equipment]);

        return $dataProvider;
    }
}
