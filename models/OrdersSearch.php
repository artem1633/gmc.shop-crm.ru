<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider; 
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'status', 'discount', 'base_id', 'responsible'], 'integer'],
            [['created_date', 'payment_date', 'comment'], 'safe'],
            [['total_sum', 'total_nds_sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied 
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->orderBy('id DESC');
        $query->joinWith('client');
        $query->joinWith('responsible0');
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'responsible' => $this->responsible,
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'created_date' => $this->created_date,
            'payment_date' => $this->payment_date,
            'status' => $this->status,
            'discount' => $this->discount,
            'base_id' => $this->base_id,
            'responsible' => $this->responsible,
            'total_sum' => $this->total_sum,
            'total_nds_sum' => $this->total_nds_sum,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
    public function searchByClient($params,$id)
    {
        $query = Orders::find()->where(['client_id' => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'created_date' => $this->created_date,
            'payment_date' => $this->payment_date,
            'status' => $this->status,
            'discount' => $this->discount,
            'base_id' => $this->base_id,
            'responsible' => $this->responsible,
            'total_sum' => $this->total_sum,
            'total_nds_sum' => $this->total_nds_sum,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
