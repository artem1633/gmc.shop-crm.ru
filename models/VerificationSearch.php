<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Verification;

/**
 * VerificationSearch represents the model behind the search form about `app\models\Verification`.
 */
class VerificationSearch extends Verification
{
    /**
     * @inheritdoc
     */
    public function rules() 
    {
        return [
            [['id', 'devices_id', 'client_id','status'], 'integer'],
            [['application_date', 'check_date', 'number', 'series','documents','date_next_check'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Verification::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('client');
        $query->joinWith('devices');
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'devices_id' => $this->devices_id,
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'application_date' => $this->application_date,
            'check_date' => $this->check_date,
            'devices_id' => $this->devices_id,
            'date_next_check' => $this->date_next_check,
            'client_id' => $this->client_id,
            'status' => $this->status,
            'documents' => $this->documents,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'series', $this->series]);

        return $dataProvider;
    }
    public function searchByClient($params,$id)
    {
        $query = Verification::find()->where(['client_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith('client');
        $query->joinWith('devices');
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'devices_id' => $this->devices_id,
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'application_date' => $this->application_date,
            'check_date' => $this->check_date,
            'devices_id' => $this->devices_id,
            'date_next_check' => $this->date_next_check,
            'client_id' => $this->client_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'series', $this->series]);

        return $dataProvider;
    }
}
