<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tender;

/**
 * TenderSearch represents the model behind the search form about `app\models\Tender`.
 */
class TenderSearch extends Tender
{
    /**
     * @inheritdoc 
     */ 
    public function rules()
    {
        return [
            [['id', 'status', 'users_id', 'client_id'], 'integer'],
            [['datetime', 'number', 'procedure_name', 'customer', 'term', 'link', 'description', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tender::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->orderBy('id DESC');
        $query->joinWith('client');
        $query->joinWith('users');
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'users_id' => $this->users_id,
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'datetime' => $this->datetime,
            'status' => $this->status,
            'term' => $this->term,
 
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'procedure_name', $this->procedure_name])
            ->andFilterWhere(['like', 'customer', $this->customer]);

        return $dataProvider;
    }
    public function searchByClient($params,$id)
    {
        $query = Tender::find()->where(['client_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'datetime' => $this->datetime,
            'status' => $this->status,
            'term' => $this->term,
            'users_id' => $this->users_id,
            'client_id' => $this->client_id,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'procedure_name', $this->procedure_name])
            ->andFilterWhere(['like', 'customer', $this->customer]);

        return $dataProvider;
    }
}
