<?php

namespace app\models;

use Yii; 
use yii\base\Model; 
use yii\data\ActiveDataProvider;
use app\models\Documentation;

/**
 * DocumentationSearch represents the model behind the search form about `app\models\Documentation`.
 */
class DocumentationSearch extends Documentation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'stage_id', 'users_id','client_id','documents_type_id'], 'integer'],
            [['date', 'number', 'ownership', 'working', 'file'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider 
     */
    public function search($params)
    {
        $query = Documentation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->orderBy('id DESC');
        $query->joinWith('client');
        $query->joinWith('users');
        $query->joinWith('stage');
        $query->joinWith('documentstype');
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'users_id' => $this->users_id,
            'stage_id' => $this->stage_id,
            'documents_type_id' => $this->documents_type_id,
        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'ownership', $this->ownership])
            ->andFilterWhere(['like', 'working', $this->working])
            ->andFilterWhere(['like', 'file', $this->file]);

        return $dataProvider;
    }
    public function searchByClient($params,$id)
    {
        $query = Documentation::find()->where(['client_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'stage_id' => $this->stage_id,
            'users_id' => $this->users_id,
            'client_id' => $this->client_id,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'ownership', $this->ownership])
            ->andFilterWhere(['like', 'working', $this->working])
            ->andFilterWhere(['like', 'file', $this->file]);

        return $dataProvider;
    }
}
