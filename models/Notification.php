<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property string $table_name
 * @property int $field
 * @property int $status
 * @property int $user_id
 * @property string $description
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field', 'status', 'user_id'], 'integer'],
            [['table_name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Table Name',
            'field' => 'Field',
            'status' => 'Status',
            'user_id' => 'User ID',
            'description' => 'Description',
        ];
    }

    public function setNotification($table, $field, $user, $description)
    {
        $notification = new Notification();
        $notification->table_name = $table;
        $notification->field = $field;
        $notification->status = 0;
        $notification->user_id = $user;
        $notification->description = $description;
        $notification->save();
    }
}
