<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%trade}}`.
 */
class m191002_194751_add_auction_price_id_column_to_trade_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'auction_price_id', $this->integer()->comment('Цена по аукционам'));

        $this->createIndex(
            'idx-tender-auction_price_id',
            'tender',
            'auction_price_id'
        );

        $this->addForeignKey(
            'fk-tender-auction_price_id',
            'tender',
            'auction_price_id',
            'auction_price',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender-auction_price_id',
            'tender'
        );

        $this->dropIndex(
            'idx-tender-auction_price_id',
            'tender'
        );

        $this->dropColumn('tender', 'auction_price_id');
    }
}
