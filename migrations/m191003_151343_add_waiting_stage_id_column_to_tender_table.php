<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191003_151343_add_waiting_stage_id_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'waiting_stage_id', $this->integer()->comment('Статус ожидания'));

        $this->createIndex(
            'idx-tender-waiting_stage_id',
            'tender',
            'waiting_stage_id'
        );

        $this->addForeignKey(
            'fk-tender-waiting_stage_id',
            'tender',
            'waiting_stage_id',
            'waiting_stages',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender-waiting_stage_id',
            'tender'
        );

        $this->dropIndex(
            'idx-tender-waiting_stage_id',
            'tender'
        );

        $this->dropColumn('tender', 'waiting_stage_id');
    }
}
