<?php

use yii\db\Migration;

/**
 * Handles the creation of table `goods`.
 */
class m180419_184650_create_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('goods', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'name' => $this->string(255),
            'count' => $this->integer(),
            'cost' => $this->float(),
            'sum' => $this->float(),
            'nds_protsent' => $this->integer(),
            'nds_sum' => $this->float(),
            'total_sum' => $this->float(),
        ]);

        $this->createIndex('idx-goods-order_id', 'goods', 'order_id', false);
        $this->addForeignKey("fk-goods-order_id", "goods", "order_id", "orders", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-goods-order_id','goods');
        $this->dropIndex('idx-goods-order_id','goods');
        
        $this->dropTable('goods');
    }
}
