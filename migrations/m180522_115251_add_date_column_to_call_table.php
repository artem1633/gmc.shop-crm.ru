<?php

use yii\db\Migration;

/**
 * Handles adding date to table `call`.
 */
class m180522_115251_add_date_column_to_call_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('call', 'date', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('call', 'date');
    }
}
