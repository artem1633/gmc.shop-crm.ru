<?php

use yii\db\Migration;

/**
 * Handles the creation of table `stages_document`.
 */
class m180407_003802_create_stages_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('stages_document', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('stages_document');
    }
}
