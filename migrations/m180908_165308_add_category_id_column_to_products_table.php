<?php

use yii\db\Migration;

/**
 * Handles adding category_id to table `products`.
 */
class m180908_165308_add_category_id_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'category_id', $this->integer());

        $this->createIndex('idx-products-category_id', 'products', 'category_id', false);
        $this->addForeignKey("fk-products-category_id", "products", "category_id", "category", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-products-category_id','products');
        $this->dropIndex('idx-products-category_id','products');
        
        $this->dropColumn('products', 'category_id');
    }
}
