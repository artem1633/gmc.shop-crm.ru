<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `columns_from_manufactures`.
 */
class m180605_072428_drop_columns_from_manufactures_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-tovars-manufacturer_id','tovars');
        $this->dropIndex('idx-tovars-manufacturer_id','tovars');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('idx-tovars-manufacturer_id', 'tovars', 'manufacturer_id', false);
        $this->addForeignKey("fk-tovars-manufacturer_id", "tovars", "manufacturer_id", "manufacturer", "id");
    }
}
