<?php

use yii\db\Migration;

/**
 * Handles adding documents to table `verification`.
 */
class m180921_151508_add_documents_column_to_verification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('verification', 'documents', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('verification', 'documents');
    }
}
