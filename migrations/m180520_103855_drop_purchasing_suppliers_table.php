<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `purchasing_suppliers`.
 */
class m180520_103855_drop_purchasing_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('purchasing_suppliers', 'delivery_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('purchasing_suppliers', 'delivery_time', $this->integer()->notNull());
    }
}
