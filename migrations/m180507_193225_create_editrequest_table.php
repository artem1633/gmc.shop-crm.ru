<?php

use yii\db\Migration;

/**
 * Handles the creation of table `editrequest`.
 */
class m180507_193225_create_editrequest_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('editrequest', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'date_cr' => $this->date(),
            'user_id'=> $this->integer(),
            'status' => $this->integer(),
            'description' => $this->text(),
        ]);
        $this->createIndex('idx-editrequest-user_id', 'editrequest', 'user_id', false);
        $this->addForeignKey("fk-editrequest-user_id", "editrequest", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-editrequest-user_id','editrequest');
        $this->dropIndex('idx-editrequest-user_id','editrequest');

        $this->dropTable('editrequest');
    }
}
