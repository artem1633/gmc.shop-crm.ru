<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180419_184257_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'created_date' => $this->datetime(),
            'payment_date' => $this->date(),
            'status' => $this->integer(),
            'discount' => $this->integer(),
            'base_id' => $this->integer(),
            'comment' => $this->text(),
            'responsible' => $this->integer(),
            'total_sum' => $this->float(),
            'total_nds_sum' => $this->float(),
        ]);

        $this->createIndex('idx-orders-client_id', 'orders', 'client_id', false);
        $this->addForeignKey("fk-orders-client_id", "orders", "client_id", "client", "id");

        $this->createIndex('idx-orders-responsible', 'orders', 'responsible', false);
        $this->addForeignKey("fk-orders-responsible", "orders", "responsible", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-orders-client_id','orders');
        $this->dropIndex('idx-orders-client_id','orders');

        $this->dropForeignKey('fk-orders-responsible','orders');
        $this->dropIndex('idx-orders-responsible','orders');


        $this->dropTable('orders');
    }
}
