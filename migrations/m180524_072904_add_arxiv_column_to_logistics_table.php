<?php

use yii\db\Migration;

/**
 * Handles adding arxiv to table `logistics`.
 */
class m180524_072904_add_arxiv_column_to_logistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistics', 'arxiv', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('logistics', 'arxiv');
    }
}
