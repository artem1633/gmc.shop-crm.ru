<?php

use yii\db\Migration;

/**
 * Handles adding date_creation to table `client`.
 */
class m180521_175306_add_date_creation_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'date_creation', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'date_creation');
    }
}
