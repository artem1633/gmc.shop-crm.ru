<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `news`.
 */
class m180520_112535_drop_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->dropColumn('news', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         $this->addColumn('news', 'date', $this->integer()->notNull());
    }
}
