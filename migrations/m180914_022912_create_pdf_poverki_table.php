<?php

use yii\db\Migration;
use yii\helpers\Html;
/**
 * Handles the creation of table `pdf_poverki`.
 */
class m180914_022912_create_pdf_poverki_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pdf_poverki', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'text' => $this->text(),
        ]);

        $this->insert('pdf_poverki',[
            'id' =>1,
            'name' => 'Создать счёт',
            'text' => '
            <table style="width: 100%;">
        <tr>
            <td style="text-align: right; width: 40%;">
'.Html::img("images/logo-cu.jpg", ["style" => "width: 80px;"]).'
            </td>
            <td style="text-align: left; width: 60%;">
                  <p style="display: inline; font-size: 12px; line-height: 1;">акционерное общество</p>
                  <h2 style="display: inline; font-size: 22px; font-weight: 700; text-transform: uppercase; line-height: 2.5;
                  padding-top:5px;">
                  <b>СТРОЙИЗЫСКАНИЯ</b></h2>
            </td>
         </tr>
      </table>
 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 14px;">
               Акционерное общество по инженерно-строительным изысканиям «Стройизыскания» <br>
630009, г.Новосибирск, ул. Пролетарская, 155 <br>
Регистрационный номер в реестре аккредитации юридических лиц  № 0072

            </td>
         </tr> 
      </table>

<table style="width: 100%; margin-top: 30px;">
         <tr>
            <td style="text-align: center; line-height: 1.2; font-size: 22px; text-transform: uppercase;">
               <b style="font-weight: 500;">СВИДЕТЕЛЬСТВО О ПОВЕРКЕ № _________</b>
            </td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 45px;">
         <tr>
            <td style="text-align: right; line-height: 1.2; font-size: 16px;">
               Действительно до «________»__________________________201____г.
            </td>
         </tr>
      </table>

     <table style="width: 100%; margin-top: 35px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 260px;">
               Средство измерений</td>
            <td style="font-size: 23px; border-bottom: 1px solid #000; width: 800px; text-align: left; ">Средство измеренийСредство измеренийСредство измерений</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px;">наименование, тип, модификация, регистрационный номер в Федеральном информационном Фонде по обеспечению</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">рег. №</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">единства измерений (если в состав средств измерений входят несколько автономных блоков, то приводят их перечень и заводские номера)</td>
         </tr>
         
      </table>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%; font-size: 15px; text-align:center; ">единства измерений (если в состав средств измерений входят</td>
         </tr>
         <tr>
            <td style="text-align: center; font-size: 9px;">серия и номер клейма предыдущей калибровки (если такие серия и номер имеются)</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 320px;">
               заводской номер (номера)</td>
            <td style="border-bottom: 1px solid #000; width: 800px; text-align: left; font-size: 24px; ">Иванов Иван Иванович Иванов Иван Иванович</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 325px;">
            поверено в соответствии с</td>
            <td style="border-bottom: 1px solid #000; width: 560px; text-align: left; font-size: 23px; ">Иванов Иван ИвановичИванов Иван Иванович</td>
         </tr>
      </table>

 <p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование документа, на основании которого выполнена поверка </p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, praesentium.</td>
         </tr>
      </table>

 <table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 24px; width: 310px;">
            с применением эталонов</td>
            <td style="border-bottom: 1px solid #000; width: 760px; text-align: left; font-size: 23px;  ">Иванов Иван ИвановичИванов Иван ИвановичИванов Иван Иванович</td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 30px;">наименование, заводской номер, разряд, класс или погрешность применяемого эталона</p>

 <table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="border-bottom: 1px solid #000; width: 100%;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, praesentium.</td>
         </tr>
      </table>

<table style="width: 100%; margin-top: 15px;">
         <tr>
            <td style="text-align: left; font-size: 22px; width: 520px;">
            при следующих значениях влияющих факторов:</td>
            <td style="border-bottom: 1px solid #000; width: 400px; text-align: left; font-size: 22px;  ">
             Иванов Иван ИвановичИванов
            </td>
         </tr>
      </table>

<p style="font-size: 9px; text-align: right; width: 100%; margin-top: 0px; padding-right: 50px;">перечень влияющих факторов с указанием их значений</p>

<p style="font-size: 14px; text-align: left; width: 100%; margin-top: 15px;">и на основании результатов первичной <span style="text-decoration: underline;">(периодической)</span> поверки признано соответствующим установленным в описании типа метрологическим требованиям и пригодным к применению в сфере государственного регулирования обеспечения единства измерений.</p>

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="width: 50%; padding-left: 35px;">М.П.</td>
            <td style="width: 50%; padding-left: 35px;">Знак поверки</td>
         </tr>
      </table>

<table style="margin-top: 30px; width: 100%;">
         <tr>
            <td style="width: 55%; padding-left: 0;">Заведующий метрологической лабораторией</td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 30%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 
<p style="font-size: 9px; padding-left:60%; width: 100%; margin-top: 0px;">подпись</p>  


<table style="margin-top: 15px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">«&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;201&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;г.</td>
         </tr>
      </table>  

<table style="margin-top: 15px; width: 100%;">
         <tr>
            <td style="text-align: center; font-style: italic;">т. 8(383) 262-15-43, факс 8(383) 224-49-47, м.т. 8 913-906-93-20, е-mail: si@stiz-nsk.ru</td>
         </tr>
      </table>  

<p style="width: 100%; margin-top: 30px; text-align: center;">Результаты поверки</p>  
<p style="width: 100%; margin-top: 15px; text-align: center;">(заполняется при наличии соответствующих требований в нормативном документе по поверке)</p>  

<table style="width: 100%; border-collapse: collapse; margin-top: 15px;">
   <tr style="">
      <td style="height: 20px;text-align: center; width: 60%; border: 1px solid #000;"><b>Наименование параметров</b></td>
      <td style="height: 20px;text-align: center; width: 40%; border: 1px solid #000;"><b>Значение параметров</b></td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Внешний осмотр</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">Опробование</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
   <tr style="">
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
      <td style="height: 30px; border: 1px solid #000;">&nbsp;</td>
   </tr>
    
</table>

<table style="margin-top: 330px; width: 100%;">
         <tr>
            <td style="width: 5%; padding-left: 0;">Поверитель </td>
            <td style="width: 15%; text-align: left; border-bottom: 1px solid #000;"></td>
            <td style="width: 80%; padding-left: 35px;">
            <p style=" border-bottom: 1px solid #000;">П.И.Михеев</p></td>
         </tr>
      </table> 

<table style="margin-top: 50px;">
         <tr>
            <td style="padding-left: 0;">Дата поверки&nbsp;&nbsp;</td>
            <td style="text-align: left; border-bottom: 1px solid #000;">«&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;201&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;г.</td>
         </tr>
      </table>
            ',]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pdf_poverki');
    }
}
