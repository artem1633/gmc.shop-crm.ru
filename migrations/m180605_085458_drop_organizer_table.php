<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `organizer`.
 */
class m180605_085458_drop_organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('organizer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('organizer', [
            'id' => $this->primaryKey(),
        ]);
    }
}
