<?php

use yii\db\Migration;

/**
 * Handles adding copyprice to table `products`.
 */
class m180909_045258_add_copyprice_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'copyprice', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'copyprice');
    }
}
