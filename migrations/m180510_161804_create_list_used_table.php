<?php

use yii\db\Migration;

/**
 * Handles the creation of table `list_used`.
 */
class m180510_161804_create_list_used_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('list_used', [
            'id' => $this->primaryKey(),
            'manufacturer_country_production' => $this->string(255),
            'model' => $this->string(255),
            'description' => $this->text(),
            'production_year' => $this->date(),
            'price' => $this->float(),
            'owner_contact_inform' => $this->string(255),
            'photo_equipment' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('list_used');
    }
}
