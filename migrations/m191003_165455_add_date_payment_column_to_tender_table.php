<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191003_165455_add_date_payment_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'date_payment', $this->date()->comment('Срок оплаты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'date_payment');
    }
}
