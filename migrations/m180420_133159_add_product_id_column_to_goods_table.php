<?php

use yii\db\Migration;

/**
 * Handles adding product_id to table `goods`.
 */
class m180420_133159_add_product_id_column_to_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('goods', 'product_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('goods', 'product_id');
    }
}
