<?php

use yii\db\Migration;

/**
 * Handles adding gender_occupation to table `client`.
 */
class m180521_175044_add_gender_occupation_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'gender_occupation', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'gender_occupation');
    }
}
