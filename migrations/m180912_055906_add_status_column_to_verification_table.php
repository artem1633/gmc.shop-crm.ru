<?php

use yii\db\Migration;

/**
 * Handles adding status to table `verification`.
 */
class m180912_055906_add_status_column_to_verification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('verification', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('verification', 'status');
    }
}
