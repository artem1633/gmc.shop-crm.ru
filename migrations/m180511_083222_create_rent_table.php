<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rent`.
 */
class m180511_083222_create_rent_table extends Migration
{
    /**
     * {@inheritdoc} 
     */
    public function safeUp()
    {
        $this->createTable('rent', [
            'id' => $this->primaryKey(),
            'start_date_lease' => $this->date(),
            'name_equipment' => $this->string(255),
            'serial_number' => $this->string(255),
            'name_lessee_company' => $this->string(255),
            'term_lease' => $this->integer(),
            'rent_price' => $this->float(),
            'paid_amount' => $this->float(),
            'stage_transaction' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rent');
    }
}
