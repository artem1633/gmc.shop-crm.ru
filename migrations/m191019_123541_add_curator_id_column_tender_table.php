<?php

use yii\db\Migration;

/**
 * Class m191019_123541_add_curator_id_column_tender_table
 */
class m191019_123541_add_curator_id_column_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'curator_id', $this->integer()->after('client_id')->comment('Куратор'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'curator_id');
    }
}
