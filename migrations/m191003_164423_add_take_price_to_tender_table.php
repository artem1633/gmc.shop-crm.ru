<?php

use yii\db\Migration;

/**
 * Class m191003_164423_add_take_price_to_tender_table
 */
class m191003_164423_add_take_price_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'take_price', $this->float()->comment('Цена по чем забрали'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'take_price');
    }
}
