<?php

use yii\db\Migration;

/**
 * Handles adding poslednego_zvonka to table `client`.
 */
class m180507_172416_add_poslednego_zvonka_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'poslednego_zvonka', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'poslednego_zvonka');
    }
}
