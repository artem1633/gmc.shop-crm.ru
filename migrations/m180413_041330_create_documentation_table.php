<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documentation`.
 */
class m180413_041330_create_documentation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documentation', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'number' => $this->string(255),
            'ownership' => $this->string(255),
            'working' => $this->text(),
            'stage_id' => $this->integer(),
            'file' => $this->string(255),
            'users_id' => $this->integer(),
            'client_id' => $this->integer(),
        ]);

        $this->createIndex('idx-documentation-stage_id', 'documentation', 'stage_id', false);
        $this->addForeignKey("fk-documentation-stage_id", "documentation", "stage_id", "stages_document", "id");

        $this->createIndex('idx-documentation-users_id', 'documentation', 'users_id', false);
        $this->addForeignKey("fk-documentation-users_id", "documentation", "users_id", "users", "id");

        $this->createIndex('idx-documentation-client_id', 'documentation', 'client_id', false);
        $this->addForeignKey("fk-documentation-client_id", "documentation", "client_id", "client", "id");
    }
       
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-documentation-stage_id','documentation');
        $this->dropIndex('idx-documentation-stage_id','documentation');

        $this->dropForeignKey('fk-documentation-users_id','documentation');
        $this->dropIndex('idx-documentation-users_id','documentation');

        $this->dropForeignKey('fk-documentation-client_id','documentation');
        $this->dropIndex('idx-documentation-client_id','documentation');

        $this->dropTable('documentation');
    }
}
