<?php

use yii\db\Migration;

/**
 * Handles adding tovar_id to table `stock`.
 */
class m180605_043215_add_tovar_id_column_to_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stock', 'tovar_id', $this->integer());

        $this->createIndex('idx-stock-tovar_id', 'stock', 'tovar_id', false);
        $this->addForeignKey("fk-stock-tovar_id", "stock", "tovar_id", "tovars", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-stock-tovar_id','stock');
        $this->dropIndex('idx-stock-tovar_id','stock');

        $this->dropColumn('stock', 'tovar_id');
    }
}
