<?php

use yii\db\Migration;

/**
 * Handles adding sleduyushchego_zvonka to table `client`.
 */
class m180507_173209_add_sleduyushchego_zvonka_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'sleduyushchego_zvonka', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'sleduyushchego_zvonka');
    }
}
