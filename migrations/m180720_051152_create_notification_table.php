<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notification`.
 */
class m180720_051152_create_notification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(255),
            'field' => $this->integer(),
            'status' => $this->integer(),
            'user_id' => $this->integer(),
            'description' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notification');
    }
}
