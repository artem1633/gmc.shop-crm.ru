<?php

use yii\db\Migration;

/**
 * Handles the creation of table `documents_type`.
 */
class m180508_140752_create_documentstype_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('documentstype', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('documents_type');
    }
}
