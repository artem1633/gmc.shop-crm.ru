<?php

use yii\db\Migration;

/**
 * Handles the creation of table `information`.
 */
class m180407_114852_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'working_title' => $this->string(255),
            'name_floor' => $this->string(255),
            'name_juice' => $this->string(255),
            'tin_cat' => $this->string(255),
            'jur_address' => $this->text(),
            'phiz_address' => $this->text(),
            'bic' => $this->string(255),
            'bank' => $this->string(255),
            'r_c' => $this->string(255),
            'k_c' => $this->string(255),
            'ogrn' => $this->string(255),
            'head_of' => $this->string(255),
            'comment' => $this->text(),
            'contacts' => $this->string(255),
            'curator' => $this->integer(),
            'relevance' => $this->integer(),
        ]);
        $this->createIndex('idx-client-curator', 'client', 'curator', false);
        $this->addForeignKey("fk-client-curator", "client", "curator", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-client-curator','client');
        $this->dropIndex('idx-client-curator','client');
        
        $this->dropTable('client');
    }
}
