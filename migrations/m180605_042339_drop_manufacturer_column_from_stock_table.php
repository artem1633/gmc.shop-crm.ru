<?php

use yii\db\Migration;

/**
 * Handles dropping manufacturer from table `stock`.
 */
class m180605_042339_drop_manufacturer_column_from_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('stock', 'manufacturer');
        $this->dropColumn('stock', 'name_goods');
        $this->dropColumn('stock', 'product_image');
        $this->dropColumn('stock', 'short_description');
        $this->dropColumn('stock', 'specifications');
        $this->dropColumn('stock', 'full_description');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('stock', 'manufacturer', $this->string(255));
        $this->addColumn('stock', 'name_goods', $this->string(255));
        $this->addColumn('stock', 'product_image', $this->string(255));
        $this->addColumn('stock', 'short_description', $this->text());
        $this->addColumn('stock', 'specifications', $this->text());
        $this->addColumn('stock', 'full_description', $this->text());
    }
}
