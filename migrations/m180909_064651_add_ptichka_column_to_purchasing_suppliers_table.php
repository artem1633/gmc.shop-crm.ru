<?php

use yii\db\Migration;

/**
 * Handles adding ptichka to table `purchasing_suppliers`.
 */
class m180909_064651_add_ptichka_column_to_purchasing_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('purchasing_suppliers', 'ptichka', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('purchasing_suppliers', 'ptichka');
    }
}
