<?php

use yii\db\Migration;

/**
 * Handles dropping days_for_supervision from table `client`.
 */
class m181016_135814_drop_days_for_supervision_column_from_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('client', 'days_for_supervision');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('client', 'days_for_supervision', $this->integer());
    }
}
