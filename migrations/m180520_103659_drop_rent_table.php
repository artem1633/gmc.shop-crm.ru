<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `rent`.
 */
class m180520_103659_drop_rent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('rent', 'term_lease');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->addColumn('rent', 'term_lease', $this->integer()->notNull());
    }
}
