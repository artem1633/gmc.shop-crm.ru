<?php

use yii\db\Migration;

/**
 * Handles the creation of table `call_status`.
 */
class m180407_003842_create_call_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('call_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('call_status');
    }
}
