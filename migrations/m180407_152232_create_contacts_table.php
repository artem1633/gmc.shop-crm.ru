<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180407_152232_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255),
            'position' => $this->string(255),
            'phone' => $this->string(255),
            'email' => $this->string(255),
            'client_id' => $this->integer(),
        ]);
        $this->createIndex('idx-contacts-client_id', 'contacts', 'client_id', false);
        $this->addForeignKey("fk-contacts-client_id", "contacts", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-contacts-client_id','contacts');
        $this->dropIndex('idx-contacts-client_id','contacts');

        $this->dropTable('contacts');
    }
}
