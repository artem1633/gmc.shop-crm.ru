<?php

use yii\db\Migration;

/**
 * Handles adding client_id to table `editrequest`.
 */
class m180907_134446_add_client_id_column_to_editrequest_table extends Migration
{
    /**add_category_id
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('editrequest', 'client_id', $this->integer());

        $this->createIndex('idx-editrequest-client_id', 'editrequest', 'client_id', false);
        $this->addForeignKey("fk-editrequest-client_id", "editrequest", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-editrequest-client_id','editrequest');
        $this->dropIndex('idx-editrequest-client_id','editrequest');
        
        $this->dropColumn('editrequest', 'client_id');
    }
}
