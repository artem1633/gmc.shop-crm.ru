<?php

use yii\db\Migration;
use app\models\TemplateFields;

/**
 * Handles the creation of table `template`.
 */
class m180920_121024_create_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('template', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'text' => $this->text(),
        ]);

        $this->insert('template',array(
            'name' => 'Дальномер калибровка',
            'key' => 'dalnomer_kalibrovka',
            'text' => TemplateFields::getDalnomerKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Дальномер поверка',
            'key' => 'dalnomer_poverka',
            'text' => TemplateFields::getDalnomerPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Колесо дорожное калибровка',          
            'key' => 'koleso_dorojnoe_kalibrovka',
            'text' => TemplateFields::getKolesoDorojnoeKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Колесо дорожное поверка',          
            'key' => 'koleso_dorojnoe_poverka',
            'text' => TemplateFields::getKolesoDorojnoePoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Линейка калибровка',          
            'key' => 'lineyka_kalibrovka',
            'text' => TemplateFields::getLineykaKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Ниверил в комплекте с рейками поверка',          
            'key' => 'niveril_reyka_poverka',
            'text' => TemplateFields::getNiverilReykaPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Ниверил калибровка',          
            'key' => 'niveril_kalibrovka',
            'text' => TemplateFields::getNiverilKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Ниверил поверка',          
            'key' => 'niveril_poverka',
            'text' => TemplateFields::getNiverilPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Ниверил поверка-1',          
            'key' => 'niveril_poverka_1',
            'text' => TemplateFields::getNiverilPoverka1(),
        ));

        $this->insert('template',array(
            'name' => 'Прибор вертикального проектирование калибровка',          
            'key' => 'pribor_verticalnogo_proyektirovanie_kalibrovka',
            'text' => TemplateFields::getPriborVerticalnogoProyektirovanieKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Пустышка калибровка',          
            'key' => 'pustishka_kalibrovka',
            'text' => TemplateFields::getPustishkaKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Пустышка поверка',          
            'key' => 'pustishka_poverka',
            'text' => TemplateFields::getPustishkaPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Рейка дорожная поверка',          
            'key' => 'reyka_dorojnaya_poverka',
            'text' => TemplateFields::getReykaDorojnayaPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Рейка калибровка',          
            'key' => 'reyka_kalibrovka',
            'text' => TemplateFields::getReykaKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Рейка поверка',          
            'key' => 'reyka_poverka',
            'text' => TemplateFields::getReykaPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Рулетка калибровка',          
            'key' => 'ruletka_kalibrovka',
            'text' => TemplateFields::getRuletkaKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Рулетка поверка',          
            'key' => 'ruletka_poverka',
            'text' => TemplateFields::getRuletkaPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Тахеометр поверка',          
            'key' => 'taxeometr_poverka',
            'text' => TemplateFields::getTaxeometrPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Теодолит поверка',          
            'key' => 'teodolit_poverka',
            'text' => TemplateFields::getTeodolitPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Теодолить калибровка',          
            'key' => 'teodolit_kalibrovka',
            'text' => TemplateFields::getTeodolitKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Уровень калибровка',          
            'key' => 'uroven_kalibrovka',
            'text' => TemplateFields::getUrovenKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Уровень поверка',          
            'key' => 'uroven_poverka',
            'text' => TemplateFields::getUrovenPoverka(),
        ));

        $this->insert('template',array(
            'name' => 'Штангенциркуль поверка',          
            'key' => 'shtangensirkul_poverka',
            'text' => TemplateFields::getShtangensirkulPoverka(),
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('template');
    }
}
