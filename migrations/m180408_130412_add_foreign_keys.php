<?php

use yii\db\Migration;

/**
 * Class m180408_130412_add_foreign_keys
 */
class m180408_130412_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-verification-devices_id', 'verification', 'devices_id', false);
        $this->addForeignKey("fk-verification-devices_id", "verification", "devices_id", "devices", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-verification-devices_id','verification');
        $this->dropIndex('idx-verification-devices_id','verification');

    }
}
