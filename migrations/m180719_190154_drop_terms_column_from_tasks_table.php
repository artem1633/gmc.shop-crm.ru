<?php

use yii\db\Migration;

/**
 * Handles dropping terms from table `tasks`.
 */
class m180719_190154_drop_terms_column_from_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tasks', 'terms');
        $this->addColumn('tasks', 'terms', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('tasks', 'terms', $this->float());
        $this->dropColumn('tasks', 'terms');
    }
}
