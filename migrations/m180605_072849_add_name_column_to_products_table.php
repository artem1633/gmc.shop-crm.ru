<?php

use yii\db\Migration;

/**
 * Handles adding name to table `products`.
 */
class m180605_072849_add_name_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'manufacturer_id', $this->integer());
        $this->addColumn('products', 'product_image', $this->string(255));
        $this->addColumn('products', 'short_description', $this->text());
        $this->addColumn('products', 'specifications', $this->text());
        $this->addColumn('products', 'full_description', $this->text());

        $this->createIndex('idx-products-manufacturer_id', 'products', 'manufacturer_id', false);
        $this->addForeignKey("fk-products-manufacturer_id", "products", "manufacturer_id", "manufacturer", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-products-manufacturer_id','products');
        $this->dropIndex('idx-products-manufacturer_id','products');
    }
}
