<?php

use yii\db\Migration;

/**
 * Handles adding delivery_time to table `purchasing_suppliers`.
 */
class m180520_104858_add_delivery_time_column_to_purchasing_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('purchasing_suppliers', 'delivery_time', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('purchasing_suppliers', 'delivery_time');
    }
}
