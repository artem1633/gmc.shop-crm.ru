<?php

use yii\db\Migration;

/**
 * Handles dropping manufacture from table `purchasing_suppliers`.
 */
class m180919_182858_drop_manufacture_column_from_purchasing_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('purchasing_suppliers', 'manufacture');
        $this->addColumn('purchasing_suppliers', 'manufacture', $this->integer());

        $this->createIndex('idx-purchasing_suppliers-manufacture', 'purchasing_suppliers', 'manufacture', false);
        $this->addForeignKey("fk-purchasing_suppliers-manufacture", "purchasing_suppliers", "manufacture", "manufacturer", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-purchasing_suppliers-manufacture','purchasing_suppliers');
        $this->dropIndex('idx-purchasing_suppliers-manufacture','purchasing_suppliers');

        $this->dropColumn('purchasing_suppliers', 'manufacture');
        $this->addColumn('purchasing_suppliers', 'manufacture', $this->string(255));
    }
}
