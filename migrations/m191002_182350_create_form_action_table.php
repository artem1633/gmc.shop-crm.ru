<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%form_action}}`.
 */
class m191002_182350_create_form_action_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%form_action}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%form_action}}');
    }
}
