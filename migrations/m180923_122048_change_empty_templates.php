<?php

use yii\db\Migration;
use app\models\TemplateFields;

/**
 * Class m180923_122048_change_empty_templates
 */
class m180923_122048_change_empty_templates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->update('template', array(
            'text' => TemplateFields::getPustishkaKalibrovka()), 
            'id=11'
        );

        $this->update('template', array(
            'text' => TemplateFields::getPustishkaPoverka()), 
            'id=12'
        );

        /*$this->insert('template',array(
            'name' => 'Пустышка калибровка',          
            'key' => 'pustishka_kalibrovka',
            'text' => TemplateFields::getPustishkaKalibrovka(),
        ));

        $this->insert('template',array(
            'name' => 'Пустышка поверка',          
            'key' => 'pustishka_poverka',
            'text' => TemplateFields::getPustishkaPoverka(),
        ));*/
    }

    public function down()
    {
        
    }
}
