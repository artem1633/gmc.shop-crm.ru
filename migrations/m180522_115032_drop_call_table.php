<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `call`.
 */
class m180522_115032_drop_call_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('call', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('call', 'date', $this->date()->notNull());
    }
}
