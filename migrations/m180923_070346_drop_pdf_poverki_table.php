<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `pdf_poverki`.
 */
class m180923_070346_drop_pdf_poverki_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('pdf_poverki');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('pdf_poverki', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'text' => $this->text(),
        ]);
    }
}
