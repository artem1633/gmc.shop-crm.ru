<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `tovars`.
 */
class m180605_072706_drop_tovars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('tovars');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('tovars', [
            'id' => $this->primaryKey(),
        ]);
    }
}
