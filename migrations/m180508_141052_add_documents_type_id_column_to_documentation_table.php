<?php

use yii\db\Migration;

/**
 * Handles adding documents_type_id to table `documentation`.
 */
class m180508_141052_add_documents_type_id_column_to_documentation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('documentation', 'documents_type_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('documentation', 'documents_type_id');
    }
}
