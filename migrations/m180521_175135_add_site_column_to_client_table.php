<?php

use yii\db\Migration;

/**
 * Handles adding site to table `client`.
 */
class m180521_175135_add_site_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'site', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'site');
    }
}
