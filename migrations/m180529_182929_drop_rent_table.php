<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `rent`.
 */
class m180529_182929_drop_rent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('rent', 'stage_transaction');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('rent', 'stage_transaction', $this->string()->notNull());
    }
}
