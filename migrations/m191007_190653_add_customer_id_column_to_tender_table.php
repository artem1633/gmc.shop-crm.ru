<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191007_190653_add_customer_id_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'customer_id', $this->integer()->after('customer')->comment('Клиент'));

        $this->createIndex(
            'idx-tender-customer_id',
            'tender',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-tender-customer_id',
            'tender',
            'customer_id',
            'client',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender-customer_id',
            'tender'
        );

        $this->dropIndex(
            'idx-tender-customer_id',
            'tender'
        );

        $this->dropColumn('tender', 'customer_id');
    }
}
