<?php

use yii\db\Migration;

/**
 * Class m180508_141502_add_foreign_keys
 */
class m180508_141502_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-documentation-documents_type_id', 'documentation', 'documents_type_id', false);
        $this->addForeignKey("fk-documentation-documents_type_id", "documentation", "documents_type_id", "documentstype", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-documentation-documents_type_id','documentation');
        $this->dropIndex('idx-documentation-documents_type_id','documentation');
    }

}
