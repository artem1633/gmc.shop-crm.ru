<?php

use yii\db\Migration;

/**
 * Handles adding verification to table `organizer`.
 */
class m180513_145803_add_verification_column_to_organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('organizer', 'verification', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('organizer', 'verification');
    }
}
