<?php

use yii\db\Migration;

/**
 * Handles adding arxiv to table `purchasing_suppliers`.
 */
class m180520_122946_add_arxiv_column_to_purchasing_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('purchasing_suppliers', 'arxiv', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('purchasing_suppliers', 'arxiv');
    }
}
