<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tovars`.
 */
class m180605_042948_create_tovars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tovars', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'cost' => $this->float(),
            'manufacturer_id' => $this->integer(),
            'product_image' => $this->string(255),
            'short_description' => $this->text(),
            'specifications' => $this->text(),
            'full_description' => $this->text(),
        ]);

        $this->createIndex('idx-tovars-manufacturer_id', 'tovars', 'manufacturer_id', false);
        $this->addForeignKey("fk-tovars-manufacturer_id", "tovars", "manufacturer_id", "manufacturer", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tovars-manufacturer_id','tovars');
        $this->dropIndex('idx-tovars-manufacturer_id','tovars');

        $this->dropTable('tovars');
    }
}
