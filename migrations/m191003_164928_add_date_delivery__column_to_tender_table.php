<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191003_164928_add_date_delivery__column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'date_delivery', $this->date()->comment('Дата поставки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'date_delivery');
    }
}
