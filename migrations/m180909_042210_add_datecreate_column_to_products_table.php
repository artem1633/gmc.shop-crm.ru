<?php

use yii\db\Migration;

/**
 * Handles adding datecreate to table `products`.
 */
class m180909_042210_add_datecreate_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'datecreate', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'datecreate');
    }
}
