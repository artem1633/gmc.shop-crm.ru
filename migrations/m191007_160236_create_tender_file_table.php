<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tender_file}}`.
 */
class m191007_160236_create_tender_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tender_file}}', [
            'id' => $this->primaryKey(),
            'tender_id' => $this->integer()->comment('Тендер'),
            'name' => $this->string()->comment('Наименование файла'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-tender_file-tender_id',
            'tender_file',
            'tender_id'
        );

        $this->addForeignKey(
            'fk-tender_file-tender_id',
            'tender_file',
            'tender_id',
            'tender',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender_file-tender_id',
            'tender_file'
        );

        $this->dropIndex(
            'idx-tender_file-tender_id',
            'tender_file'
        );

        $this->dropTable('{{%tender_file}}');
    }
}
