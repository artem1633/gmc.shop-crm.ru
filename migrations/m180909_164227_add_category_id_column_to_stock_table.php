<?php

use yii\db\Migration;
 
/**
 * Handles adding category_id to table `stock`.
 */
class m180909_164227_add_category_id_column_to_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stock', 'category_id', $this->integer());

        $this->createIndex('idx-stock-category_id', 'stock', 'category_id', false);
        $this->addForeignKey("fk-stock-category_id", "stock", "category_id", "category", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-stock-category_id','stock');
        $this->dropIndex('idx-stock-category_id','stock');
        
        $this->dropColumn('stock', 'category_id');
    }
}
