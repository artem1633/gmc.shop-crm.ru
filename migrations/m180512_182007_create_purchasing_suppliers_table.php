<?php

use yii\db\Migration;

/**
 * Handles the creation of table `purchasing_suppliers`.
 */
class m180512_182007_create_purchasing_suppliers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('purchasing_suppliers', [
            'id' => $this->primaryKey(),
            'date_creation_application' => $this->date(),
            'manufacture' => $this->string(255),
            'order_number' => $this->integer(),
            'execution_phase' => $this->string(255),
            'notes_order' => $this->text(),
            'documents' => $this->string(255),
            'delivery_time' => $this->integer(),
            'status' => $this->integer(),
        ]);
        $this->createIndex('idx-purchasing_suppliers-order_number', 'purchasing_suppliers', 'order_number', false);
        $this->addForeignKey("fk-purchasing_suppliers-order_number", "purchasing_suppliers", "order_number", "orders", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-purchasing_suppliers-order_number','purchasing_suppliers');
        $this->dropIndex('idx-purchasing_suppliers-order_number','purchasing_suppliers');
        
        $this->dropTable('purchasing_suppliers');
    }
}
