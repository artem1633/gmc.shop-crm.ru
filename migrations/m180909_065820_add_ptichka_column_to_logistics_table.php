<?php

use yii\db\Migration;

/**
 * Handles adding ptichka to table `logistics`.
 */
class m180909_065820_add_ptichka_column_to_logistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('logistics', 'ptichka', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('logistics', 'ptichka');
    }
}
