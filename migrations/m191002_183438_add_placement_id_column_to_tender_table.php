<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191002_183438_add_placement_id_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'placement_id', $this->integer()->comment('Площадка размещения'));

        $this->createIndex(
            'idx-tender-placement_id',
            'tender',
            'placement_id'
        );

        $this->addForeignKey(
            'fk-tender-placement_id',
            'tender',
            'placement_id',
            'placement',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender-placement_id',
            'tender'
        );

        $this->dropIndex(
            'idx-tender-placement_id',
            'tender'
        );

        $this->dropColumn('tender', 'placement_id');
    }
}
