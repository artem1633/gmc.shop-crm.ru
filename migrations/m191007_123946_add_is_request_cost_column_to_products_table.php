<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%products}}`.
 */
class m191007_123946_add_is_request_cost_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'is_request_cost', $this->boolean()->defaultValue(false)->after('cost')->comment('Цена по запросу'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'is_request_cost');
    }
}
