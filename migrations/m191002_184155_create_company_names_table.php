<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_names}}`.
 */
class m191002_184155_create_company_names_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_names}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_names}}');
    }
}
