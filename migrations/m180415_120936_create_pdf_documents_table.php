<?php

use yii\db\Migration; 

/**
 * Handles the creation of table `pdf_documents`.
 */
class m180415_120936_create_pdf_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pdf_documents', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'text' => $this->text(),
        ]);
 
        $this->insert('pdf_documents',[
            'id' =>1,
            'name' => 'Создать счёт',
            'text' => '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<!-- =======pdf dakumentning css qismi=======================-->
<style>
* { margin: 0; padding: 0; }
body { font: 14px/1.4 Georgia, serif; }
#page-wrap { width: 800px; margin: 0 auto; }

textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
table { border-collapse: collapse; }
table td, table th { border: 1px solid black; padding: 5px;  }

#header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }
#meta { margin-top: 1px; width: 300px; float: right; }
#meta td { text-align: right;  }
#meta td.meta-head { text-align: left; background: #eee; }
#meta td textarea { width: 100%; height: 20px; text-align: right; }
#terms { text-align: center; margin: 20px 0 0 0; }
#terms h5 { border-bottom: 2px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
#terms textarea { width: 100%; text-align: center;}
</style>
<!-- ========================================================= -->
    <title>РусГеоТорг</title>
</head>
<body>
    <div style=" width: 800px; margin: 0 auto;">
        <div >
        <table style="width:100%;">
            <tr style="border:none;"> 
                <th style="text-align:left;border:none; width:170px ">
                    <table  style="width: 100%;height:50px;text-align: center;font-size: 13px;font-family: Arial; ">
                        <tr >
                            <td style="height:120px;"></td>
                        </tr>                                        
                    </table>
                </th>
                <th style="border:none; text-align:right;width:550px;">
                    <table  style="width: 100%;text-align: center;font-size: 13px;font-family: Arial; text-align:left ">
                            <tr >
                                <td  colspan="2" style="border-bottom:none;width:90px;">Ф ТОЧКА БАНК КИВИ БАНК (АО)</td>
                                <td style="width:50px">БИК</td>
                                <td style="border-bottom:none;">{client_bik}</td>
                            </tr>                                       
                            <tr> 
                                <td  colspan="2" style="border-top:none;">Банк получателя</td>
                                <td style="height:50px;">Сч. №</td>   
                                <td style="height:40px;" style="border-top:none;">30101810445250000797</td>     
                            </tr>
                            <tr>
                                <td >ИНН {client_inn}</td>
                                <td >КПП {client_kpp}</td>
                                <td style="border-bottom:none;">Сч. №</td>        
                                <td style="border-bottom:none;">40702810910050016706</td>
                            </tr>  
                             <tr>
                                <td style="height:35px;" colspan="2"><pre>ООО "РУСГЕОТОРГ"</pre><pre>Получатель</pre></td>
                                <td style="border-top:none;"></td>        
                                <td style="border-top:none;"></td>
                            </tr>            
                    </table>
                </th>
            </tr>
        </table>      
        <table id="meta" style="width: 150px;">
            <tr>
                <img style="width: 100%;height: 100%" id="image" src="/web/img/123.jpg" alt="logo" />
            </tr>  
        </table>
        </div>
       <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <div id="terms">
<pre style="text-align: left;font-size: 17px; font-family: Arial"><b>Счет на оплату № {number} от {today}</b></pre>
              <p style="border-bottom: 2px solid black;"></p>
<pre style="text-align: left;font-size: 13px; font-family: Arial">
Поставщик:  <b>ООО "РУСГЕОТОРГ", ИНН {client_inn}, КПП {client_kpp}, 630007, Новосибирская обл,</b>
(Исполнитель): <b>Новосибирск г, Коммунистическая ул, дом № 1, эт/оф 1/4 </b>
Покупатель: <b>{client_use_name}, ИНН {client_inn}, КПП {client_kpp}</b>
(Заказчик):
Основание: <b>{number} от {today}</b></pre>   
        </div>
   
        <table style="width: 100%;border: 1px solid black;text-align: center;font-size: 13px;font-family: Arial; ">
            <tr >
                <th>№</th>
                <th></th>
                <th style="40px">Товары (работы, услуги)</th>
                <th>Кол-во</th>
                <th>Ед.</th>
                <th>Цена</th>
                <th>Скидка</th>
                <th>Сумма</th>
            </tr>                                       
            {table}
        </table>
            <b><pre style="text-align: right;font-size: 13px;font-family: Arial">Итого:   {total}
            В том числе НДС:   {nds} 
            Всего к оплате:  {total_sum} </pre></b>
        <div id="terms">
<pre style="text-align: left;font-size: 13px;font-family: Arial">Всего наименований {total_count}, на сумму {total_sum} рублей
<b>{total_sum_string}</b><br/><br/>
Оплатить не позднее ___________________
Оплата данного счета означает согласие с условиями поставки товара.
Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе. 
Товар отпускается по факту прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и
паспорта.</pre>        
            <h5 ></h5>
        </div> <br/><br/> 
        <table style="width:100%;">
            <tr style="border:none;"> 
                <th style="text-align:left;border:none;font-size:14px;font-family: Arial "><pre><h4>Руководитель______________________________</h4></pre></th>
                <th style="border:none; text-align:right;font-size:14px;font-family: Arial"><h4 >Бухгалтер_________________________________</h4></th>
            </tr>
        </table>     
    </div>
</body>
</html>',
        ]);
        $this->insert('pdf_documents',[
            'id' =>2,
            'name' => 'Договор поставки',
            'text' => '
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">
            
            

<head>
<meta http-equiv=Content-Type content="text/html; charset=unicode">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
<link rel=File-List href="договор%20поставки%20РГТ%20рыба.files/filelist.xml">
<title>Договор поставки товара с гарантийным сроком - в MS Word (.doc)</title>
<style>

 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-unhide:no;
    mso-style-qformat:yes;
    mso-style-parent:"";
    margin:0cm;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:7.5pt;
    mso-bidi-font-size:8.0pt;
    font-family:"Verdana","sans-serif";
    mso-fareast-font-family:Verdana;
    mso-bidi-font-family:Times New Roman;}

@page Section1
    {size:595.3pt 841.9pt;
    margin:2.0cm 42.5pt 2.0cm 1.5cm;
    mso-header-margin:35.4pt;
    mso-footer-margin:35.4pt;
    mso-paper-source:0;}
div.Section1
    {page:Section1;}
 
</style>
<!--[if gte mso 10]>

<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="3074">
  <o:colormenu v:ext="edit" strokecolor="none"/>
 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>
<body lang=RU link=blue vlink=blue style="tab-interval:35.4pt">
<div class=Section1>
<p class=MsoNormal align=center style="text-align:center"><span
style="font-size:12px;font-family:Times New Roman,"serif"">ДОГОВОР ПОСТАВКИ
№ <o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">г.
Новосибирск&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
style="mso-spacerun:yes">                                                 
</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="background:yellow;mso-highlight:
yellow">{today}</span><o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">Общество с ограниченной
ответственностью «РУСГЕОТОРГ»,</span></b><span style="font-size:12px;
font-family:Times New Roman,"serif""> в лице директора <span class=SpellE>Мукашева</span>
Николая Александровича, действующего на основании Устава, именуемое в
дальнейшем &quot;Поставщик&quot;, с одной стороны, и<o:p></o:p></span></p>

<p class=MsoNormal><span class=GramE><span style="font-size:12px;font-family:
Times New Roman,"serif";background:yellow;mso-highlight:yellow">[полное
наименование организации</span><span style="font-size:12px;font-family:Times New Roman,"serif"">],
в <span style="background:yellow;mso-highlight:yellow">лице {type},
{fio},</span> действующего на основании <span style="background:
yellow;mso-highlight:yellow">[Устава, положения, доверенности],</span>
именуемое в дальнейшем &quot;Покупатель&quot;, с другой стороны, а вместе
именуемые &quot;Стороны&quot;, заключили настоящий договор о нижеследующем:</span></span><span
style="font-size:12px;font-family:Times New Roman,"serif""><o:p></o:p></span></p>

<b style="font-size:12px;font-family:Times New Roman,"serif"">1. Предмет договора&nbsp;</b>
<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">1.1.
Поставщик обязуется поставить Покупателю оборудование (далее по тексту - Товар)
в количестве, ассортименте и сроки, обусловленные настоящим договором, а
Покупатель обязуется принять и оплатить его в установленном настоящим договором
порядке, форме и размере.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">1.2.
Ассортимент и количество Товара определяется в спецификации, которая
согласовывается Сторонами и является приложением и неотъемлемой частью
настоящего договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">1.3.
По настоящему договору поставка Товара будет осуществляться по адресу
грузополучателя - <span style="background:yellow;mso-highlight:yellow">[указать
адрес].</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">1.4.
Грузополучателем Товара по настоящему договору является <span style="background:
yellow;mso-highlight:yellow">[Покупатель/иное лицо].</span><o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">2. Качество,
маркировка товара<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">2.1.
Товар должен иметь маркировку и содержать информацию в соответствии с
требованиями действующего законодательства Российской Федерации, а также
сопровождаться документами, предусмотренными законодательством Российской
Федерации для реализации Товара, в том числе подтверждающими качество Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">2.2.
Поставщик гарантирует, что поставленный Товар соответствует установленным
настоящим договором и приложениям к нему, <span class=GramE>являющихся</span>
его неотъемлемой частью, тр"бованиям в течение срока [<span style="background:
yellow;mso-highlight:yellow">годности/реализации</span>], гарантийного срока.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">2.3.
При любых изменениях в документации, подтверждающей качество и маркировку
Товара, <span style="mso-spacerun:yes"> </span>Поставщик обязуется сообщить о таких
изменениях Покупателю не позднее, чем за <span style="background:yellow;
mso-highlight:yellow">[значение]</span> дней до поставки Товара, путем
предоставления соответствующих документов.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">3. Гарантийный
срок товара<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.1.
По настоящему договору Поставщик гарантирует качество и надежность Товара в
течение гарантийного срока.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.2.
По настоящему договору гарантийный срок на Товар составляет <span
style="background:yellow;mso-highlight:yellow">[указать срок].</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.3.
Гарантийный срок Товара исчисляется с даты <span style="background:yellow;
mso-highlight:yellow">[поставки <span class=GramE>Товара/передачи <span
class=SpellE>Товара/передачи</span> Товара</span> первому потребителю/пуска
товара в эксплуатацию (если речь идет об оборудовании)/указать иной срок].</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.4.
Если Покупатель лишен возможности использовать Товар по обстоятельствам,
зависящим от Поставщика, гарантийный срок не истекает до устранения
соответствующих обстоятельств Поставщиком.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">Гарантийный
срок продлевается на время, в течение которого Товар не мог использоваться
из-за обнаруженных в нем недостатков, при условии извещения Поставщика о
недостатках Товара в <span style="background:yellow;mso-highlight:yellow">[указать
срок].</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.5.
Гарантийное обслуживание Товара <span style="background:yellow;mso-highlight:
yellow">осуществляется [указать наименование сервисного центра, осуществляющего
гарантийный/послегарантийный ремонт товара].</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.6.
Если в течение срока гарантии выявятся недостатки, неполнота и (или)
комплектность Товара, Поставщик обязуется незамедлительно за свой счет
устранить все обнаруженные дефекты, путем исправления либо полной или частичной
замены, а также <span class=SpellE>допоставить</span> недостающие
принадлежности Товара на основании соответствующих претензий Покупателя.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.7.
Замененный дефектный Товар и (или) его части возвращаются Поставщику по его
требованию и за его счет в срок, согласованный Сторонами.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.8.
Транспортные и другие расходы, связанные с возвратом или заменой дефектного
Товара, а также допоставкой недостающего Товара несет Поставщик.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.9.
Указанный в п. 3.2 настоящего договора срок в отношении новых товаров, поставленных
взамен дефектных, начинается с момента поставки.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.10.
Содержание и обоснование претензии должно быть подтверждено актом, составленным
при участии незаинтересованной компетентной организации.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.11.
Гарантийному ремонту (замене) не подлежит Товар:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
эксплуатировавшийся не должным образом, в том <span class=GramE>числе</span>
имеющий по вине Покупателя механические повреждения;<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
<span class=GramE>эксплуатировавшийся</span> или хранившийся в ненадлежащих
условиях (среде);<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
прошедший модификацию или ремонт не в <span style="background:yellow;
mso-highlight:yellow">[указать наименование сервисного центра, осуществляющего
гарантийный/послегарантийный ремонт товара];</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
<span class=GramE>имеющий</span> нарушения гарантийных пломб.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">3.12.
Дополнительные гарантии [указать дополнительные гарантии, установленные по
соглашению сторон применительно к настоящему договору].<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">4. Цена по
договору и порядок расчетов<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">4.1.
По настоящему договору Покупатель оплачивает поставляемый ему Поставщиком Товар
по ценам, указанным в спецификации.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><span
style="mso-spacerun:yes"> </span>4.2. Оплата Товара по настоящему договору
производится на условиях поэтапной оплаты:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
авансовый платеж - [значение] % от общей стоимости Товара, указанной в
спецификации, в срок не более [значение] дней <span class=GramE>с даты
выставления</span> счета;<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
окончательный платеж - [значение] % от общей стоимости Товара, указанной в
спецификации, в срок не более [значение] дней <span class=GramE>с даты поставки</span>
Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">4.4.
Оплата Товара осуществляется, путем безналичного платежа на расчетный счет
Поставщика, указанный в настоящем договоре.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">4.5.
Датой оплаты считается дата [списания денежных сре<span class=GramE>дств с р</span>асчетного
счета Покупателя/приема банком Поставщика платежных документов к
исполнению/поступления денежных средств на расчетный счет Поставщика].<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">4.6.
Увеличение Поставщиком цены Товара в одностороннем порядке в течение срока
действия настоящего договора не допускается.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">5. Порядок,
сроки и условия поставки<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.1.
По настоящему договору поставка Товара осуществляется [указать вид транспорта,
условия транспортировки].<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.2.
В случае досрочной поставки Товара Поставщиком Покупатель обязуется произвести
приемку в установленном порядке и произвести оплату в согласованные Сторонами
сроки.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.3.
При отгрузке Товара Поставщиком данный Товар должен быть осмотрен
уполномоченным представителем Покупателя в месте отгрузки Товара, в том числе
должно быть проверено соответствие Товара условиям настоящего договора,
сведениям, указанным в сопроводительных документах на данный Товар, а также
количеству, качеству, ассортименту и упаковке Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.4.
При обнаружении недостатков во время отгрузки Товара, несоответствий условиям
настоящего договора и сведениям, указанным в сопроводительных документах на
данный Товар, Покупатель уведомляет об этом Поставщика, составляя при возврате
[Товара/части Товара] Поставщику в письменной форме акт о возврате.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.5.
Все расходы, связанные при приеме Товара с обратной транспортировкой
некачественного, не соответствующего условиям настоящего договора, несет
Поставщик.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.6.
<span class=GramE>При возникновении между Сторонами настоящего договора спора
по поводу качества поставленного Товара по письменному требованию любой из
Сторон</span> может быть назначена экспертиза.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.7.
Поставщик считается исполнившим обязательство по поставке Товара, если доставил
его в место и сроки, указанные Покупателем, а также, если в результате приемки
Покупателем установлено соответствие количества, качества, ассортимента и
упаковки Товара, указанного <span class=GramE>в</span> [вписать нужное].<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">5.8.
Товар считается поставленным надлежащим образом, а Поставщик - выполнившим свои
обязательства (полностью или в соответствующей части) <span class=GramE>с даты
подписания</span> акта сдачи-приемки Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">При
этом право собственности на товары переходит от Поставщика к Покупателю в
момент <span style="background:yellow;mso-highlight:yellow">[приемки Товара
Покупателем/поступления оплаты за Товар Поставщику].</span><o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">6. Права и
обязанности сторон<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.
По настоящему договору Поставщик обязан:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.1.
Поставить Покупателю Товар надлежащего качества, на условиях, предусмотренных в
настоящем договоре.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.2.
Обеспечить участие своего представителя в приемке Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.3.
Одновременно с поставкой Товара передать Покупателю необходимую документацию.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.4.
Передать Покупателю Товар свободным от прав третьих лиц.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.5.
При возникновении недостатков Товара устранить их в течение <span
style="background:yellow;mso-highlight:yellow">[указать срок]</span> с момента
принятия Товара, составления акта и передачи акта Поставщику.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.6.
При наступлении гарантийных случаев устранять недостатки в соответствии с
условиями настоящего договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.7.
Извещать Покупателя обо всех обстоятельствах, затрудняющих или делающих
невозможным исполнение своих обязательств по настоящему договору в течение
[указать срок] с момента их возникновения.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.1.8.
Выполнять иные обязанности, предусмотренные настоящим договором.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.2.
По настоящему договору Покупатель обязан:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.2.1.
Произвести оплату Товара в порядке и в сроки, предусмотренные настоящим
договором.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.2.2.
Обеспечить своевременную приемку поставленного Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.2.3.
После приемки Товара подписать сопроводительные документы и передать один
экземпляр представителю Поставщика.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.2.4.
Уведомлять Поставщика о приостановлении, уменьшении или прекращении
финансирования настоящего договора для согласования новых сроков и других
условий поставки Товара.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.2.5.
Выполнять иные обязательства, предусмотренные настоящим договором.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.3.
По настоящему договору Поставщик вправе:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.3.1.
Требовать обеспечения своевременной приемки поставленного Товара и подписания
документа в установленные настоящим договором сроки.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.3.2.
Требовать оплаты штрафных санкций в соответствии с условиями настоящего договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.
По настоящему договору Покупатель вправе:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.1.
Требовать передачи Товара в соответствии с условиями настоящего договора и
сопроводительными документами в установленный срок.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.2.
Для проверки соответствия качества поставляемого Товара требованиям,
установленным настоящим договором, привлекать независимых экспертов.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.3.
Незамедлительно письменно уведомлять Поставщика о выявленных недостатках при
приемке Товара либо при наступлении гарантийных случаев.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.4.
Требовать оплаты штрафных санкций в соответствии с условиями настоящего
договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.5.
Полностью или частично отказаться от Товара, поставка которого предусмотрена
настоящим договором, при условии возмещения Поставщику убытков, причиненных
таким отказом.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.6.
Запрашивать у Поставщика любую относящуюся к предмету настоящего договора
документацию и информацию.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">6.4.7.
Отказаться от оплаты Товара ненадлежащего качества и некомплектного Товара. В
случае если такой Товар оплачен, потребовать возврата уплаченных сумм впредь до
устранения недостатков и доукомплектования Товара либо их замены.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">7.
Ответственность сторон. Форс-мажор<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.1.
Любая из Сторон настоящего договора, не исполнившая обязательства по договору
или исполнившая их ненадлежащим образом, несет ответственность за <span
class=GramE>упомянутое</span> при наличии вины (умысла или неосторожности,
небрежности, неосмотрительности).<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.2.
Отсутствие вины за неисполнение или ненадлежащее исполнение обязательств по
договору доказывается Стороной, нарушившей обязательства.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.3.
В случае существенного нарушения требований к качеству Товара Поставщик обязан
по выбору Покупателя вернуть ему уплаченную за Товар сумму или заменить Товар
ненадлежащего качества Товаром, соответствующим договору.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.4.
За недопоставку или просрочку поставки Товара Поставщик уплачивает Покупателю
неустойку в размере [значение] % от стоимости Товара за каждый день просрочки
до фактического исполнения обязательства.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.5.
За несвоевременную оплату переданного в соответствии с настоящим договором
Товара Покупатель уплачивает Поставщику неустойку в размере [значение] % от
суммы задолженности за каждый день просрочки.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.6.
Уплата штрафных санкций не освобождает Сторону, нарушившую условия настоящего
договора, от исполнения своих обязательств.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.7.
<span class=GramE>Ни одна из Сторон по настоящему договору не несет ответственности
перед другой Стороной за неисполнение обязательств по настоящему договору,
обусловленное действием обстоятельств непреодолимой силы, т. е. чрезвычайных и
непредотвратимых при данных условиях обстоятельств; в том числе объявленная или
фактическая война, гражданские волнения, эпидемии, блокада, эмбарго, пожары,
землетрясения, наводнения и другие природные стихийные бедствия техногенного
характера.</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.8.
Сторона, не исполнившая своего обязательства вследствие действия непреодолимой
силы, должна незамедлительно известить другую Сторону о таких обстоятельствах и
их влиянии на исполнение обязательств по настоящему договору.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">7.9.
Если обстоятельства непреодолимой силы действуют на протяжении более чем
[указать срок], настоящий <span class=GramE>договор</span> может быть расторгнут
любой из Сторон путем направления письменного уведомления другой Стороне.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">8. Риск
случайной гибели товара<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">8.1.
Риск случайной гибели или случайной порчи, утраты или повреждения Товара,
являющегося предметом настоящего договора, несет Поставщик или Покупатель в
зависимости от того, кто из них обладал правом собственности на Товар в момент
случайной гибели или случайного его повреждения.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">8.2.
По настоящему договору Поставщик несет все риски, потери или повреждения Товара
до момента его поставки Получателю.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">8.3.
По настоящему договору Получатель несет все риски, потери или повреждения
Товара с момента его получения.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">9. Действие
договора во времени<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">9.1.
Настоящий договор вступает в силу со дня подписания его Сторонами и действует в
течение <span style="background:yellow;mso-highlight:yellow">[вписать нужное] -
<span class=GramE>до</span> [число, месяц, год].</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">9.2.
Условия настоящего договора применяются к отношениям Сторон, возникшим только
после заключения настоящего договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">9.3.
Прекращение (окончание) срока действия настоящего договора влечет за собой
прекращение обязатель<span class=GramE>ств Ст</span>орон по нему, но не
освобождает Стороны договора от ответственности за его нарушения, если таковые
имели место при исполнении условий настоящего договора.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">10. Порядок
изменения и расторжения договора<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.1.
Все изменения и дополнения к настоящему договору действительны лишь в случаях
оформления в письменном виде и подписания обеими Сторонами.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.2.
Сторона направляет проект изменений в настоящий договор в виде дополнительного
соглашения к договору, другая Сторона обязана рассмотреть представленный проект
изменений и в течение [значение] дней с момента получения направить подписанный
экземпляр дополнительного соглашения либо мотивированный отказ от внесения
представленных изменений.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.3.
Последствия изменения и (или) дополнения настоящего договора определяются
взаимным соглашением Сторон или судом по требованию любой из Сторон договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.4.
Если Стороны договора не достигли согласия о приведении договора в соответствие
с изменившимися обстоятельствами (изменение или дополнение условий договора),
по требованию заинтересованной Стороны, договор может быть изменен и (или)
дополнен по решению суда только при наличии условий, предусмотренных
действующим законодательством.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.5.
Настоящий договор прекращает свое действие по окончании его срока, а также
может быть расторгнут досрочно:<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
по письменному соглашению Сторон;<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
<span class=GramE>в одностороннем порядке при отказе одной из Сторон от
настоящего договора в случаях</span>, когда возможность такого отказа
предусмотрена законом или настоящим договором;<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">-
в иных случаях, предусмотренных законом или соглашением Сторон.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.6.
Настоящий <span class=GramE>договор</span> может быть расторгнут судом по
требованию одной из Сторон только при существенном нарушении условий договора
одной из Сторон или в иных случаях, предусмотренных настоящим договором или
действующим законодательством.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">Нарушение
условий договора признается существенным, когда одна из Сторон допустила
действие (бездействие), которое влечет для другой Стороны такой ущерб, что
дальнейшее действие договора теряет смысл, поскольку эта Сторона в значительной
мере лишается того, на что рассчитывала при заключении договора.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">10.7.
Последствия расторжения настоящего договора определяются взаимным соглашением
Сторон или судом по требованию любой из Сторон договора.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">11. Порядок
разрешения споров<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">11.1.
Стороны примут все усилия к тому, чтобы разрешение всех споров осуществлялось
путем переговоров.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">11.2.
В случае если Стороны не достигнут соглашения по спорным вопросам в результате
проведения переговоров, то разрешение споров продолжается в претензионном
порядке.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">Срок
на рассмотрение претензии устанавливается равным [значение] дням. Все письменные
требования и претензии направляются Сторонами в оригиналах, по почте, заказными
письмами с уведомлением.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">11.3.
При <span class=SpellE>недостижении</span> взаимоприемлемого решения Стороны
вправе передать спорный вопрос <span class=GramE>на разрешение в судебном
порядке в соответствии с действующими в Российской Федерации</span> положениями
о порядке разрешения споров между Сторонами (юридическими лицами) - участниками
коммерческих, финансовых и иных отношений делового оборота.<o:p></o:p></span></p>

<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">12.
Заключительные положения<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">12.1.
<span class=GramE>По всем вопросам, не нашедшим решения в условиях настоящего
договора, но прямо или косвенно вытекающим из отношений Сторон по нему,
затрагивающих имущественные интересы и деловую репутацию Сторон настоящего
договора, имея в виду необходимости защиты их охраняемых законом прав и
интересов, Стороны настоящего договора будут руководствоваться нормами и
положениями действующего законодательства Российской Федерации.</span><o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">12.2.
В случае изменения юридического адреса или обслуживающего банка Стороны
настоящего договора обязаны в 3-дневный срок письменно уведомить об этом друг
друга.<o:p></o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif"">12.3.
Настоящий договор составлен в двух экземплярах, имеющих одинаковую юридическую
силу: один экземпляр хранится у Поставщика, другой - у Покупателя.<o:p></o:p></span></p>
<br/><br/>
<br/>
<br/>
<p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
style="font-size:12px;font-family:Times New Roman,"serif"">13. Реквизиты и
подписи сторон<o:p></o:p></span></b></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></p>

        <table  class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
         style="border: 1px solid black; border-collapse:collapse;mso-border-alt:solid black .5pt;
         mso-border-themecolor:text1;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt">
         <tr style="border: 1px solid black; mso-yfti-irow:0;mso-yfti-firstrow:yes">
          <td width=319 valign=top style="border: 1px solid black; width:239.25pt;border: 1px solid black;
          mso-border-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
          text1;padding:0cm 5.4pt 0cm 5.4pt">
          <p class=MsoNormal><b style=" mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">Поставщик<o:p></o:p></span></b></p>
          </td>
          <td width=319 valign=top style="border: 1px solid black; width:239.3pt;border:solid black 1.0pt;
          mso-border-themecolor:text1;border-left:none;mso-border-left-alt:solid black .5pt;
          mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
          text1;padding:0cm 5.4pt 0cm 5.4pt">
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">Покупатель<o:p></o:p></span></b></p>
          </td>
         </tr>
         <tr style="border: 1px solid black; mso-yfti-irow:1">
          <td width=319 valign=top style="width:239.25pt;border: 1px solid black;
          mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;
          mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
          text1;padding:0cm 5.4pt 0cm 5.4pt">
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">ООО
          «РУСГЕОТОРГ»<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">Адрес: 630007,
          г. Новосибирск, <o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">ул. <span
          class=GramE>Коммунистическая</span>, дом 1, <span class=SpellE>эт</span>/<span
          class=SpellE>оф</span> <span style="mso-spacerun:yes"> </span>1/4<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">ИНН/КПП <span
          style="mso-spacerun:yes"> </span>{client_inn}/{client_kpp}<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">ОГРН {client_ogrn}<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">ОКПО 24774634<o:p></o:p></span></b></p>
          <p class=MsoNormal><span class=GramE><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">Р</span></b></span><b
          style="mso-bidi-font-weight:normal"><span style="font-size:12px;font-family:
          Times New Roman,"serif"">/сч.{client_r_s} <o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">Ф ТОЧКА БАНК
          КИВИ БАНК (АО)<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">К/<span
          class=SpellE>сч</span>. {client_k_s}<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">БИК {client_bik}<o:p></o:p></span></b></p>
          <p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></p>
          </td>
          <td width=319 valign=top style="border: 1px solid black; width:239.3pt;border-top:none;border-left:
          none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
          border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
          solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
          mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
          text1;padding:0cm 5.4pt 0cm 5.4pt">
          <p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></p>
          </td>
         </tr>
         <tr style="border: 1px solid black; mso-yfti-irow:2;mso-yfti-lastrow:yes">
          <td width=319 valign=top style="width:239.25pt;border: 1px solid black;
          mso-border-themecolor:text1;border-top:none;mso-border-top-alt:solid black .5pt;
          mso-border-top-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
          text1;padding:0cm 5.4pt 0cm 5.4pt">
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">Директор <o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></b></p>
          <p class=MsoNormal><span class=SpellE><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">_______________________Мукашев</span></b></span><b
          style="mso-bidi-font-weight:normal"><span style="font-size:12px;font-family:
          Times New Roman,"serif""> Н.А.<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif"">М.П.<o:p></o:p></span></b></p>
          <p class=MsoNormal><b style="mso-bidi-font-weight:normal"><span
          style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></b></p>
          </td>
          <td width=319 valign=top style="width:239.3pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;mso-border-bottom-themecolor:text1;
          border-right:solid black 1.0pt;mso-border-right-themecolor:text1;mso-border-top-alt:
          solid black .5pt;mso-border-top-themecolor:text1;mso-border-left-alt:solid black .5pt;
          mso-border-left-themecolor:text1;mso-border-alt:solid black .5pt;mso-border-themecolor:
          text1;padding:0cm 5.4pt 0cm 5.4pt">
          <p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></p>
          </td>
         </tr>
        </table>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal><span style="font-size:12px;font-family:Times New Roman,"serif""><o:p>&nbsp;</o:p></span></p>

</div>

</body>

</html>

            ',]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pdf_documents');
    }
}
