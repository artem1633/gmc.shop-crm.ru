<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171209_075358_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'login' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'type' => $this->integer()->notNull(),
            'dostup' => $this->boolean(),
        ]);

        $this->insert('users',array(
            'fio' => 'admin',
            'login' => 'admin@gmail.com',
            'password' => md5('admin'),
            'type' => 1,
            'dostup' => 1,
        ));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
