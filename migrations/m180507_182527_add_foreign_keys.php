<?php

use yii\db\Migration;

/**
 * Class m180507_182527_add_foreign_keys
 */
class m180507_182527_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-client-city_id', 'client', 'city_id', false);
        $this->addForeignKey("fk-client-city_id", "client", "city_id", "city", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-client-city_id','client');
        $this->dropIndex('idx-client-city_id','client');
    }
}
