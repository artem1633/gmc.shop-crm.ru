<?php

use yii\db\Migration;

/**
 * Handles adding stage_transaction to table `rent`.
 */
class m180529_183438_add_stage_transaction_column_to_rent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rent', 'stage_transaction', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rent', 'stage_transaction');
    }
}
