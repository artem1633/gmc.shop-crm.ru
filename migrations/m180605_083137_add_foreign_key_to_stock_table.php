<?php

use yii\db\Migration;

/**
 * Class m180605_083137_add_foreign_key_to_stock_table
 */
class m180605_083137_add_foreign_key_to_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->dropColumn('stock', 'tovar_id');
        $this->addColumn('stock', 'tovar_id', $this->integer());

        $this->createIndex('idx-stock-tovar_id', 'stock', 'tovar_id', false);
        $this->addForeignKey("fk-stock-tovar_id", "stock", "tovar_id", "products", "id");
    }

    public function down()
    {
        $this->dropForeignKey('fk-stock-tovar_id','stock');
        $this->dropIndex('idx-stock-tovar_id','stock');
    }
}
