<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rent_etap`.
 */
class m180529_183654_create_rent_etap_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rent_etap', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rent_etap');
    }
}
