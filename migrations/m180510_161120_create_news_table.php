<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m180510_161120_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'header' => $this->string(255),
            'text_post' => $this->text(),
            'image' => $this->string(255),
            'attached_file' => $this->string(255),
            'date' => $this->date(),
            'user_id' => $this->integer(),
            'arxiv' => $this->integer(),
        ]);
        $this->createIndex('idx-news-user_id', 'news', 'user_id', false);
        $this->addForeignKey("fk-news-user_id", "news", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-news-user_id','news');
        $this->dropIndex('idx-news-user_id','news');

        $this->dropTable('news');
    }
}
