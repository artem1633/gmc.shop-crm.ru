<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191002_191857_add_provision_contract_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'provision_contract', $this->float()->comment('Обеспечение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'provision_contract');
    }
}
