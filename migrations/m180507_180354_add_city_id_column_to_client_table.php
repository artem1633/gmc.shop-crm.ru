<?php

use yii\db\Migration;

/**
 * Handles adding city_id to table `client`.
 */
class m180507_180354_add_city_id_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'city_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'city_id');
    }
}
