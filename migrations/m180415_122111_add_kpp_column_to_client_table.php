<?php

use yii\db\Migration;

/**
 * Handles adding kpp to table `client`.
 */
class m180415_122111_add_kpp_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'kpp', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'kpp');
    }
}
