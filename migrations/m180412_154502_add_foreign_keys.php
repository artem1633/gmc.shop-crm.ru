<?php

use yii\db\Migration;

/**
 * Class m180412_154502_add_foreign_keys
 */
class m180412_154502_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-tasks-who_users_id', 'tasks', 'who_users_id', false);
        $this->addForeignKey("fk-tasks-who_users_id", "tasks", "who_users_id", "users", "id");

        $this->createIndex('idx-tasks-to_users_id', 'tasks', 'to_users_id', false);
        $this->addForeignKey("fk-tasks-to_users_id", "tasks", "to_users_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks-who_users_id','tasks');
        $this->dropIndex('idx-tasks-who_users_id','tasks');

        $this->dropForeignKey('fk-tasks-to_users_id','tasks');
        $this->dropIndex('idx-tasks-to_users_id','tasks');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_154502_add_foreign_keys cannot be reverted.\n";

        return false;
    }
    */
}
