<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%waiting_stages}}`.
 */
class m191003_150830_create_waiting_stages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%waiting_stages}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%waiting_stages}}');
    }
}
