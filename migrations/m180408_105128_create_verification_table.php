<?php

use yii\db\Migration;

/**
 * Handles the creation of table `verification`.
 */
class m180408_105128_create_verification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('verification', [
            'id' => $this->primaryKey(),
            'application_date'=>$this->date(),
            'check_date' => $this->date(),
            'number' => $this->string(255),
            'devices_id' => $this->integer(),
            'series' => $this->string(255),
            'date_next_check' => $this->date(),
            'client_id'=>$this->integer(),
        ]);
        $this->createIndex('idx-verification-client_id', 'verification', 'client_id', false);
        $this->addForeignKey("fk-verification-client_id", "verification", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-verification-client_id','verification');
        $this->dropIndex('idx-verification-client_id','verification');

        $this->dropTable('verification');
    }
}
