<?php

use yii\db\Migration;

/**
 * Class m180408_131040_add_foreign_keys
 */
class m180408_131040_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-call-contact_id', 'call', 'contact_id', false);
        $this->addForeignKey("fk-call-contact_id", "call", "contact_id", "contacts", "id");

        $this->createIndex('idx-call-status_call_id', 'call', 'status_call_id', false);
        $this->addForeignKey("fk-call-status_call_id", "call", "status_call_id", "call_status", "id");

        $this->createIndex('idx-call-users_id', 'call', 'users_id', false);
        $this->addForeignKey("fk-call-users_id", "call", "users_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-call-contact_id','call');
        $this->dropIndex('idx-call-contact_id','call');

        $this->dropForeignKey('fk-call-status_call_id','call');
        $this->dropIndex('idx-call-status_call_id','call');

        $this->dropForeignKey('fk-call-users_id','call');
        $this->dropIndex('idx-call-users_id','call');
    }
}
