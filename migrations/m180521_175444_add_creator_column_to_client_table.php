<?php

use yii\db\Migration;

/**
 * Handles adding creator to table `client`.
 */
class m180521_175444_add_creator_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'creator', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'creator');
    }
}
