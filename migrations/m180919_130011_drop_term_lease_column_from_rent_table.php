<?php

use yii\db\Migration;

/**
 * Handles dropping term_lease from table `rent`.
 */
class m180919_130011_drop_term_lease_column_from_rent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('rent', 'term_lease');
        $this->addColumn('rent', 'term_lease', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rent', 'term_lease');
        $this->addColumn('rent', 'term_lease', $this->string(255));
    }
}
