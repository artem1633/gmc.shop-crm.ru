<?php

use yii\db\Migration;

/**
 * Class m180529_183707_add_foreign_keys
 */
class m180529_183707_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
   public function safeUp()
    {
        $this->createIndex('idx-rent-stage_transaction', 'rent', 'stage_transaction', false);
        $this->addForeignKey("fk-rent-stage_transaction", "rent", "stage_transaction", "rent_etap", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-rent-stage_transaction','rent');
        $this->dropIndex('idx-rent-stage_transaction','rent');
    }
}
