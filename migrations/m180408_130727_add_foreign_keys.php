<?php

use yii\db\Migration;

/**
 * Class m180408_130727_add_foreign_keys
 */
class m180408_130727_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-tender-users_id', 'tender', 'users_id', false);
        $this->addForeignKey("fk-tender-users_id", "tender", "users_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tender-users_id','tender');
        $this->dropIndex('idx-tender-users_id','tender');
    }
}
