<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%goods}}`.
 */
class m191008_122050_add_new_columns_to_goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('goods', 'provider', $this->string()->comment('Поставщик'));
        $this->addColumn('goods', 'provide_time', $this->string()->comment('Срок поставки'));
        $this->addColumn('goods', 'contract_time', $this->string()->comment('Срок по договору'));
        $this->addColumn('goods', 'price_one_client', $this->float()->comment('Цена за ед. для заказчика'));
        $this->addColumn('goods', 'price_client', $this->float()->comment('Цена для заказчика'));
        $this->addColumn('goods', 'price_for_us', $this->float()->comment('Стоимость товара для нас'));
        $this->addColumn('goods', 'profit', $this->float()->comment('Прибыль'));
        $this->addColumn('goods', 'sum_additional', $this->float()->comment('Сумма, на которую надо доказать'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('goods', 'provider');
        $this->dropColumn('goods', 'provide_time');
        $this->dropColumn('goods', 'contract_time');
        $this->dropColumn('goods', 'price_one_client');
        $this->dropColumn('goods', 'price_client');
        $this->dropColumn('goods', 'price_for_us');
        $this->dropColumn('goods', 'profit');
        $this->dropColumn('goods', 'sum_additional');
    }
}
