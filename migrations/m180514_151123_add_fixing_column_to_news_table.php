<?php

use yii\db\Migration;

/**
 * Handles adding fixing to table `news`.
 */
class m180514_151123_add_fixing_column_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('news', 'fixing', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news', 'fixing');
    }
}
