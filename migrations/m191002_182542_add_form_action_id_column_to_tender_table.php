<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191002_182542_add_form_action_id_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'form_action_id', $this->integer()->comment('Форма заявление'));

        $this->createIndex(
            'idx-tender-form_action_id',
            'tender',
            'form_action_id'
        );

        $this->addForeignKey(
            'fk-tender-form_action_id',
            'tender',
            'form_action_id',
            'form_action',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender-form_action_id',
            'tender'
        );

        $this->dropIndex(
            'idx-tender-form_action_id',
            'tender'
        );

        $this->dropColumn('tender', 'form_action_id');
    }
}
