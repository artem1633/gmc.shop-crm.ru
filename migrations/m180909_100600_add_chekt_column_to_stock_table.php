<?php

use yii\db\Migration;

/**
 * Handles adding chekt to table `stock`.
 */
class m180909_100600_add_chekt_column_to_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stock', 'chekt', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('stock', 'chekt');
    }
}
