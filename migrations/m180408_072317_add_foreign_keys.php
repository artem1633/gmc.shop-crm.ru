<?php

use yii\db\Migration;

/**
 * Class m180408_072317_add_foreign_keys
 */
class m180408_072317_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-client-relevance', 'client', 'relevance', false);
        $this->addForeignKey("fk-client-relevance", "client", "relevance", "relevance_client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-client-relevance','client');
        $this->dropIndex('idx-client-relevance','client');
    }
}
