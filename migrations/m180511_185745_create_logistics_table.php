<?php

use yii\db\Migration;

/**
 * Handles the creation of table `logistics`.
 */
class m180511_185745_create_logistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('logistics', [
            'id' => $this->primaryKey(),
            'date_application_created' => $this->date(),
            'date_application' => $this->date(),
            'company_name' => $this->string(255),
            'order_id' => $this->integer(),
            'stage' => $this->integer(),
            'other' => $this->text(),
        ]);
        $this->createIndex('idx-logistics-order_id', 'logistics', 'order_id', false);
        $this->addForeignKey("fk-logistics-order_id", "logistics", "order_id", "orders", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-logistics-order_id','logistics');
        $this->dropIndex('idx-logistics-order_id','logistics');

        $this->dropTable('logistics');
    }
}
