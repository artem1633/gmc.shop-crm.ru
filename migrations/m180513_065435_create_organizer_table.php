<?php

use yii\db\Migration;

/**
 * Handles the creation of table `organizer`.
 */
class m180513_065435_create_organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('organizer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'who' => $this->integer(),
            'whom' => $this->integer(),
            'status' => $this->integer(),
            'date' => $this->date(),
            'description' => $this->text(),
        ]);
        $this->createIndex('idx-organizer-who', 'organizer', 'who', false);
        $this->addForeignKey("fk-organizer-who", "organizer", "who", "users", "id");

        $this->createIndex('idx-organizer-whom', 'organizer', 'whom', false);
        $this->addForeignKey("fk-organizer-whom", "organizer", "whom", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-organizer-who','organizer');
        $this->dropIndex('idx-organizer-who','organizer');

        $this->dropForeignKey('fk-organizer-whom','organizer');
        $this->dropIndex('idx-organizer-whom','organizer');

        $this->dropTable('organizer');
    }
}
