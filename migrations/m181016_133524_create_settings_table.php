<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m181016_133524_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'value' => $this->string(255),
        ]);

        $this->insert('settings',array(
            'name' => 'Количество дней на кураторство',
            'key' => 'curator_day_count',
            'value' => '0',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
