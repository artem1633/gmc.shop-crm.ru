<?php

use yii\db\Migration;

/**
 * Class m180420_133803_add_foreign_keys
 */
class m180420_133803_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-goods-product_id', 'goods', 'product_id', false);
        $this->addForeignKey("fk-goods-product_id", "goods", "product_id", "products", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-goods-product_id','goods');
        $this->dropIndex('idx-goods-product_id','goods');
    }
}
