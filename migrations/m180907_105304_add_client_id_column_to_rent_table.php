<?php

use yii\db\Migration;

/**
 * Handles adding client_id to table `rent`.
 */
class m180907_105304_add_client_id_column_to_rent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rent', 'client_id', $this->integer());

        $this->createIndex('idx-rent-client_id', 'rent', 'client_id', false);
        $this->addForeignKey("fk-rent-client_id", "rent", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-rent-client_id','rent');
        $this->dropIndex('idx-rent-client_id','rent');

        $this->dropColumn('rent', 'client_id');
    }
}
