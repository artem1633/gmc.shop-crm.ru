<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191002_191500_add_provision_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'provision', $this->float()->comment('Обеспечение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'provision');
    }
}
