<?php

use yii\db\Migration;

/**
 * Handles adding term_lease to table `rent`.
 */
class m180520_104801_add_term_lease_column_to_rent_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rent', 'term_lease', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rent', 'term_lease');
    }
}
