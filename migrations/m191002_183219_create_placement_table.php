<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%placement}}`.
 */
class m191002_183219_create_placement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%placement}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%placement}}');
    }
}
