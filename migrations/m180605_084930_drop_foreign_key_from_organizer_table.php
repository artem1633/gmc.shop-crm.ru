<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `foreign_key_from_organizer`.
 */
class m180605_084930_drop_foreign_key_from_organizer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-organizer-who','organizer');
        $this->dropIndex('idx-organizer-who','organizer');

        $this->dropForeignKey('fk-organizer-whom','organizer');
        $this->dropIndex('idx-organizer-whom','organizer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('idx-organizer-who', 'organizer', 'who', false);
        $this->addForeignKey("fk-organizer-who", "organizer", "who", "users", "id");

        $this->createIndex('idx-organizer-whom', 'organizer', 'whom', false);
        $this->addForeignKey("fk-organizer-whom", "organizer", "whom", "users", "id");
    }
}
