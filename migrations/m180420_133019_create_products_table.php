<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m180420_133019_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'count' => $this->integer(),
            'cost' => $this->float(),
            'sum' => $this->float(),
            'nds_protsent' => $this->integer(),
            'nds_sum' => $this->float(),
            'total_sum' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('products');
    }
}
