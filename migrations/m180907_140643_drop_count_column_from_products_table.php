<?php

use yii\db\Migration;

/**
 * Handles dropping count from table `products`.
 */
class m180907_140643_drop_count_column_from_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('products','count');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('products','count',$this->integer()->notNull());
    }
}
