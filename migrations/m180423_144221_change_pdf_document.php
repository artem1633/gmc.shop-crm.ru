<?php

use yii\db\Migration;

/**
 * Class m180423_144221_change_pdf_document
 */
class m180423_144221_change_pdf_document extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->update('pdf_documents', 
            array(
                'text' => '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<!-- =======pdf dakumentning css qismi=======================-->
<style>
* { margin: 0; padding: 0; }
body { font: 14px/1.4 Georgia, serif; }
#page-wrap { width: 800px; margin: 0 auto; }

textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
table { border-collapse: collapse; }
table td, table th { border: 1px solid black; padding: 5px;  }

#header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }
#meta { margin-top: 1px; width: 300px; float: right; }
#meta td { text-align: right;  }
#meta td.meta-head { text-align: left; background: #eee; }
#meta td textarea { width: 100%; height: 20px; text-align: right; }
#terms { text-align: center; margin: 0 0 0 0; }
#terms h5 { border-bottom: 2px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
#terms textarea { width: 100%; text-align: center;}
</style>
<!-- ========================================================= -->
    <title>РусГеоТорг</title>
</head>
<body>
    <div style=" width: 800px; margin: 0 auto;">
        <div >
        <table style="width:100%;">
            <tr style="border:none;"> 
                <th style="text-align:left;border:none; width:120px;height:120px;font-size: 13px;font-family: Arial;  ">
                    {logo}
                </th>
                <th style="border:none; text-align:right;width:550px;">
                    <table  style="width: 100%;text-align: center;font-size: 13px;font-family: Arial; text-align:left ">
                            <tr >
                                <td  colspan="2" style="border-bottom:none;width:90px;">Ф ТОЧКА БАНК КИВИ БАНК (АО)</td>
                                <td style="width:50px">БИК</td>
                                <td style="border-bottom:none;">044525797</td>
                            </tr>                                       
                            <tr>
                                <td  colspan="2" style="border-top:none;">Банк получателя</td>
                                <td style="height:50px;">Сч. №</td>   
                                <td style="height:40px;" style="border-top:none;">30101810445250000797</td>     
                            </tr>
                            <tr>
                                <td >ИНН 5407968820</td>
                                <td >КПП 540701001</td>
                                <td style="border-bottom:none;">Сч. №</td>        
                                <td style="border-bottom:none;">40702810910050016706</td>
                            </tr>  
                             <tr>
                                <td style="height:35px;" colspan="2"><pre>ООО "РУСГЕОТОРГ"</pre><pre>Получатель</pre></td>
                                <td style="border-top:none;"></td>        
                                <td style="border-top:none;"></td>
                            </tr>            
                    </table>
                </th>
            </tr>
        </table>      
        </div>
        <div id="terms">
<pre style="text-align: left;font-size: 17px; font-family: Arial"><b>Счет на оплату № {number_pay} от {today}</b></pre>
              <p style="border-bottom: 2px solid black;"></p>
<pre style="text-align: left;font-size: 13px; font-family: Arial">
Поставщик:  <b>ООО "РУСГЕОТОРГ", ИНН 5407968820, КПП 540701001, 630007, Новосибирская обл,</b>
(Исполнитель): <b>Новосибирск г, Коммунистическая ул, дом № 1, эт/оф 1/4 </b>
Покупатель: <b>{client_use_name}, ИНН {client_inn}, КПП {client_kpp}</b>
(Заказчик): {client_yur_address}
Основание: <b>{number} от {date_osnovanii}</b></pre>   
        </div>
   
        {table}
        
            <pre style="text-align: right;font-size: 13px;font-family: Arial"><b>Итого:   {total}
            В том числе НДС:   {nds} 
            Всего к оплате:  {total_sum} </b></pre>
        <div id="terms">
<pre style="text-align: left;font-size: 13px;font-family: Arial">Всего наименований {total_count}, на сумму {total_sum} рублей
<b>{total_sum_string}</b><br/><br/>
Оплатить не позднее {order_payment_date}
Оплата данного счета означает согласие с условиями поставки товара.
Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе. 
Товар отпускается по факту прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и
паспорта.</pre>        
            <h5 ></h5>
        </div> <br/><br/> 
        <table style="width:100%;">
            <tr style="border:none;"> 
                <th style="text-align:left;border:none;font-size:14px;font-family: Arial "><pre><h4>Руководитель______________________________</h4></pre></th>
                <th style="border:none; text-align:right;font-size:14px;font-family: Arial"><h4 >Бухгалтер_________________________________</h4></th>
            </tr>
        </table>     
    </div>
</body>
</html>'
            ), 
            'id=1'
        );

    }

    public function down()
    {
        
    }

}
