<?php

use yii\db\Migration;

/**
 * Handles adding days_for_supervision to table `client`.
 */
class m181013_092143_add_days_for_supervision_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('client', 'days_for_supervision', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('client', 'days_for_supervision');
    }
}
