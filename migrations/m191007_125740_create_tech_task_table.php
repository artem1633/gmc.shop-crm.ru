<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tech_task}}`.
 */
class m191007_125740_create_tech_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tech_task}}', [
            'id' => $this->primaryKey(),
            'tender_id' => $this->integer()->comment('Тендер'),
            'product_id' => $this->integer()->comment('Товар'),
            'count' => $this->integer()->comment('Кол-во'),
            'price' => $this->float()->comment('Цена за шт.'),
            'sum' => $this->float()->comment('Сумма'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-tech_task-tender_id',
            'tech_task',
            'tender_id'
        );

        $this->addForeignKey(
            'fk-tech_task-tender_id',
            'tech_task',
            'tender_id',
            'tender',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-tech_task-product_id',
            'tech_task',
            'product_id'
        );

        $this->addForeignKey(
            'fk-tech_task-product_id',
            'tech_task',
            'product_id',
            'products',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tech_task-product_id',
            'tech_task'
        );

        $this->dropIndex(
            'idx-tech_task-product_id',
            'tech_task'
        );

        $this->dropForeignKey(
            'fk-tech_task-tender_id',
            'tech_task'
        );

        $this->dropIndex(
            'idx-tech_task-tender_id',
            'tech_task'
        );

        $this->dropTable('{{%tech_task}}');
    }
}
