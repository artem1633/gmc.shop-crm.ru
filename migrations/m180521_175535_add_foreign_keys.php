<?php

use yii\db\Migration;

/**
 * Class m180521_175535_add_foreign_keys
 */
class m180521_175535_add_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-client-creator', 'client', 'creator', false);
        $this->addForeignKey("fk-client-creator", "client", "creator", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-client-creator','client');
        $this->dropIndex('idx-client-creator','client');
    }

}
