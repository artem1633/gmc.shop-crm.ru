<?php

use yii\db\Migration;

/**
 * Handles the creation of table `relevance_client`.
 */
class m180407_004205_create_relevance_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('relevance_client', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'color' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('relevance_client');
    }
}
