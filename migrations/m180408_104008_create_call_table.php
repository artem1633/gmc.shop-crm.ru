<?php

use yii\db\Migration;

/**
 * Handles the creation of table `calls`.
 */
class m180408_104008_create_call_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('call', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'contact_id' => $this->integer(),
            'description' => $this->string(255),
            'users_id'=>$this->integer(),
            'next_contact' => $this->date(),
            'status_call_id' => $this->integer(),
            'client_id'=>$this->integer(),
        ]);
        $this->createIndex('idx-call-client_id', 'call', 'client_id', false);
        $this->addForeignKey("fk-call-client_id", "call", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-call-client_id','call');
        $this->dropIndex('idx-call-client_id','call');

        $this->dropTable('call');

    }
}
