<?php

use yii\db\Migration;

/**
 * Handles adding completeness to table `products`.
 */
class m180907_141450_add_completeness_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'completeness', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'completeness');
    }
}
