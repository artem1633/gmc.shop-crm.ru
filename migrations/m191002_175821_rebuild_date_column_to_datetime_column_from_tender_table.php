<?php

use yii\db\Migration;

/**
 * Class m191002_175821_rebuild_date_column_to_datetime_column_from_tender_table
 */
class m191002_175821_rebuild_date_column_to_datetime_column_from_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tender', 'date');

        $this->addColumn('tender', 'datetime', $this->dateTime()->after('id')->comment('Дата и время'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tender', 'datetime');

        $this->addColumn('tender', 'date', $this->date()->after('id'));
    }
}
