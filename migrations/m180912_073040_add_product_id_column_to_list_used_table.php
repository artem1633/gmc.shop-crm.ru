<?php

use yii\db\Migration;

/**
 * Handles adding product_id to table `list_used`.
 */
class m180912_073040_add_product_id_column_to_list_used_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('list_used', 'product_id', $this->integer());

        $this->createIndex('idx-list_used-product_id', 'list_used', 'product_id', false);
        $this->addForeignKey("fk-list_used-product_id", "list_used", "product_id", "products", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-list_used-product_id','list_used');
        $this->dropIndex('idx-list_used-product_id','list_used');
            
        $this->dropColumn('list_used', 'product_id');
    }
}
