<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%tender}}`.
 */
class m191002_184308_add_company_name_id_column_to_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tender', 'company_name_id', $this->integer()->comment('Наименование компании'));

        $this->createIndex(
            'idx-tender-company_name_id',
            'tender',
            'company_name_id'
        );

        $this->addForeignKey(
            'fk-tender-company_name_id',
            'tender',
            'company_name_id',
            'company_names',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tender-company_name_id',
            'tender'
        );

        $this->dropIndex(
            'idx-tender-company_name_id',
            'tender'
        );

        $this->dropColumn('tender', 'company_name_id');
    }
}
