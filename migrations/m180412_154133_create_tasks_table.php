<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m180412_154133_create_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'time' => $this->time(),
            'who_users_id' => $this->integer(),
            'to_users_id' => $this->integer(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'terms' => $this->float(),
            'status' => $this->integer(),
            'attachment' => $this->string(255),
            'client_id' => $this->integer(),
        ]);
        $this->createIndex('idx-tasks-client_id', 'tasks', 'client_id', false);
        $this->addForeignKey("fk-tasks-client_id", "tasks", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks-client_id','tasks');
        $this->dropIndex('idx-tasks-client_id','tasks');

        $this->dropTable('tasks');
    }
}
