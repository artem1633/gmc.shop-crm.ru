<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tender`.
 */
class m180408_111713_create_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tender', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'status' => $this->integer(),
            'number' => $this->string(255),
            'procedure_name' => $this->string(255),
            'customer' => $this->string(255),
            'term' => $this->date(),
            'users_id' => $this->integer(),
            'client_id'=>$this->integer(),
        ]);
        $this->createIndex('idx-tender-client_id', 'tender', 'client_id', false);
        $this->addForeignKey("fk-tender-client_id", "tender", "client_id", "client", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-tender-client_id','tender');
        $this->dropIndex('idx-tender-client_id','tender');
        
        $this->dropTable('tender');
    }
}
