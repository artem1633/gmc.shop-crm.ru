<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_setting}}`.
 */
class m191002_143842_create_user_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_setting}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'dashboard_info' => $this->integer()->comment('Визуальный вид рабочего стола'),
        ]);

        $this->createIndex(
            'idx-user_setting-user_id',
            'user_setting',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_setting-user_id',
            'user_setting',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_setting-user_id',
            'user_setting'
        );

        $this->dropIndex(
            'idx-user_setting-user_id',
            'user_setting'
        );

        $this->dropTable('{{%user_setting}}');
    }
}
