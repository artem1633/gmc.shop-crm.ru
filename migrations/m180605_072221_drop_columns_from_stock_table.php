<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `columns_from_stock`.
 */
class m180605_072221_drop_columns_from_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-stock-tovar_id','stock');
        $this->dropIndex('idx-stock-tovar_id','stock');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('idx-stock-tovar_id', 'stock', 'tovar_id', false);
        $this->addForeignKey("fk-stock-tovar_id", "stock", "tovar_id", "tovars", "id");
    }
}
