<?php

use yii\db\Migration;

/**
 * Handles the creation of table `stock`.
 */
class m180518_173213_create_stock_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('stock', [
            'id' => $this->primaryKey(),
            'manufacturer' => $this->string(255),
            'name_goods' => $this->string(255),
            'availability_stock' => $this->integer(),
            'presence_branch' => $this->integer(),
            'reserve' => $this->integer(),
            'order' => $this->integer(),
            'product_image' => $this->string(255),
            'short_description' => $this->text(),
            'specifications' => $this->text(),
            'full_description' => $this->text(),
            'cost_goods' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('stock');
    }
}
