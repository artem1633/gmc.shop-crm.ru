<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auction_price}}`.
 */
class m191002_194458_create_auction_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auction_price}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%auction_price}}');
    }
}
