<?php

use yii\db\Migration;

/**
 * Handles adding closeopen to table `products`.
 */
class m180909_034154_add_closeopen_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'closeopen', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'closeopen');
    }
}
