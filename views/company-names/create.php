<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyNames */

?>
<div class="company-names-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
