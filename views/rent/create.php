<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rent */

?>
<div class="rent-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
