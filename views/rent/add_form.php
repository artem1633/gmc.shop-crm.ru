<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Rent */
/* @var $form yii\widgets\ActiveForm */
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="rent-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row" >
        <img style="width: 100%;margin-top: -50px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3">
            <?= $form->field($model, 'start_date_lease')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy', 
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name_equipment')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'client_id')->dropDownList($model->getClients(), ['prompt' => 'Выберите', 'disabled' => $disabled ]); ?>       
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'stage_transaction')->dropDownList($model->getStageTransactions(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'term_lease')->textInput(['maxlength' => true]) ?> 
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'rent_price')->textInput(['type' => 'number']) ?>        
        </div>
         <div class="col-md-3">
            <?= $form->field($model, 'paid_amount')->textInput(['type' => 'number']) ?>        
        </div>
    </div>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</body>
