<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rent */
?>
<div class="rent-update">

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
