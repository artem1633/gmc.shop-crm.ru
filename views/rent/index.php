<?php
use yii\helpers\Url;
use yii\helpers\Html; 
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */ 

$this->title = "Аренда";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this); 

?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="panel panel-inverse rents-index leftindex">
    <div class="panel-heading" style="background: #28292b">
        <div class="btn-group pull-right" >
            <?php if($archive == 0){?>
             <button type="button"  class="btn btn btn-xs arxiv">Все</button>
            <?php } elseif($archive == 1){?>
             <button type="button" class="btn btn btn-xs arxiv">Только активные</button>
            <?php } elseif($archive == 2){?>
             <button type="button" class="btn btn btn-xs arxiv">Только архивные</button>
            <?php }else{?>
            <button type="button" class="btn btn btn-xs arxiv">Сортировать</button>
            <?php }?>
            <button type="button" class="btn btn btn-xs dropdown-toggle  dropdown1"  data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu"  >
                <li class="rabsize2" style="width :200px;text-align-last: left;"> <?= $archive == 0 ? Html::a('Все', ['index', 'archive' => 0],[ 'data-pjax' => 0,]) : Html::a('Все', ['index','archive' => 0],[ 'data-pjax' => 0,])?></li>
                <li class="rabsize2" style="width :200px;text-align-last: left;"> <?= $archive == 1 ? Html::a('Только активные', ['index', 'archive' => 1],[ 'data-pjax' => 0,]) : Html::a('Только активные', ['index','archive' => 1],[ 'data-pjax' => 0,])?></li>
                <li class="rabsize2" style="width :200px;text-align-last: left;"> <?= $archive == 2 ? Html::a('Только архивные', ['index', 'archive' => 2],[ 'data-pjax' => 0,]) : Html::a('Только архивные', ['index','archive' => 2],[ 'data-pjax' => 0,])?></li>
            </ul>
        </div>
        <h4 class="panel-title rabsize3 left1">Аренда</h4>
    </div>
    <div class="panel-body">
        <?= Html::a('Добавить <i class="fa fa"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 35px','class'=>'btn btn']) ?>
        <?= Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px', 'class'=>'btn btn', 'title'=>'Обновить']) ?>
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>  '',
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>
</body>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
