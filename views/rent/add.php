<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rent */
?>
<div class="rent-update">

    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>

</div>
