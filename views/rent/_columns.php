<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\RentEtap;
use yii\helpers\Html; 
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'start_date_lease',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->start_date_lease, 'php:d.m.Y');
        },    
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name_equipment',
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'stage_transaction',
        'filter' => ArrayHelper::map(RentEtap::find()->all(),'id','name'),
        'content' => function ($data) {
            return $data->stageTransaction->name;
        },
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'serial_number',
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'content' => function($data){
            return $data->client->working_title;
        },
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
        // [
        //     'class'=>'\kartik\grid\DataColumn',
        //     'attribute'=>'name_lessee_company',
        // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'term_lease',
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
        'content' => function ($data) {
            $qoldiq = (integer)(strtotime($data->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $data->term_lease ;
            $kun = (integer)($qoldiq/86400);
            return $kun;
        },
        'contentOptions' => function ($model, $key, $index, $column) {
            $qoldiq = (integer)(strtotime($model->start_date_lease)-strtotime(date('Y-m-d')));
            $qoldiq += 86400 * $model->term_lease ;
            $kun = (integer)($qoldiq/86400);
            if($kun>0&&$kun<=3)return ['style' => 'color:black;background-color:yellow' ];
            if($kun<=0)return ['style' => 'color:black;background-color:red' ];
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'rent_price',
        'format' => ['currency', 'rub'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{up} {view} {update} {delete}', 
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данную позицию?'], 
        'buttons'  => [            
            'up' => function ($url, $model) { 
                $url = Url::to(['load', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
        ]
    ],

];   