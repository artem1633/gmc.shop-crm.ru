<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rent */
?>
<div class="rent-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                    'attribute' => 'start_date_lease',
                    'value' => function ($data) {
                     if($data->start_date_lease != null )  return \Yii::$app->formatter->asDate($data->start_date_lease, 'php:d.m.Y');
                    },
            ],
            'name_equipment',
            'serial_number',
            [
                'attribute' => 'client_id',
                'value' => function ($data) {
                  return $data->client->working_title;
                },
            ],
            'term_lease',

            [
                'attribute' => 'rent_price',
                'format' => ['currency', 'rub'],
            ],
            [
                'attribute' => 'paid_amount',
                'format' => ['currency', 'rub'],
            ],
            'paid_amount',
            'stage_transaction',
        ],
    ]) ?>

</div>
