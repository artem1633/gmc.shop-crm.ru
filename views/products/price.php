<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Products */

?>
<div class="products-create">
    <?= $this->render('add_price', [
        'model' => $model,
    ]) ?>
</div>
