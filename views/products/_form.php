<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */ 
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-4">
            <?= $form->field($model, 'manufacturer_id')->dropDownList($model->getManufacturerList(), ['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'category_id')->dropDownList($model->getCategorys(), ['prompt' => 'Выберите', 'id' => 'type']) ?>     
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-4">
            <?= $form->field($model, 'cost')->textInput(['type' => 'number']) ?>
            <?= $form->field($model, 'is_request_cost')->checkbox() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'completeness')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'nds_protsent')->dropDownList($model->getNds(), ['prompt' => 'Выберите', 'id' => 'type']) ?>     
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'specifications')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'full_description')->textarea(['rows' => 5]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'file')->fileInput(['style' => 'margin-top:5px;']) ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

$script = <<< JS
    $('#products-is_request_cost').change(function(){
        if($(this).is(':checked')) {
            $('#products-cost').val('');
            $('#products-cost').attr('disabled', 'disabled');
        } else {
            $('#products-cost').removeAttr('disabled');
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
