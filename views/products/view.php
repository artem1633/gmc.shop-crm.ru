<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
?>
<div class="products-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'manufacturer_id',
                'value' => function($data){
                    return $data->manufacturer->name;
                }
            ],
            [
                    'attribute' => 'datecreate',
                    'value' => function ($data) {
                     if($data->datecreate != null )  return \Yii::$app->formatter->asDate($data->datecreate, 'php:H:i:s d.m.Y');
                    },
            ],
            'name',
            [
                'attribute' => 'cost',
                'format' => ['currency', 'rub'],
            ],
            [
                'attribute' => 'sum',
                'format' => ['currency', 'rub'],
            ],
            'product_image',
            'short_description',
            'specifications',
            'full_description',
            [
                'attribute' => 'category_id',
                'value' => function($data){
                    return $data->category->name;
                }
            ],
            [
                'attribute' => 'nds_protsent',
                'value' => function($data){
                    return $data->getNdsDescription();
                }
            ],
            'nds_sum',
            'total_sum',
        ],
    ]) ?>

</div>
