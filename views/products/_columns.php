<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;

$tip = Users::find()->where(['id' =>\Yii::$app->user->identity->id])->one();
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'datecreate',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->datecreate, 'php:H:i:s d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'product_image',
        'contentOptions' => ['style'=>'width: 80px;'],
        'content' => function ($data) {
            return '<center>' . Html::img('/uploads/'.$data->product_image, [ 'style' => 'height:80px;width:80px;' ]) . '</center>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_id',
        'content' => function($data){
            return $data->category->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'manufacturer_id',
        'content' => function($data){
            return $data->manufacturer->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost',
//        'format' => ['currency', 'rub'],
        'content' => function($data){
            if($data->is_request_cost == 1){
                return '<i>(Цена по запросу)</i>';
            }

            return Yii::$app->formatter->asCurrency($data->cost,'rub');
        },
        'pageSummary' => true,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'copyprice',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{close}&nbsp {change_price}&nbsp {view}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'buttons' => [ 
            'close' => function ($url, $model) {
                if(Yii::$app->user->identity->type == 1)
                {
                    $url = Url::to(['close', 'id' => $model->id]);
                    return Html::a('<span class="fa fa-sign-out"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                }
            },
            'change_price' => function ($url, $model) { 
                if(Yii::$app->user->identity->type == 1)
                {
                    $url = Url::to(['price', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                }
                else
                {
                    if($model->closeopen == 0 || $model->closeopen == null)
                    {
                        $url = Url::to(['price', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-usd"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                    }                    
                }
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{update}&nbsp {delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'buttons' => [ 
            'update' => function ($url, $model) {
                if(Yii::$app->user->identity->type == 1)
                {
                    $url = Url::to(['update', 'id' => $model->id]);
                      return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                }
                else
                {
                    if($model->closeopen == 0 || $model->closeopen == null)
                    {
                        $url = Url::to(['update', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                    }                                
                }
            },
        ],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Подтвердите действие',
            'data-confirm-message'=>'Вы уверены что хотите удалить данного продукта?'], 
    ],

];   