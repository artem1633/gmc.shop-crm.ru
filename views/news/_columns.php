<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Users;
use yii\helpers\Html; 
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'width' => '100px',
        'attribute'=>'image',
        'content' => function ($data) {
            return '<a>'.($data->image)?Html::img('/uploads/'.$data->image,['style'=>'width:130px; height:150px;text-align:center']):null.'</a>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format'=>'html', 
        'attribute'=>'text_post',
        'content' => function ($data) {
            $title = substr($data->text_post, 0, 1200) . '...';
            return '<span class="" style="font-size:10px;">'. Html::a( $title, ['view','id' =>$data->id], ['title' => 'Просмотр', 'style' => 'color:#000000;font-size:12px','role'=>'modal-remote',]) . '</span>';
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{up} {view} {update} {delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного новости?'], 
        'buttons'  => [            
            'up' => function ($url, $model) {
                $url = Url::to(['up', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
        ]
    ],

];   