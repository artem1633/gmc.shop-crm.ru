<?php

use yii\widgets\DetailView;
use yii\helpers\Html; 
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\News */
?>
<div class="panel panel-inverse">
    <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <div class="row" style="margin-top:-30px">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
                                <div class="col-md-4 col-sm-4">
                                    <div class="table-responsive">                                    
                                        <table class="table table" style="margin-top: -20px">
                                                 <h3 style="margin-top:0px"></h3>
                                                    <tbody >
                                                        <td rowspan="4" width="280px" height="190px style="text-align: center;"><?=
                                                            ($model->image)?Html::img('/uploads/'.$model->image,['style'=>'width:210px; height:260px']):null;
                                                            ?>      
                                                        </td>                                      
                                                    </tbody>
                                        </table>
                                        <table class="" style="border: 0px;width: 220px;">
                                                    <tbody>
                                                        <tr >
                                                            <td height="40px"><b><?=$model->getAttributeLabel('attached_file')?></b></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="20px"><?=Html::a($model->attached_file, ['/news/send-file', 'file' => $model->attached_file],['title'=> 'Скачать', 'data-pjax' => 0]);?></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="40px"><b><?=$model->getAttributeLabel('date')?></b></td>
                                                        </tr>
                                                        <tr>
                                                            <td height="10px"><?=Html::encode(\Yii::$app->formatter->asDate($model->date, 'php:d / m / Y'))?></td>
                                                        </tr>
                                                        
                                                    </tbody>
                                        </table>    
                                    </div>
                                </div>   
                                <div class="col-md-8">
                                    <div class="table-responsive">                                    
                                        <table class="" style="border: 0px;" >
                                                    <tbody>
                                                        <tr>
                                                             <td ><?=Html::decode($model->text_post)?></td>
                                                        </tr>
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="table-responsive">                                    
                                        
                                    </div>
                                </div>
                                <?php Pjax::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
