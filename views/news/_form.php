<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
 
/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?> 
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="news-form"> 

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
     <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
         <div class="col-md-2 labelnew">
            <?=$model->getAttributeLabel('header')?>
        </div>
        <div class="col-md-10" style="margin-top: -20px">
            <?= $form->field($model, 'header')->textInput(['maxlength' => true])->label(""); ?>        
        </div>
    </div>
     <div class="row">
         <div class="col-md-12">
                    <?php echo $form->field($model, 'text_post')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline' => false, //по умолчанию false
                            'height' => '200px',
                        ],
                    ]);
                    ?>
                </div>
    </div>
     <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'other_image')->fileInput() ?>            
        </div>
         <div class="col-md-4">
            <?= $form->field($model, 'other_attached_file')->fileInput() ?>            
        </div>
    </div>
     <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'fixing')->checkbox(); ?>
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'arxiv')->textInput(['value' => 0]) ?>
        <?= $form->field($model, 'user_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>        
    </div>
    
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</body>