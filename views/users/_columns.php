<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'filter' => array('1' => 'Администратор' , '2' => 'Руководство', '3' => 'Главный менеджер', '4' => 'Менеджер', '5' => 'Поверитель', '6' => 'Бухгалтер', '7' => 'Начальник склада', '8' => 'Тендерный специалист', '9' => 'Водитель', '10' => 'Технический специалист'),
        'content' => function ($data) {
                   if($data->type == 1) return 'Администратор';
                   if($data->type == 2) return 'Руководство';
                   if($data->type == 3) return 'Главный менеджер';
                   if($data->type == 4) return 'Менеджер';
                   if($data->type == 5) return 'Поверитель';
                   if($data->type == 6) return 'Бухгалтер';
                   if($data->type == 7) return 'Начальник склада';
                   if($data->type == 8) return 'Тендерный специалист';
                   if($data->type == 9) return 'Водитель';
                   if($data->type == 10) return 'Технический специалист';
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dostup',
        'filter' => array('1' => 'Вкл' , '2' => 'Выкл'),
        'content' => function ($data) {
           if($data->dostup == 1) return 'Вкл';
           if($data->dostup == 2) return 'Выкл';
       },
       'width'=>'150px',
    ], 
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'<span style="color:red;" class="deluser"><center>Вы действительно хотите удалить этого пользователя. Если вы удалите этого пользователя то удаляется все прикреплённые к нему данние</center></span>'], 
    ],

];   