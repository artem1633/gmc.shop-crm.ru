<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-12"> 
             <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
     </div>
     <div class="row">
        <div class="col-md-6"> 
             <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
             <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
     </div>
     <div class="row">
        <div class="col-md-6">
             <?= $form->field($model, 'type')->dropDownList($model->getType(), ['prompt' => 'Выберите должность']) ?>
        </div>
        <div class="col-md-6">
             <?= $form->field($model, 'dostup')->dropDownList($model->getDostup(), ['prompt' => 'Выберите']) ?>
        </div>
     </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
   </div>
</div>
