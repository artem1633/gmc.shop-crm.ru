<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body > 
<div class="users-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-4 label1 " style="margin-top: -0px">
            <?=$model->getAttributeLabel('fio');?>
        </div>
        <div class="col-md-8 change"> 
             <?= $form->field($model, 'fio')->textInput(['maxlength' => true])->label(""); ?>
        </div>
        <div class="col-md-4 label1 " style="margin-top: -0px">
            <?=$model->getAttributeLabel('login');?>
        </div>
        <div class="col-md-8 change"> 
             <?= $form->field($model, 'login')->textInput(['maxlength' => true])->label(""); ?>
        </div>
        <div class="col-md-4 label1 " style="margin-top: -0px">
            <?=$model->getAttributeLabel('new_password');?>
        </div>
        <div class="col-md-8 change"> 
             <?= $form->field($model, 'new_password')->textInput(['maxlength' => true])->label(""); ?>
        </div>
     </div>


    <?php ActiveForm::end(); ?>

</div>
</body>