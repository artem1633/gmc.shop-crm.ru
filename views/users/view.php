<?php

use yii\widgets\DetailView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fio',
            'login',
            [
                'attribute' => 'type',
                'value' => function ($data) {
                   if($data->type == 1) return 'Администратор';
                   if($data->type == 2) return 'Руководство';
                   if($data->type == 3) return 'Главный менеджер';
                   if($data->type == 4) return 'Менеджер';
                   if($data->type == 5) return 'Поверитель';
                   if($data->type == 6) return 'Бухгалтер';
                   if($data->type == 7) return 'Начальник склада';
                   if($data->type == 8) return 'Тендерный специалист';
                   if($data->type == 9) return 'Водитель';
                   if($data->type == 10) return 'Технический специалист';
                },
            ],
            [
                'attribute' => 'dostup',
                'value' => function ($data) {
                   if($data->type == 1) return 'Вкл';
                   if($data->type == 2) return 'Выкл';
                },
            ],
        ],
    ]) ?>

</div>
