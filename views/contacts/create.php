<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contacts */

?>
<div class="contacts-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
