<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
?>

<div class="call-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row"> 
        <img style="width: 100%;margin-top: -60px;" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-6">
           <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
           <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>            
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-6">
           <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>           
        </div>
         <div class="col-md-6">
           <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>           
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'client_id')->textInput() ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
