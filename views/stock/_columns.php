<?php
use yii\helpers\Url;
use app\models\Stock;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_id',
        'filter' => Stock::getCategories(),
        'content' => function($data){
            return $data->category->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tovar_id',
        'filter' => Stock::getAllProduct(),
        'content' => function($data){
            return $data->tovar->name;
        }
    ],
    [
        'attribute' => 'chekt',
        'filter'=>array("0"=>"Нет","1"=>"Да"),
        'value' => function ($data) {
            return \yii\helpers\ArrayHelper::map([
                        ['id' => 0,
                            'title' => 'Нет',],
                        ['id' => 1,
                            'title' => 'Да',],
                    ], 'id', 'title')[$data->chekt];
            }
        ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'cost_goods',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'availability_stock',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'presence_branch',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'reserve',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template'=>'{view}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>[/*'role'=>'modal-remote',*/'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного склад?'],
    ],

];   