<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Stock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-4">
            <?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'name_goods')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'availability_stock')->textInput() ?>       
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'presence_branch')->textInput() ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'reserve')->textInput() ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'order')->textInput() ?>       
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cost_goods')->textInput(['type' => 'number']) ?>        
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'short_description')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'specifications')->textarea(['rows' => 4]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'full_description')->textarea(['rows' => 4]) ?>
        </div>
    </div>
    <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'other_file')->fileInput() ?>            
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'chekt')->checkbox(); ?>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
