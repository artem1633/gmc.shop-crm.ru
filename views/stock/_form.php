<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Stock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stock-form">
 
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3">
            <?= $form->field($model, 'manufacturer_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getManufacturerList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'category_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getCategories(),
                'options' => [
                    'placeholder' => 'Выберите',
                    'onchange'=>'
                        $.post( "list-products?id='.'"+$(this).val(), function( data ){
                            $( "select#tttt" ).html( data);
                        });' 
                    ],
                'pluginOptions' => [ 
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tovar_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getProductsList($model->category_id),
                'options' => [
                    'placeholder' => 'Выберите',
                    'id' => 'tttt',
                    ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cost_goods')->textInput(['type'=>'number']) ?>       
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'availability_stock')->textInput(['type'=>'number']) ?>       
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'presence_branch')->textInput(['type'=>'number']) ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'reserve')->textInput(['type'=>'number']) ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'order')->textInput(['type'=>'number']) ?>       
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'chekt')->checkbox(); ?>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
