<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\HtmlPurifier;
 

\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Склад';

?>
    
<div class="panel panel-inverse">
    <div class="panel-heading" style="background: #28292b">
        <h2 class="panel-title rabsize3" style="margin-left: 20px">
            Склад № <?=$model->id?>
        </h2>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="tab-pane fade active in">
                        <div class="row" style="margin-top:-30px">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
                                <div class="col-md-4 col-sm-4">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                            <h3 style="text-align: left;">Информация <a   class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['stock/update', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a></h3>
                                                 <h3 style="margin-top:0px"></h3>
                                                    <tbody >
                                                        <td rowspan="4" width="320px" height="230px style="text-align: center;"><?=
                                                            ($model->tovar->product_image)?Html::img('/uploads/'.$model->tovar->product_image,['style'=>'width:280px; height:280px']):null;
                                                            ?>      
                                                        </td>                                      
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>   
                                <div class="col-md-8 col-sm-8">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                            <h3 style="margin-top:63px"></h3>
                                                    <tbody>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('tovar.manufacturer_id')?></b></th>
                                                            <th><b><?=$model->getAttributeLabel('tovar.name')?></b></th>
                                                            <th><b><?=$model->getAttributeLabel('availability_stock')?></b></th>
                                                              <th><b><?=$model->getAttributeLabel('presence_branch')?></b></th>
                                                        </tr>
                                                        <tr >
                                                            <td><?=Html::encode($model->tovar->manufacturer->name)?></td>
                                                            <td><?=Html::encode($model->tovar->name)?></td>        
                                                            <td><?=Html::encode($model->availability_stock)?></td>
                                                            <td ><?=Html::encode($model->presence_branch)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('reserve')?></b></th>
                                                            <th><b><?=$model->getAttributeLabel('order')?></b></th>
                                                            <th><b><?=$model->getAttributeLabel('cost_goods')?></b></th>
                                                             <th><b><?=$model->getAttributeLabel('category_id')?></b></th>
                                                        </tr>
                                                        <tr>
                                                            <td><?=Html::encode($model->reserve)?></td>
                                                            <td><?=Html::encode($model->order)?></td>
                                                            <td><?=Html::encode($model->cost_goods)?></td>
                                                            <td><?=Html::encode($model->category->name)?></td>
                                                        </tr>
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>
                                 <div class="col-md-8 col-sm-8">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                                 <h3 style="margin-top:-10px"></h3>
                                                    <tbody>
                                                        <tr>
                                                            <th ><b><?=$model->getAttributeLabel('tovar.full_description')?></b></th>
                                                        </tr>
                                                    
                                                        <tr>
                                                            <td height="106"><?=Html::encode($model->tovar->full_description)?></td>
                                                                                                             
                                                        </tr>
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-4">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                        
                                                    <tbody>
                                                        <tr>
                                                            <th ><b><?=$model->getAttributeLabel('tovar.specifications')?></b></th>
                                                        </tr>
                                                        <tr>
                                                            <td height="80"><?=Html::encode($model->tovar->specifications)?></td>
                                                        </tr>
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-4">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                        
                                                    <tbody>
                                                        <tr>
                                                            <th ><b><?=$model->getAttributeLabel('tovar.short_description')?></b></th>
                                                        </tr>
                                                        <tr>
                                                             <td height="80 "><?=Html::encode($model->tovar->short_description)?></td>
                                                        </tr>
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>
                                <?php Pjax::end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>