<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Documentstype */
?>
<div class="documentstype-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
