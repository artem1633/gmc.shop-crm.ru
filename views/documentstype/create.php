<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Documentstype */

?>
<div class="documentstype-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
