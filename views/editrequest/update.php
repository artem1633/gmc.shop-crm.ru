<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Editrequest */
?>
<div class="editrequest-update">

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
