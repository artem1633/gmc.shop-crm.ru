<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Editrequest */
?> 
<div class="editrequest-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
            'attribute'=>'description',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
            [
                'attribute' => 'client_id',
                'value' => function ($data) {
                  return $data->client->working_title;
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                   if($data->status == 1) return 'Новый';
                   if($data->status == 2) return 'В работе';
                   if($data->status == 3) return 'Готово';
                },
            ],
        ],
    ]) ?>

</div>
