<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Editrequest */

?>
<div class="editrequest-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
