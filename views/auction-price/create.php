<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AuctionPrice */

?>
<div class="auction-price-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
