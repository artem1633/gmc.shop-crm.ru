<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RentEtap */
?>
<div class="rent-etap-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
        ],
    ]) ?>

</div>
