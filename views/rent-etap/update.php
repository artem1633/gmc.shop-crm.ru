<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RentEtap */
?>
<div class="rent-etap-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
