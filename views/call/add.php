<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Call */

?>
<div class="call-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
