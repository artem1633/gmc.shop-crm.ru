<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */ 
/* @var $model app\models\Call */
/* @var $form yii\widgets\ActiveForm */
?>
 <head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body > 
<div class="call-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-6">
            <?= $form->field($model, 'contact_id')->dropDownList($model->getContactList($model->client_id), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'next_contact')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
            
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status_call_id')->dropDownList($model->getStatusList(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
    </div>
    <div class="row">
     <div class="col-md-12">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>            
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'date')->textInput(['value' => date('H:i d-m-Y')]) ?>
        <?= $form->field($model, 'client_id')->textInput() ?>       
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
</body>