<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Call */
?>
<div class="call-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'contact_id',
            'description',
            'users_id',
            'next_contact',
            'status_call_id',
            'client_id',
        ],
    ]) ?>

</div>
