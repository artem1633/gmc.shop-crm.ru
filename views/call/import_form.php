<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
 <?php $form = ActiveForm::begin(['options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->label('<span class="btn btn-primary btn-sm">Загрузить excel файл</span>',[])->fileInput(['class'=>'sr-only']) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>