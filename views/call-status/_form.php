<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CallStatus */
/* @var $form yii\widgets\ActiveForm */
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body > 
<div class="call-status-form">

    <?php $form = ActiveForm::begin(); ?>

	 <div class="row">
	 	<img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
	        <div class="col-md-12"> 
  				  <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	        </div>
     </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</body>
