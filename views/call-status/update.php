<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CallStatus */
?>
<div class="call-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
