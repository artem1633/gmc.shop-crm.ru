<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */ 
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<head>
    
</head>
<body style="background: #e8e8e8">
<div class="login animated fadeInDown login1" >
    <!-- begin brand -->
    <div class="login-header center">
        <img class="image" style="height: 85px; width: 321px;" src="/img/logo-new.png" alt="">
    </div>
    <!-- end brand -->
    <div class="login-content">
        <div class="bottom" style="margin-left: 30px;margin-top: -10px"">
            <b>Введите данные для авторизации</b>
        </div>
        <div class="row" style="margin-left: 20px;margin-top: 20px">
            <div class=" col-md-11 ">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg inverse-mode no-border email']) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border email']) ?>
        </div>
    </div>
        <div class="row" style="margin-left: 20px;">
            <div class="admin1 col-md-6 ">
                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомни меня') ?>
            </div>
        <div class="col-md-11" class="login-buttons" >
            <?= Html::submitButton('Вход', ['class' => 'btn btn btn-block btn-lg rabsize3 vxod', 'name' => 'login-button']) ?>
        </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

