<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div >

    <!-- begin error -->
    <div class="" style="margin-top: 150px">
        <div class="error m-b-10" style="color: #74b916;font-size: 50px" ><?=$exception->name?> <i class="fa fa-warning"></i></div>
        <div class="error" >
            <!-- <div class=""><?=$message?></div> -->
            <div class="error-desc m-b-20 rabsize2" style="color: #000000">
                Если это ошибка сервера. <br>
                Свяжитесь с тех поддержкой.
            </div>
            <div >
                <a href="/" class="btn btn save">На главную</a>
            </div>
            <div style="margin-top: 150px"></div>
        </div>
    </div>
    <!-- end error -->

</div>
</body>