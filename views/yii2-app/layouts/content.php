<?php

use yii\widgets\Breadcrumbs;

?>

<div id="page-loader" class="fade in "><span class="spinner"></span></div>
<section class="content " style="margin-top: 20px;" id="content">

    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success fade in m-b-15">
            <strong>Успех!</strong>
            <?=Yii::$app->session->getFlash('success')?>
            <span class="close" data-dismiss="alert">×</span>
        </div>
    <?php endif; ?>

    <?php if(Yii::$app->session->hasFlash('info')): ?>
        <div class="alert alert-info fade in m-b-15">
            <strong>Уведомление!</strong>
            <?=Yii::$app->session->getFlash('info')?>
            <span class="close" data-dismiss="alert">×</span>
        </div>
    <?php endif; ?>

    <?php if(Yii::$app->session->hasFlash('warning')): ?>
        <div class="alert alert-warning fade in m-b-15">
            <strong>Внимание!</strong>
            <?=Yii::$app->session->getFlash('warning')?>
            <span class="close" data-dismiss="alert">×</span>
        </div>
    <?php endif; ?>

    <?php if(Yii::$app->session->hasFlash('danger')): ?>
        <div class="alert alert-danger fade in m-b-15">
            <strong>Ошибка!</strong>
            <?=Yii::$app->session->getFlash('danger')?>
            <span class="close" data-dismiss="alert">×</span>
        </div>
    <?php endif; ?>

    <?php if (isset($this->blocks['content-header'])) { ?>
        <h1 class="page-header">Basic Tables</h1>
    <?php } else { ?>
    <?php } ?>
    
    <?= $content ?>
</section>

