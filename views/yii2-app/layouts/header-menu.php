<?php

use yii\helpers\Url;

$tip = 0;
if (Yii::$app->user->identity) $tip = Yii::$app->user->identity->type;

?>

    <!-- <div id="top-menu" class="top-menu">
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
//                    ['label' => 'Рабочий стол', 'icon' => 'fa fa-dashboard', 'url' => ['site/dashboard'], ],
                    ['label' => 'Клиенты', 'icon'=>'fa fa-group' ,'url' => ['client/index'], ],
                    ['label' => 'Органайзер', 'icon' => 'fa fa-ticket', 'url' => ['tasks/index'], ],
                    ['label' => 'Заказы', 'icon'=>'fa fa-shopping-cart' , 'url' => ['orders/index'], ],
                    ['label' => 'Документы','icon'=>'fa fa-file-text-o' ,  'url' => ['documentation/index'], ],
                    ['label' => 'Тендер', 'icon'=>'fa fa-spinner' , 'url' => ['tender/index'], ],
                    ['label' => 'Поверки', 'icon'=>'fa fa-clipboard' , 'url' => ['verification/index'], ],
                    ['label' => 'Аренда', 'icon' => 'fa fa-fire', 'url' => ['rent/index'], ],
                    ['label' => 'Склад', 'icon' => 'fa fa-bars', 'url' => ['stock/index'], ],
                    ['label' => 'Список б/у', 'icon' => 'fa fa-bar-chart-o', 'url' => ['list-used/index'], ],
                    ['label' => 'Логистика',  'icon' => 'fa fa-tag','url' => ['logistics/index'], ],
                    ['label' => 'Закупки у поставщиков','icon'=>'fa fa-chain' ,  'url' => ['purchasing-suppliers/index'], ],
                    ['label' => 'Отчёты','icon' => 'fa fa-th-list', 'url' => ['#'],  ],
                    ['label' => 'Статистика','icon' => 'fa fa-list-ul', 'url' => ['#'],  ],
                    ['label' => 'Новости ','icon' => 'fa fa-chain-broken', 'url' => ['/news/index'],  ],
                    [
                        'label' => 'Настройки',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'options' => ['class' => 'has-sub'],
                        'items' => [
                            ['label' => 'Пользователи','icon' => 'fa fa-users', 'url' => ['users/index'],'visible' => ($tip == 2) ],
                            ['label' => 'Клиенты', 'options' => ['class' => 'has-sub'], 'url' => '#', 'items' => [
                                ['label' => 'Города','icon' => 'fa fa-bank', 'url' => ['city/index'],  ],
                                ['label' => 'Тип документа ','icon' => 'fa fa-file-text-o', 'url' => ['/documentstype/index'],  ],
                                ['label' => 'Этапы aренды','icon' => 'fa fa-file-text', 'url' => ['/rent-etap/index'],  ],
                                ['label' => 'Производитель','icon' => 'fa fa-puzzle-piece', 'url' => ['manufacturer/index'],  ],
                                ['label' => 'Форма заявления','icon' => 'fa fa-puzzle-piece', 'url' => ['form-action/index'],  ],
                                ['label' => 'Приборы','icon' => 'fa fa-gavel', 'url' => ['devices/index'],  ],
                            ]],
                            ['label' => 'Этапы документов','icon' => 'fa fa-file-text-o', 'url' => ['stages-document/index'],  ],
                            ['label' => 'Статус звонков','icon' => 'fa fa-phone', 'url' => ['call-status/index'],  ],
                            ['label' => 'Актуальность клиента', 'icon' => 'fa fa-user','url' => ['relevance-client/index'],  ],
                            ['label' => 'Настройки','icon' => 'fa fa-gear', 'url' => ['setting/index'],  ],
                            ['label' => 'Продукты','icon' => 'fa fa-puzzle-piece', 'url' => ['products/index'],  ],
                            ['label' => 'Запрос на редактирование','icon' => 'fa fa-list', 'url' => ['editrequest/index'],  ],
                        ],
                    ],

                ],
            ]
        );
        ?>
    </div> -->