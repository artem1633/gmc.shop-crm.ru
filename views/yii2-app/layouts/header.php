<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use app\models\Editrequest; 
use app\models\Organizer; 
use app\models\Notification;
use app\models\Users; 
use app\models\Tasks;

CrudAsset::register($this);
$tip=Yii::$app->user->identity->type;
$allrequest = Editrequest::find()->where(['status' => 1])->all();
$total_count = Editrequest::find()->where(['status' => 1])->count();
$fio = Users::find()->where(['id' =>\Yii::$app->user->identity->id])->one();
$notifications = Notification::find()->where(['table_name' => 'tasks', 'user_id' => Yii::$app->user->identity->id])->all();
$count = Notification::find()->where(['table_name' => 'tasks', 'user_id' => Yii::$app->user->identity->id])->count();
$color_count = Notification::find()->where(['status'=>0, 'table_name' => 'tasks', 'user_id' => Yii::$app->user->identity->id])->count();

function isActive($name){
    $current = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;

    if(is_array($name) == false){
        return $current == $name ? 'active' : '';
    } else {
        foreach ($name as $url){
            if($url == $current){
                return 'active';
            }
        }

        return '';
    }
}

?> 
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
    
</head>
<body>
<div id="ajaxCrudDatatable">
<div id="sidebar" class="sidebar" style="padding-top: 15px;">
    <!-- begin container-fluid -->
    <div class="container-fluid" style="margin-top: 20px; padding-left: 0; padding-right: 0;">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=\Yii::$app->homeUrl?>">  <img class="imgrab" style="margin-left: 28px;" src="/img/logo-new.png" alt=""></a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled" data-toggle="collapse" data-target="#my_navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <div class="collapse navbar-collapse" id="my_navbar">
        <ul class="nav navbar-nav" >
            <li style="display: none;">
               <a style="" class="home home1 colortab" href="<?=\Yii::$app->homeUrl?>"> Рабочий стол</a>
            </li>
            <li class="dropdown">
                <a style="margin-left: 0;  color: #ffffff;" class="home home1 <?= isActive([
                        'orders/index',
                        'documentation/index',
                        'tender/index',
                        'verification/index',
                        'rent/index',
                        'stock/index',
                        'list-used/index',
                        'logistics/index',
                        'purchasing-suppliers/index',
                ]) ?>" ><span><i style="font-size: 14px; margin-right: 5px;" class="fa fa-caret-down"></i> Данные</span></a>
                <div class="dropdown-content rabsize4" >
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==10){?>
                  <?php }?>
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==7){?>
                  <a class="unga" href="<?=Url::toRoute(['/orders/index', ])?>">Заказы</a>
                  <?php }?>
                  <a class="unga" href="<?=Url::toRoute(['/documentation/index', ])?>">Документы</a>
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==10){?>
                  <a class="unga" href="<?=Url::toRoute(['/tender/index', ])?>">Тендер</a>
                  <?php }?>
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==5){?>
                  <a class="unga" href="<?=Url::toRoute(['/verification/index', ])?>">Поверки</a><?php }?>
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==7){?>
                  <a class="unga" href="<?=Url::toRoute(['/rent/index', ])?>">Аренда</a>
                  <?php }?>
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==7){?>
                  <a class="unga" href="<?=Url::toRoute(['/stock/index', ])?>">Склад</a>
                  <?php }?>
                  <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8){?>
                  <a class="unga" href="<?=Url::toRoute(['/list-used/index', ])?>">Список б/у</a><?php }?>
                  <a class="unga" href="<?=Url::toRoute(['/logistics/index', ])?>">Логистика</a>
                  <a class="unga" href="<?=Url::toRoute(['/purchasing-suppliers/index', ])?>">Закупки у поставщиков</a>
                  <a class="unga" href="<?=Url::toRoute(['#', ])?>">Отчёты</a>
                </div>
            </li>
            <?php if($tip==1||$tip==2||$tip==3||$tip==4||$tip==6||$tip==8||$tip==10){?>
                <li style="display: block; text-align: left;">
                    <a class="home home1 <?= isActive("client/index") ?>" style="margin-left: 0;" href="<?=Url::toRoute(['/client/index', ])?>"> Клиенты</a>
                </li>
                <li style="display: block; text-align: left;">
                    <a class="home home1 <?= isActive("tasks/index") ?>" style="margin-left: 0;" href="<?=Url::toRoute(['/tasks/index', ])?>"> Органайзер</a>
                </li>
            <?php }?>
            <?php if($tip==1){?>
            <li class="dropdown ">
                <a style="margin-left: 0; color: #ffffff;" class="home home1 <?= isActive([
                    'users/index',
                    'stages-document/index',
                    'call-status/index',
                    'relevance-client/index',
                    'city/index',
                    'documentstype/index',
                    'rent-etap/index',
                    'manufacturer/index',
                    'form-action/index',
                    'placement/index',
                    'waiting-stages/index',
                    'company-names/index',
                    'auction-price/index',
                    'devices/index',
                    'products/index',
                    'category/index',
                    'settings/index',
                    'editrequest/index',
                    'template/index',
                    'statistic/interval',
                    'statistic/manager',
                ]) ?>">Настройки<i style="margin-right: 5px; font-size: 14px" class="fa fa-caret-down"></i></a>
                <div class="dropdown-content rabsize4" style="max-height: 500px; overflow-x: scroll;">
                  <?php if($tip==1){ ?>
                  <a class="unga" href="<?=Url::toRoute(['/users/index', 'id' => $model->id])?>">Пользователи</a>
                  <?php }?>
                    <div class="unga has-sub" href="#">Клиенты
                        <div class="items">
                            <a class="unga" href="<?=Url::toRoute(['/stages-document/index', ])?>">Этапы документов</a>
                            <a class="unga" href="<?=Url::toRoute(['/call-status/index', ])?>">Статус звонков</a>
                            <a class="unga" href="<?=Url::toRoute(['/relevance-client/index', ])?>">Актуальность клиента</a>
                        </div>
                    </div>
                    <div class="unga has-sub" href="#">Добавить
                        <div class="items">
                            <a class="unga" href="<?=Url::toRoute(['/city/index', ])?>">Города</a>
                            <a class="unga" href="<?=Url::toRoute(['/documentstype/index', ])?>">Тип документа</a>
                            <a class="unga" href="<?=Url::toRoute(['/rent-etap/index', ])?>">Этапы aренды</a>
                            <a class="unga" href="<?=Url::toRoute(['/manufacturer/index', ])?>">Производитель</a>
                            <a class="unga" href="<?=Url::toRoute(['/form-action/index', ])?>">Форма заявки</a>
                            <a class="unga" href="<?=Url::toRoute(['/placement/index', ])?>">Площадки</a>
                            <a class="unga" href="<?=Url::toRoute(['/waiting-stages/index', ])?>">Статусы ожиданий</a>
                            <a class="unga" href="<?=Url::toRoute(['/company-names/index', ])?>">Наименование компаний</a>
                            <a class="unga" href="<?=Url::toRoute(['/auction-price/index', ])?>">Цены по аукционам</a>
                            <a class="unga" href="<?=Url::toRoute(['/devices/index', ])?>">Приборы</a>
                        </div>
                    </div>
                    <div class="unga has-sub" href="#">Продукты
                        <div class="items">
                            <a class="unga" href="<?=Url::toRoute(['/products/index', ])?>">Продукты</a>
                            <a class="unga" href="<?=Url::toRoute(['/category/index', ])?>">Kатегории продуктов</a>
                        </div>
                    </div>
                  <a class="unga" href="<?=Url::toRoute(['/settings/index', ])?>">Настройки</a>
                  <a class="unga" href="<?=Url::toRoute(['/editrequest/index', ])?>">Запрос на редактирование</a>
                  <a class="unga" href="<?=Url::toRoute(['/template/index',])?>">Шаблоны</a>
                  <a class="unga" href="<?=Url::toRoute(['/statistic/interval', ])?>">Статистика по интервалу</a>
                  <a class="unga" href="<?=Url::toRoute(['/statistic/manager', ])?>">Статистика по менеджеру</a>
                </div>
            </li>
            <?php }else{?>


            <?php }?>
            <li class="dropdown notifications-menu belldown" style="margin-left: 0; padding: 18px 0;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <a class="bellup home1" style="color: #ffffff; font-size: 18px; padding-right: 44px;">Оповещения <span <?=$color_count == 0 ? 'style="background: #c1ccd1;"' : '' ?>  class="label label-success"><?=$count?></span></a>
                </a> 
                <ul class="dropdown-menu" id="ajaxCrudDatatable" >
                    <?php if($count == 0) { ?> <li class="header">&nbsp; &nbsp; У вас нет оповещения</li> <?php } ?>                   
                    <li style="width: 100%">                        
                        <?php foreach ($notifications as $value) { 
                          $task = Tasks::findOne($value->field);
                          if($task != null) $name = $task->title;
                          else $name = 'Не удалено';
                          if($value->status == 0) $dot = '<i style="color:red;">*</i>';
                          else $dot = '';
                          if($task != null){
                          ?>
                            <a role="modal-remote" style="text-align: left;" class="fa fa-list" href="<?=Url::toRoute(['/notification/tasks', 'id' => $value->id])?>">
                                <?='№ ' .  $value->field. '. ' . $dot .' ' . $name?>
                            </a>                            
                        <?php } } ?>                        
                    </li>
                     <li>                        
                        <?php foreach ($allorg as $value) { ?>                            
                            <a role="modal-remote" class="fa fa-ticket" href="<?=Url::toRoute(['/organizer/answer', 'id' => $value->id])?>">
                                <?=$value->name?>
                            </a>                            
                        <?php } ?>                        
                    </li>
                </ul>
            </li>
                <?php 
                    $idUs = 0;
                    if (Yii::$app->user->identity){
                        $idUs = \Yii::$app->user->identity->id;
                    }
                ?>

            <li style="display: block; text-align: left;">
                <a class="home home1" style="margin-left: 0;" role="modal-remote" href="<?=Url::toRoute(['/users/change', 'id' => $idUs])?>"><i class="fa fa-user"></i> <?= substr($fio->fio, 0, 10) ?></a>
            </li>
            <li style="display: block; text-align: left;">
                <a class="home home1" style="margin-left: 0;" data-method="post" href="<?=Url::toRoute(['/site/logout', 'id' => $model->id])?>"><i class="fa fa-sign-out"></i> Выход</a>
            </li>
        </ul>
    </div>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
</div>
</body>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "large",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
