<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TechTask */

?>
<div class="tech-task-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
