<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TechTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tech-task-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'product_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Products::find()->all(), 'id', 'name'), ['prompt' => 'Выберите продукт']) ?>
        </div>
    </div>

    <?= $form->field($model, 'count')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'sum')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$url = \yii\helpers\Url::to(['products/view', 'answer' => \yii\web\Response::FORMAT_JSON]);

$script = <<< JS

$('#techtask-product_id').change(function(event){
    var id = $(this).val();
    
    $.get('{$url}&id='+id, function(res){
        $('#techtask-price').val(res.cost);
        formProductCalc();
    });
});

$('#techtask-count, #techtask-price').change(function(event){
    formProductCalc();
});

function formProductCalc()
{
    var count = $('#techtask-count').val();
    var price = $('#techtask-price').val();
    
    $('#techtask-sum').val(count*price);
}

JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>