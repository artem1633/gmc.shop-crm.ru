<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TechTask */
?>
<div class="tech-task-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tender_id',
            'product_id',
            'count',
            'price',
            'sum',
            'created_at',
        ],
    ]) ?>

</div>
