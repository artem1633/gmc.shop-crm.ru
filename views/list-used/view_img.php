<?php

use yii\widgets\DetailView;
use yii\helpers\Html; 

?>
<div class="news-view"  style="overflow: scroll">
    <center>
        <?= $model->photo_equipment ? Html::img('/uploads/'.$model->photo_equipment,[]) : 'Не задано'?>
    </center>
</div>
