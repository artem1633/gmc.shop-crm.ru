<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ListUsed */

?>
<div class="list-used-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
