<?php
use yii\helpers\Url;
use yii\helpers\Html; 
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'product_id',
        'content' => function ($data) {
            return $data->product->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'model', 
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'content' => function ($data) {
            $title = substr($data->description, 0, 15) . '...';
            return '<span class="label" style="font-size:10px;">'. Html::a( $title, ['view','id' =>$data->id], ['title' => 'Просмотр', 'style' => 'color:#337ab7;','role'=>'modal-remote',]) . '</span>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'production_year',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->production_year, 'php:Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'photo_equipment',
        'content' => function ($data) {
            if($data->photo_equipment != null)return Html::a($data->photo_equipment, ['/list-used/send-file', 'file' => $data->photo_equipment,'class'=>'fa fa-user'],['title'=> 'Скачать', 'data-pjax' => 0])." ".'<a class="fa fa-eye" role="modal-remote" href="'.Url::toRoute(['list-used/view-img', 'id' => $data->id]).'">'.'</a>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'format' => ['currency', 'rub'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного cписок б/у?'],
    ],

];   