<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\ListUsed */
/* @var $form yii\widgets\ActiveForm */
?>
  
<div class="list-used-form"> 

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-6">
            <?= $form->field($model, 'product_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getProducts(),
                'id' => 'type',
                'options' => ['placeholder' => 'Выберите'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>  
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'owner_contact_inform')->textInput(['maxlength' => true]) ?>        
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'production_year')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                        'startView'=>'year',
                        'minViewMode'=>'years',
                        'format' => 'yyyy'
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>        
        </div>
         <div class="col-md-4">
            <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>        
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>        
        </div>
    </div>
     <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'other_file')->fileInput() ?>            
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
