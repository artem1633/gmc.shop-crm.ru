<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\ListUsed */
?>
<div class="list-used-view">
 <?php if($model->photo_equipment != null){ ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'product_id',
                'value' => function ($data) {
                   return $data->product->name;
                },
            ],
            'model',
            [
            'attribute'=>'description',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
            [
                    'attribute' => 'production_year',
                    'value' => function ($data) {
                     if($data->production_year != null )  return \Yii::$app->formatter->asDate($data->production_year, 'php:Y');
                    },
            ],
            'price',
            'owner_contact_inform',
            'photo_equipment',
        ],
    ]) ?>
    <?php }?>
    <?php if($model->photo_equipment == null){ ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                'attribute' => 'product_id',
                'value' => function ($data) {
                   return $data->product->name;
                },
                ],
                'model',
                [
                'attribute'=>'description',
                'format'=>'ntext',   
                'contentOptions' => [
                    'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
                ],
                ],
                [
                        'attribute' => 'production_year',
                        'value' => function ($data) {
                         if($data->production_year != null )  return \Yii::$app->formatter->asDate($data->production_year, 'php:Y');
                        },
                ],
                'price',
                'owner_contact_inform',
            ],
        ]) ?>
    <?php }?>
</div>
