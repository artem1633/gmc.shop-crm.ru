<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListUsed */
?>
<div class="list-used-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
