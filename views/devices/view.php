<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Devices */
?>
<div class="devices-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
