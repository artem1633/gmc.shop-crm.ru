<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel panel-inverse users-index">
    <div class="panel-heading" style="background: #28292b">
        <h4 class="panel-title"><?=$model->name?></h4>
    </div>
    <div class="panel-body">

	    <?php $form = ActiveForm::begin(); ?>
	    <div class="row">
	    	<div class="col-md-6">
	    		<?= $form->field($model, 'number')->textInput(['type' => true]) ?>    		
	    	</div>
	    	<div class="col-md-6">
	    		<?= $form->field($model, 'teodolit')->textInput(['maxlength' => true]) ?>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-md-4">
			    <?= $form->field($model, 'data_validate')->widget(DatePicker::classname(), [
	                'options' => ['placeholder' => 'Выберите дату',],
	                'removeButton' => false,
	                'pluginOptions' => [
	                    'autoclose'=>true, 
	                    'format' => 'yyyy-mm-dd',
	                ]
	            	]);
	            ?>
	    	</div>
	    	<div class="col-md-4">
	    		<?= $form->field($model, 'date_calibr')->widget(DatePicker::classname(), [
	                'options' => ['placeholder' => 'Выберите дату',],
	                'removeButton' => false,
	                'pluginOptions' => [
	                    'autoclose'=>true, 
	                    'format' => 'yyyy-mm-dd',
	                ]
	            	]);
	            ?>
	    	</div>
	    	<div class="col-md-4">
			    <?= $form->field($model, 'plant_number')->textInput(['maxlength' => true]) ?>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-md-6">
			    <?= $form->field($model, 'gorizontal')->textInput(['maxlength' => true]) ?>
	    	</div>
	    	<div class="col-md-6">
	    		<?= $form->field($model, 'vertical')->textInput(['maxlength' => true]) ?>
	    	</div>
	    </div>
	  
		<?php if (!Yii::$app->request->isAjax){ ?>
		  	<div class="form-group">
		        <?= Html::submitButton( 'Печатать', ['class' => 'btn btn-primary']) ?>
		    </div>
		<?php } ?>

	    <?php ActiveForm::end(); ?>
    </div>
</div>
