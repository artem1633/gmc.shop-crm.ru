<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="template-view" style="padding: 20px;">
	<?php
		foreach ($templates as $template) {
			if($template->key == 'pustishka_kalibrovka' || $template->key == 'pustishka_poverka')
			{
				echo Html::a( $template->name, Url::to(['/template/empty-template', 'key' => $template->key]), ['data-pjax' => 0, 'target' =>'_blank',  'class' => 'btn btn-success btn-xs', 'title' => 'Печатать', 'style' => 'width:100%;' ]) . '<br><br>';
			}
			else
			{
				echo Html::a( $template->name, Url::to(['/template/print', 'key' => $template->key]), ['data-pjax' => 0, 'target' =>'_blank',  'class' => 'btn btn-success btn-xs', 'title' => 'Печатать', 'style' => 'width:100%;' ]) . '<br><br>';
			}
		}
	?>

</div>
