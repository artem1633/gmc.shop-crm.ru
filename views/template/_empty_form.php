<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<?php $form = ActiveForm::begin(); ?>

	<div class="panel panel-inverse settings-index leftindex">
	    <div class="panel-heading" style="background: #28292b">
	        <h4 class="panel-title rabsize3 left1"><?=$model->name?></h4>
	        <span class="pull-right">
	        	<?= Html::submitButton('Печатать', [ 'style' => 'margin-top:-35px;', 'class' => 'btn btn-primary btn-xs']) ?>
	        </span>
	    </div>
	    <div class="panel-body">

		    <?= $form->field($model, 'text')->widget(CKEditor::className(),[
		        'editorOptions' => [
		            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
		            'inline' => false, //по умолчанию false
		            'height' => '400px',
		            ],
		        ]);
		    ?>

			<div style="display: none;">
			    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		    	<?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
			</div>
	    </div>
	</div>
<?php ActiveForm::end(); ?>
</body>