<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="verification-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-2 label1" >
            <?=$model->getAttributeLabel('application_date')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'application_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ])->label("");
            ?>
        </div>
        <div class="col-md-1 label1" >
            <?=$model->getAttributeLabel('devices_id')?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'devices_id')->dropDownList($model->getStatusList(), ['prompt' => 'Выберите', 'id' => 'type'])->label("") ?>
        </div> 
        <div class="col-md-2 label1" >
            <?=$model->getAttributeLabel('check_date')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'check_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ])->label("");
            ?>
        </div>
        <div class="col-md-1 label1" >
            <?=$model->getAttributeLabel('series')?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'series')->textInput(['maxlength' => true])->label("") ?>             
        </div>
        <div class="col-md-2 label1">
            <?=$model->getAttributeLabel('status')?>            
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(),['prompt' => 'Выбреите','id'=> 'type'])->label("")?>
        </div>
        <div class="col-md-1 label1" >
            <?=$model->getAttributeLabel('number')?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true])->label("") ?>
        </div>
        <div class="col-md-7 label1" >
            <?=$model->getAttributeLabel('date_next_check')?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'date_next_check')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ])->label("");
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"> 
            <?= $form->field($model, 'other_file')->fileInput() ?>        
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'client_id')->textInput() ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
