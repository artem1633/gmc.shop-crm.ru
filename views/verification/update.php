<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Verification */
?>
<div class="verification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
