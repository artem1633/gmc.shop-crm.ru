<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Client;
use yii\helpers\Html;
use app\models\Devices;
return [
   [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'client_id',
        'filter' => ArrayHelper::map(Client::find()->all(),'id','working_title'),
        'value' => function ($data) {
            return $data->client->working_title;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'devices_id',
        'filter' => ArrayHelper::map(Devices::find()->all(),'id','name'),
        'content' => function ($data) {
            return $data->devices->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'series',
    ],
    [
        'attribute'=>'status',
        'filter' => array('1'=>'Новая','2'=>'В работе','3'=>'Выполнена'),
        'value'=> function($data){
            return \yii\helpers\ArrayHelper::map([
                ['id'=> 1,'title'=>'Новая'],
                ['id'=> 2,'title'=>'В работе'],
                ['id'=> 3,'title'=>'Выполнена'],
            ],'id','title')[$data->status];
        }
    ],
    [
        'attribute'=>'documents',
        'format'=>'html',  
        'content' => function ($data) {
             return Html::a($data->documents, ['/verification/send-file', 'file' => $data->documents],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{leadPrint}',
        'header' => 'Шаблоны',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'buttons' => [ 
            'leadPrint' => function ($url, $model) 
            {
                $url = Url::to(['/template/all-templates']);
                return Html::a('<span class="glyphicon glyphicon-print"></span>', $url, ['role'=>'modal-remote', /*'data-pjax' => 0, 'target' =>'_blank', */   'title'=>'', 'data-toggle'=>'tooltip']);
            }, 
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view} {update} {delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного поверки?'],
    ],

];   