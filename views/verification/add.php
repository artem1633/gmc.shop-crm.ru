<?php

use yii\helpers\Html;


?>
<div class="verification-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
