<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Verification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="verification-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
         <div class="col-md-3">
            <?= $form->field($model, 'client_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getClients(),
                'id' => 'type',
                'options' => ['placeholder' => 'Выберите'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'devices_id')->dropDownList($model->getStatusList(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'series')->textInput(['maxlength' => true]) ?>             
        </div>
        <div class="col-md-3">
            <?= $form->field($model,'status')->dropDownList($model->getStatus(),['prompt'=>'Вибрате','id' => 'type']) ?>
        </div>
    </div>
     <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'application_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
        </div> 
        <div class="col-md-3">
            <?= $form->field($model, 'check_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date_next_check')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"> 
            <?= $form->field($model, 'other_file')->fileInput() ?>        
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
