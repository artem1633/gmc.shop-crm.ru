<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Verification */

?>
<div class="verification-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
