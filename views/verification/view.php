<?php

use yii\widgets\DetailView;
use yii\helpers\Html; 
/* @var $this yii\web\View */
/* @var $model app\models\Verification */
?>
<div class="verification-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                     'attribute'=>'client_id',
                     'value' => function ($data) {
                           return $data->client->working_title;
                        },
                     'contentOptions' => [
                        'style'=>'max-width:100%; min-height:100px; overflow: auto; word-wrap: break-word;'
                    ],
            ],
            [
                    'attribute' => 'application_date',
                    'value' => function ($data) {
                     if($data->application_date != null )  return \Yii::$app->formatter->asDate($data->application_date, 'php:d.m.Y');
                    },
            ],
            [
                    'attribute' => 'check_date',
                    'value' => function ($data) {
                     if($data->check_date != null )  return \Yii::$app->formatter->asDate($data->check_date, 'php:d.m.Y');
                    },
            ],
            [
                    'attribute' => 'status',
                    'value' => function($data){
                        return \yii\helpers\ArrayHelper::map([
                        ['id'=>'1','title'=>'Новая'],
                        ['id'=>'2','title'=>'В работе'],
                        ['id'=>'3','title'=>'Выполнена'],
                    ],'id','title')[$data->status];
                    }
            ],
            [
                'attribute'=>'documents',
                'format'=>'html',  
                'value' => function ($data) {
                    return Html::a($data->documents, ['/verification/send-file', 'file' => $data->documents],['title'=> 'Скачать', 'data-pjax' => 0]);
                },
            ],
            [
                    'attribute' => 'date_next_check',
                    'value' => function ($data) {
                     if($data->date_next_check != null )  return \Yii::$app->formatter->asDate($data->date_next_check, 'php:d.m.Y');
                    },
            ],
            'number',
            [
                'attribute' => 'devices_id',
                'value' => function ($data) {
                   return $data->devices->name;
                },
            ],
            'series',
        ],
    ]) ?>

</div>
