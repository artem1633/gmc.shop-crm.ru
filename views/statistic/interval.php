<?php
use yii\helpers\Html;
use kartik\grid\GridView;

$this->title = "Статистика по интервалу";
$this->params['breadcrumbs'][] = $this->title;

?>
<head> 
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head> 
<div class="panel panel-inverse settings-index ">
    <div class="panel-heading" style="background: #28292b">
        <h4 class="panel-title rabsize3 left1">Статистика по интервалу</h4>
    </div>
    <div class="box box-default">
        <div class="box-body">
        </div>
        <img style="width: 100%;margin-top: 20px" src="/theme/assets/img/chiziq95.png" alt="">
    </div>
    <div class="panel-body">
        <?php  echo $this->render('search_interval', ['post'=>$post]); ?>

        <div class="row">                                 
            <div class="col-md-12 col-sm-12">
                <div class="table-responsive">                                    
                    <table class="table table-bordered">
                        <thead class="rabsize1">
                            <tr>
                                <th style="color:red;">ФИО менеджера</th>
                                <th style="color:red;">Кол-во звонков</th>
                            </tr>
                        </thead>
                        <tbody class="rabsize1">
                            <?php
                            foreach ($result as $value) {
                            ?>
                            <tr>
                                <th><b><?=$value['name']?></b></th>
                                <td><?=$value['count']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>  
                </div>
            </div>
        </div>        

    </div>
</div>