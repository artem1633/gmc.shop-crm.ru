<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use kartik\field\FieldRange;
use kartik\form\ActiveForm;
use kartik\touchspin\TouchSpin;
use app\models\Users;
use kartik\select2\Select2;

?>

<div class="client-form" style="margin-left: 30px;">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <label style="margin-left: 15px;">Интервал </label><br>
<?php 
$layout = <<< HTML
    <span class="input-group-addon" style="background-color: #ecf0f5; color:black;">С</span>
    {input1}
    <span class="input-group-addon" style="background-color: #ecf0f5; color:black;">По</span>
    {input2}
    <span class="input-group-addon kv-date-remove" style="background-color: #ecf0f5; color:black;">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;
 ?>
     <div class="col-md-6" > 
        <?php
            echo DatePicker::widget([
                'type' => DatePicker::TYPE_RANGE,
                'name' => 'date_time_from',
                'value' => $post['date_time_from'],
                'name2' => 'date_time_to',
                'value2' => $post['date_time_to'],
                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
        ?>
    </div>

    <div class="col-md-4"> 
        <?php
            echo Select2::widget([
                'name' => 'manager',
                'data' => ArrayHelper::map(Users::find()->where(['type' => 4])->all(), 'id', 'fio'),
                'value' => $post['manager'],
                'options' => [
                    'placeholder' => 'Выберите менеджера',
                ],
            ]);
        ?>
    </div>


    <div class="col-md-2">
        <div class="form-group" >
            <?= Html::submitButton('Поиск' , ['style'=>' ','class' =>  'btn btn-primary']) ?>
        </div>
    </div>
 </div>   
    <?php ActiveForm::end(); ?>
    
</div>
