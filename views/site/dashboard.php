<?php
use app\models\ClientSearch;
use app\models\OrganizerSearch;
use app\models\TasksSearch;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Users;  
use app\models\Tasks;  
use yii\helpers\Html;
use rmrevin\yii\module\Comments;
use yii\widgets\Pjax;
$this->title = 'Рабочий стол'; 

/** @var \app\models\UserSetting $user */
$settings = Yii::$app->user->identity->setting;



?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >

<?php Pjax::begin(['id' => 'pjax-selected-id']) ?>
<?php

if($settings->dashboard_info == \app\models\UserSetting::DASHBOARD_INFO_CLIENTS) {
    $classClients = 'class="active"';
    $classChat = '';
    $classOrganizer = '';
} else if($settings->dashboard_info == \app\models\UserSetting::DASHBOARD_INFO_CHAT) {
    $classClients = '';
    $classChat = 'class="active"';
    $classOrganizer = '';
} else if($settings->dashboard_info == \app\models\UserSetting::DASHBOARD_INFO_ORGANIZER) {
    $classClients = '';
    $classChat = '';
    $classOrganizer = 'class="active"';
} else {
    $classClients = '';
    $classChat = '';
    $classOrganizer = '';
}

?>
<div class="row">
    <div class="col-md-12">
            <ul class="vertical-menu">
                <li <?=$classClients?>>
                    <a href="#" data-type="<?=\app\models\UserSetting::DASHBOARD_INFO_CLIENTS?>">Клиенты</a>
                </li>
                <li <?=$classChat?>>
                    <a href="#" data-type="<?=\app\models\UserSetting::DASHBOARD_INFO_CHAT?>">Чат</a>
                </li>
                <li <?=$classOrganizer?>>
                    <a href="#" data-type="<?=\app\models\UserSetting::DASHBOARD_INFO_ORGANIZER?>">Органайзер</a>
                </li>
            </ul>
    </div>
</div>
    <div class="row">
        <div class="col-md-8">
            <div id="select-content-wrapper">
                <?php if($settings->dashboard_info == \app\models\UserSetting::DASHBOARD_INFO_CHAT): ?>
                    <div class="panel panel-inverse" data-sortable-id="index-5"  style="border-style: outset;">
                        <div style="background: #28292b;" class="panel-heading" >
                            <div class="panel-heading-btn">
                                <a role="modal-remote"  href="<?=Url::toRoute(['/tasks/create', 'client_id' => $model->id])?>" class="btn btn-xs btn-icon btn-circle btn-primary" data-click=""><i class="fa fa-plus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title rabsize3">Чат</h4>
                        </div>
                        <div class="panel-body page-container-width1 desc-page-container" >
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                                <div  data-scrollbar="true" data-init="true" style="overflow: hidden; width: auto; height: auto;">
                                    <?php echo Comments\widgets\CommentListWidget::widget(['entity' => 'dashboard' ]);?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php elseif($settings->dashboard_info == \app\models\UserSetting::DASHBOARD_INFO_ORGANIZER): ?>
                   <?php
                    $searchModel = new TasksSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    ?>
                    <?= $this->render('@app/views/tasks/index.php', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'status' => 0,
                    ]) ?>
                <?php elseif($settings->dashboard_info == \app\models\UserSetting::DASHBOARD_INFO_CLIENTS): ?>
                    <?php
                    $searchModel = new ClientSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    ?>
                    <?= $this->render('@app/views/client/index.php', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-4 ui-sortable">
            <div class="panel panel-inverse" data-sortable-id="index-8"  style="border-style: outset;">
                <div style="background: #28292b;" class="panel-heading">
                    <div class="panel-heading-btn">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'crud-datatable-pjax']) ?>
                        <a role="modal-remote"  href="<?=Url::toRoute(['/tasks/create', 'client_id' => $model->id])?>" class="btn btn-xs btn-icon btn-circle btn-primary" data-click=""><i class="fa fa-plus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        <?php Pjax::end() ?>
                    </div>
                    <h4 class="panel-title rabsize3">Общий список задач</h4>
                </div>
                <div class="panel-body p-0 page-container-width1 desc-page-container" style="background: #FFFFFF;color: #000000">
                    <ul class="todolist" >
                        <?php
                        foreach ($tasks as $task) { $url = Url::to(['tasks/view', 'id' => $task->id]); ?>
                            <li >
                                <a style="width: 400px" href="<?=$url?>" role="modal-remote" class="todolist-container" data-click="todolist">
                                    <div class="todolist-title"><?= $task->title ?></div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php
$changeSettingUrl = Url::toRoute(['user-setting/set-setting', 'name' => 'dashboard_info']);
$script = <<< JS
    $('[data-type]').click(function(event){
        event.preventDefault();
        var type = $(this).data('type');
        
        $.get('{$changeSettingUrl}&value='+type, function(response){
            if(response.result){
                $.pjax.reload('#pjax-selected-id');
            }
        });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);
?>
<?php Pjax::end() ?>

</body>
<?php

$this->registerJsFile('/theme/assets/js/dashboard.min.js', ['position' => yii\web\View::POS_READY]);


?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
