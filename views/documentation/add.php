<?php

use yii\helpers\Html;

?>
<div class="documentation-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
