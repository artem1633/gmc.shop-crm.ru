<?php

use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Client;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Documentation */
?>
<div class="documentation-view">
 
    <?= DetailView::widget([ 
        'model' => $model,
        'attributes' => [
            'id',
             [
            'attribute'=>'working',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
            [
                'attribute' => 'users_id',
                'value' => function ($data) {
                   return $data->users->fio;
                },
            ],
            [
                'attribute' => 'client_id',
                'value' => function ($data) {
                   return $data->client->working_title;
                },
            ],
            [
                    'attribute' => 'date',
                    'value' => function ($data) {
                     if($data->date != null )  return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
                    },
            ],
            'number',
            'ownership',
            'file',
            [
                'attribute' => 'stage_id',
                'value' => function ($data) {
                   return $data->stage->name;
                },
            ],
            
        ],
    ]) ?>

</div>
