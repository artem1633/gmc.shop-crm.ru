<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Documentation */
/* @var $form yii\widgets\ActiveForm */
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body > 
<div class="documentation-form"> 

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <img style="width: 100%;margin-top: -60px;" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-2 label1">
            <?=$model->getAttributeLabel('working')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'working')->textInput(['maxlength' => true])->label(""); ?>
        </div>
        <div class="col-md-2 label1">
            <?=$model->getAttributeLabel('client_id')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'client_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getClients(),
                'id' => 'type',
                'options' => ['placeholder' => 'Выберите'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-2 label1" style="margin-top: 10px">
            <?=$model->getAttributeLabel('date')?>
        </div>
        <div class="col-md-4" style="margin-top: -10px">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ])->label(""); 
            ?>
        </div>
        <div class="col-md-2 label1" style="margin-top: 10px">
            <?=$model->getAttributeLabel('documents_type_id')?>
        </div>
        <div class="col-md-4" style="margin-top: -10px">
           <?= $form->field($model, 'documents_type_id')->dropDownList($model->getDocumentstypes(), ['prompt' => 'Выберите', 'id' => 'type'])->label(""); ?>
        </div> 
    </div>
    <div class="row">
        <div class="col-md-2 label1" style="margin-top: 10px">
            <?=$model->getAttributeLabel('ownership')?>
        </div>
        <div class="col-md-4" style="margin-top: -10px">
            <?= $form->field($model, 'ownership')->textInput(['maxlength' => true])->label(""); ?>            
        </div>
        <div class="col-md-2 label1" style="margin-top: 10px">
            <?=$model->getAttributeLabel('number')?>
        </div>
        <div class="col-md-4" style="margin-top: -10px">
           <?= $form->field($model, 'number')->textInput(['maxlength' => true])->label(""); ?>
        </div>
        <div class="col-md-2 label2" style="margin-top: 25px">
            <?=$model->getAttributeLabel('other_file')?>
        </div>
         <div class="col-md-4" style="margin-top: 10px">
            <?= $form->field($model, 'other_file')->fileInput()->label(""); ?>            
        </div>
        <div class="col-md-2 label1" style="margin-top: 10px">
            <?=$model->getAttributeLabel('stage_id')?>
        </div>
         <div class="col-md-4" style="margin-top: -10px">
            <?= $form->field($model, 'stage_id')->dropDownList($model->getStagesList(), ['prompt' => 'Выберите', 'id' => 'type'])->label(""); ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'users_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>    
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</body>