<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Client;
use app\models\StagesDocument;
use app\models\Documentstype;
use app\models\Users;
use yii\helpers\Html;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'working',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'filter' => ArrayHelper::map(Client::find()->all(),'id','working_title'),
        'content' => function ($data) {
            return $data->client->working_title;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'stage_id',
        'filter' => ArrayHelper::map(StagesDocument::find()->all(),'id','name'),
        'content' => function ($data) {
            return $data->stage->name;
        },
    ],
     [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documents_type_id',
        'filter' => ArrayHelper::map(Documentstype::find()->all(),'id','name'),
        'content' => function ($data) {
            return $data->documentstype->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'users_id',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function ($data) {
            return $data->users->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'content' => function ($data) {
            return Html::a($data->file, ['/documentation/send-document-file', 'file' => $data->file],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ownership',
    ],
   
    
   [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view}{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','data-pjax'=>0,'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного документы?'], 
    ],

];   