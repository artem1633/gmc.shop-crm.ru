<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
 
?>

<div class="call-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px;" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-2 label1">
            <?=$model->getAttributeLabel('working')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'working')->textInput(['maxlength' => true])->label(""); ?>
        </div>
        <div class="col-md-1 label1" >
            <?=$model->getAttributeLabel('date')?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ])->label(""); 
            ?>
        </div> 
    </div>

    <div class="row">
        <div class="col-md-2 label1" >
            <?=$model->getAttributeLabel('ownership')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ownership')->textInput(['maxlength' => true])->label(""); ?>            
        </div>
        <div class="col-md-1 label1" >
            <?=$model->getAttributeLabel('number')?>
        </div>
        <div class="col-md-5">
           <?= $form->field($model, 'number')->textInput(['maxlength' => true])->label(""); ?>
        </div>
        <div class="col-md-2 label1" >
            <?=$model->getAttributeLabel('documents_type_id')?>
        </div>
        <div class="col-md-4">
           <?= $form->field($model, 'documents_type_id')->dropDownList($model->getDocumentstypes(), ['prompt' => 'Выберите', 'id' => 'type'])->label(""); ?>
        </div>
        <div class="col-md-1 label1" >
            <?=$model->getAttributeLabel('stage_id')?>
        </div>
         <div class="col-md-5">
            <?= $form->field($model, 'stage_id')->dropDownList($model->getStagesList(), ['prompt' => 'Выберите', 'id' => 'type'])->label(""); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1 label2" >
            <?=$model->getAttributeLabel('other_file')?>
        </div>
         <div class="col-md-4" >
            <?= $form->field($model, 'other_file')->fileInput()->label(""); ?>            
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'users_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>    
        <?= $form->field($model, 'client_id')->textInput() ?>     
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
