<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Documentation */
?>
<div class="documentation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
