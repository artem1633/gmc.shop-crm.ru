<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
$user_id = Yii::$app->user->identity->id;
$disabled = false;
if(!$model->isNewRecord /*&& $model->to_users_id == $user_id*/ && Yii::$app->user->identity->type != 1) $disabled = true;
?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3 label1">
            <?=$model->getAttributeLabel('client_id')?>
        </div>
        <div class="col-md-5" style="margin-left: -75px;">
            <?= $form->field($model, 'client_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getClients(),
                'disabled' => $disabled,
                'options' => ['placeholder' => 'Выберите'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1" style="margin-top: 0px">
            <?=$model->getAttributeLabel('title')?>
        </div>
        <div class="col-md-5" style="margin-left: -75px;margin-top: -20px">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'disabled' => $disabled])->label(""); ?>           
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1" style="margin-top: 0px">
            <?=$model->getAttributeLabel('status')?>
        </div>
        <div class="col-md-5" style="margin-left: -75px;margin-top: -20px">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите', 'options'=>['1'=>['disabled'=>true], '4'=>['disabled'=>true]] ])->label(""); ?>          
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1" style="margin-top: 0px">
            <?=$model->getAttributeLabel('who_users_id')?>
        </div>
        <div class="col-md-5" style="margin-left: -75px;margin-top: -20px">
            <?= $form->field($model, 'who_users_id')->dropDownList($model->getUsers(), ['prompt' => 'Выберите',  'disabled' => $disabled ? true : (Yii::$app->user->identity->type == 1 ? false : true) ])->label(""); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1" style="margin-top: 0px">
            <?=$model->getAttributeLabel('to_users_id')?>
        </div>
        <div class="col-md-5" style="margin-left: -75px;margin-top: -20px">
            <?= $form->field($model, 'to_users_id')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'disabled' => $disabled ])->label(""); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1" style="margin-top: 0px">
            <?=$model->getAttributeLabel('terms')?>
        </div>
        <div class="col-md-5" style="margin-left: -75px;margin-top: -20px">
            <?= $form->field($model, 'terms')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату',],
                'removeButton' => false,
                'disabled' => $disabled,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ])->label("");
            ?>
        </div>  
        <div class="col-md-4" style="margin-top: -235px;margin-left: 60px;">
            <?= $form->field($model, 'description')->textarea(['rows' => 14, 'disabled' => $disabled]); ?>
        </div>      
    </div>
    <div class="row">
       <div class="col-md-2 label1" style="margin-top: 35px;">
            <?=$model->getAttributeLabel('other_file')?>
        </div>
        <div class="col-md-3" style="margin-top: 21px;"> 
            <?= $form->field($model, 'other_file')->fileInput()->label("") ?>        
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
