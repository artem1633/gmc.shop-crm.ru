<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\tasks */

?>
<div class="tasks-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
