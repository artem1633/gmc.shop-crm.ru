<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tasks */

?>
<div class="tasks-create">
    <?= $this->render('_create_form', [
        'model' => $model,
    ]) ?>
</div>
