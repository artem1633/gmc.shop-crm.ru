<?php
use yii\helpers\Url;
use yii\helpers\Html;
use rmrevin\yii\module\Comments;

$this->title = "Чат";
$this->params['breadcrumbs'][] = $this->title;


?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="panel panel-inverse users-index">
    <div class="panel-heading" style="background: #28292b">
        <h4 class="panel-title rabsize3 left1">Чат</h4>
        <span class="pull-right"><?= Html::a('Назад', ['/tasks/index'],
                    ['data-pjax'=>'0','title'=> 'Назад','class'=>'btn btn-warning btn-xs', 'style' => 'margin-top:-35px;' ])?></span>
    </div>
    <div class="panel-body">
        <?php echo Comments\widgets\CommentListWidget::widget(['entity' => 'tasks' ]);?>
    </div>
</div>
</body>
