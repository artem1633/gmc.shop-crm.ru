<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-form">
 
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
         <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>           
        </div>
       
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите', 'id' => 'type']) ?>          
        </div>
      
    </div> 
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'who_users_id')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'to_users_id')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
         <div class="col-md-4">
            <?= $form->field($model, 'terms')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату',],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
       
        
    </div>
    <div class="row">
       
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>          
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"> 
            <?= $form->field($model, 'other_file')->fileInput() ?>        
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'client_id')->textInput() ?>
    </div>
  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
