<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Call */
/* @var $form yii\widgets\ActiveForm */
$user_id = Yii::$app->user->identity->id;
$disabled = false;
if(!$model->isNewRecord && $model->to_users_id == $user_id && Yii::$app->user->identity->type != 1) $disabled = true;
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="call-form">
 
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -20px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3 label1">
            <?=$model->getAttributeLabel('title')?>
        </div>
         <div class="col-md-5" style="margin-left: -75px;">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'disabled' => $disabled])->label(""); ?>           
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1 lable3" style="margin-top: 0px">
            <?=$model->getAttributeLabel('status')?>
        </div>
        <div class="col-md-5 lable3" style="margin-left: -75px;">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите', ])->label(""); ?>          
        </div>
      
    </div> 
    <div class="row">
        <div class="col-md-3 label1 lable3" style="margin-top: 0px">
            <?=$model->getAttributeLabel('who_users_id')?>
        </div>
          <div class="col-md-5 lable3" style="margin-left: -75px;">
            <?= $form->field($model, 'who_users_id')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'disabled' => $disabled ? true : (Yii::$app->user->identity->type == 1 ? false : true) ])->label(""); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1 lable3" style="margin-top: 0px">
            <?=$model->getAttributeLabel('to_users_id')?>
        </div>
        <div class="col-md-5 lable3" style="margin-left: -75px;">
            <?= $form->field($model, 'to_users_id')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'disabled' => $disabled ])->label(""); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 label1 lable3" style="margin-top: 0px">
            <?=$model->getAttributeLabel('terms')?>
        </div>
         <div class="col-md-5 lable3" style="margin-left: -75px;">
            <?= $form->field($model, 'terms')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату',],
                'removeButton' => false,
                'disabled' => $disabled,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                ]
            ])->label("");
            ?>
        </div>
       <div class="col-md-4" style="margin-top: -212px;margin-left: 60px;">
            <?= $form->field($model, 'description')->textarea(['rows' => 13]); ?>
        </div>
        
    </div>
     <div class="row">
       <div class="col-md-2 label1" style="margin-top: 35px;">
            <?=$model->getAttributeLabel('other_file')?>
        </div>
        <div class="col-md-3" style="margin-top: 21px;"> 
            <?= $form->field($model, 'other_file')->fileInput()->label("") ?>        
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'date')->textInput(['value' => date('d-m-Y')])?>
        <?= $form->field($model, 'time')->widget(TimePicker::classname(), [
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                    'format' => 'm:H', 
                ]]); ?>
        <?= $form->field($model, 'client_id')->textInput() ?>
        <?= $form->field($model, 'users_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>        
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
</body>
