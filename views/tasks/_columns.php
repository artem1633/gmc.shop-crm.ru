<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use kartik\time\TimePicker;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
        'filter' => DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'date',
            'language' => 'ru',
                'clientOptions' => [
            'language' => 'ru',
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
        ]),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'time',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'who_users_id',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function($data){
            return $data->whoUsers->fio;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'to_users_id',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function($data){
            return $data->toUsers->fio;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter'=>array("1"=>"Выполнено","2"=>"На проверке","3"=>"Есть уточнение","4"=>"Новая"),
        'content' => function($data){
            return $data->getStatusName();
        }
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {view} {update} {leadDelete}',
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'buttons'  => [
            'leadDelete' => function ($url, $model) {
                if(Yii::$app->user->identity->type == 1){
                    $url = Url::to(['/tasks/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?',
                    ]);
                }
            },
        ]
    ]

];   