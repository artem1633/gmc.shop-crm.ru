<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
?>
<div class="tasks-update">

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
