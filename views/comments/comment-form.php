<?php
/**
 * comment-form.php
 * @author Revin Roman
 * @link https://rmrevin.ru
 *
 * @var yii\web\View $this
 * @var Comments\forms\CommentCreateForm $CommentCreateForm
 */

use rmrevin\yii\module\Comments;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var Comments\widgets\CommentFormWidget $Widget */
$Widget = $this->context;

?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<a name="commentcreateform"></a>
<div class="row comment-form" >
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <?php
        /** @var ActiveForm $form */
        $form = ActiveForm::begin();

        echo Html::activeHiddenInput($CommentCreateForm, 'id');

        if (\Yii::$app->getUser()->getIsGuest()) {
            echo $form->field($CommentCreateForm, 'from')
                ->textInput();
        }

        $options = [
            'rows' => '4',
        ];

        if ($Widget->Comment->isNewRecord) {
            $options['data-role'] = 'new-comment';
        }

        echo $form->field($CommentCreateForm, 'text')
            ->textarea($options)->label('Текст');

        ?>
        <div class="actions">
            <?php
            echo Html::submitButton(\Yii::t('app', 'Добавить'), [
                'class' => 'btn btn dobavit',
            ]);
            echo Html::resetButton(\Yii::t('app', 'Отмена'), [
                'class' => 'btn btn otmena',
            ]);
            ?>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>
</body>