<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="orders-view">
 
    <?= DetailView::widget([ 
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'client_id',
                'value' => function ($data) {
                   return $data->client->working_title;
                },
            ],
            [
                    'attribute' => 'created_date',
                    'value' => function ($data) {
                     if($data->created_date != null )  return \Yii::$app->formatter->asDate($data->created_date, 'php:d.m.Y');
                    },
            ],
            [
                    'attribute' => 'payment_date',
                    'value' => function ($data) {
                     if($data->payment_date != null )  return \Yii::$app->formatter->asDate($data->payment_date, 'php:d.m.Y');
                    },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                   if($data->status == 1) return 'Не оплачено';
                   if($data->status == 2) return 'Оплачено';
                },
            ],
            'discount',
            [
                'attribute' => 'base_id',
                'value' => function ($data) {
                   if($data->base_id == 1) return 'Счет';
                   if($data->base_id == 2) return 'Коммерческое предложения';
                },
            ],
            [
            'attribute'=>'comment',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
            [
                'attribute' => 'responsible',
                'value' => function ($data) {
                  return $data->responsible0->fio;
                },
            ],
            'total_sum',
            'total_nds_sum',
        ],
    ]) ?>

</div>
