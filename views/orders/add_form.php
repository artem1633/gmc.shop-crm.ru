<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>
 
<div class="call-form"> 

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-4">
            <?= $form->field($model, 'created_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'payment_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'discount')->textInput(['type'=>'number']) ?> 
        </div>
        
    </div>
    <div class="row">
         <div class="col-md-4">
            <?= $form->field($model, 'base_id')->dropDownList($model->getBase(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'responsible')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
         <div class="col-md-4">
             <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите', 'id' => 'type']) ?>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'client_id')->textInput() ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
