<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 06.05.2017
 * Time: 13:38
 */

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'dataProvider' => $model->orderGoodsData,
    'columns' => [
        [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function ($data) {
            return $data->product->name;
        },
        ],
        [
            'class' => \kartik\grid\EditableColumn::className(),
            'editableOptions' => [
                'inputType' => \kartik\editable\Editable::INPUT_MONEY,
                'formOptions' => [
                    'action' => \yii\helpers\Url::to(['edit'])
                ]
            ],
//        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
            'attribute' => 'count',
        ],
        'cost',
        'sum',
        [
                'attribute' => 'nds_protsent',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => '0%',],
                        ['id' => 2,
                            'title' => '10%',],
                        ['id' => 3,
                            'title' => '18%',],
                    ], 'id', 'title')[$data->nds_protsent];
                }
    ],
        'nds_sum',
        'total_sum',
        [
            'class'    => 'kartik\grid\ActionColumn',
            'template' => '{leadDelete}',
            'buttons'  => [

                'leadUpdate' => function ($url, $model) {
                    $url = Url::to(['/goods/update-goods', 'id' => $model->id]);
                    return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                },
                'leadDelete' => function ($url, $model) {
                    $url = Url::to(['/goods/delete-goods', 'id' => $model->id]);
                    return Html::a('<span class="fa fa-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'',
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить данного товар?'
                    ]);
                },
            ]
        ]
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
