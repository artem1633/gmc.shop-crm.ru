<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Client;
use app\models\Users;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('@app/views/client/_expand-order-goods', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'filter' => ArrayHelper::map(Client::find()->all(),'id','working_title'),
        'content' => function ($data) {
            return $data->client->working_title;
        },
        'width' => '200px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->created_date, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->payment_date, 'php:d.m.Y');
        },
    ],
    [
                'attribute' => 'status',
                'filter' => array('1' => 'Не оплачено' , '2' => 'Оплачено'),
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Не оплачено',],
                        ['id' => 2,
                            'title' => 'Оплачено',],
                    ], 'id', 'title')[$data->status];
                }
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'discount',
    ],
    [
                'attribute' => 'base_id',
                'filter' => array('1' => 'Счет' , '2' => 'Коммерческое предложения'),
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Счет',],
                        ['id' => 2,
                            'title' => 'Коммерческое предложения',],
                    ], 'id', 'title')[$data->base_id];
                }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'responsible',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function ($data) {
            return $data->responsible0->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Добавить товар',
        'content' => function ($data)
        {
            return '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-size:10px" class="btn btn-primary" role="modal-remote" href="'.Url::toRoute(['/goods/add', 'order_id' => $data->id, 'pjaxContainer' => '#crud-datatable-pjax']).'"><i class="fa fa-plus"></i></a>';
        },
    ],
   [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view}{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','data-pjax'=>0,'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного заказы?'], 
    ],

];   