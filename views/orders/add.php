<?php

use yii\helpers\Html;

?>
<div class="orders-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
