<?php
use yii\helpers\Url;
use yii\helpers\Html; 
use app\models\Orders;
use app\models\Client; 
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_name',
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_application_created',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_application_created, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_application', 
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_application, 'php:d.m.Y');
        },
    ],
    [
                'attribute' => 'ptichka',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 0,
                            'title' => 'Нет',],
                        ['id' => 1,
                            'title' => 'Да',],
                    ], 'id', 'title')[$data->ptichka];
                }
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_id', 
        'content' => function ($data) {
            return '<a role="modal-remote" class="btn btn-default btn-xs" href="'.Url::toRoute(['orders/view-logistics', 'id' => $data->order_id]).'">'.('№ '.$data->order->id." Клиент:".$data->order->client->working_title).'</a>';
        },
    ],
    [
                'attribute' => 'stage',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Создана заявка',],
                        ['id' => 2,
                            'title' => 'В работе',],
                        ['id' => 3,
                            'title' => 'Выполнено',], 
                    ], 'id', 'title')[$data->stage];
                }
    ], 
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного логистика?'],
    ],

];   