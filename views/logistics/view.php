<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Logistics */
?>
<div class="logistics-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [ 
            //'id',
            [
                    'attribute' => 'date_application_created',
                    'value' => function ($data) {
                     if($data->date_application_created != null )  return \Yii::$app->formatter->asDate($data->date_application_created, 'php:d.m.Y');
                    },
            ],
            [
                    'attribute' => 'date_application',
                    'value' => function ($data) {
                     if($data->date_application != null )  return \Yii::$app->formatter->asDate($data->date_application, 'php:d.m.Y');
                    },
            ],
            [
                'attribute' => 'ptichka',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 0,
                            'title' => 'Нет',],
                        ['id' => 1,
                            'title' => 'Да',],
                    ], 'id', 'title')[$data->ptichka];
                }
             ], 
            'company_name',
            [
                'attribute'=>'order_id',
                'value' => function ($data) {
                     return ('№: '.$data->order->id." Клиент: ".$data->order->client->working_title);
                },
            ],
            [
                'attribute' => 'stage',
                'value' => function ($data) {
                   if($data->stage == 1) return 'Создана заявка';
                   if($data->stage == 2) return 'В работе';
                   if($data->stage == 3) return 'Выполнено';
                },
            ],
            [
            'attribute'=>'other',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
        ],
    ]) ?>

</div>
