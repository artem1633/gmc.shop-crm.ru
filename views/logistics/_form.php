<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Client;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Logistics */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logistics-form">

    <?php $form = ActiveForm::begin(); ?> 

    <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3">
            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date_application')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]); 
            ?>  
        </div>
         <div class="col-md-3">
            <?= $form->field($model, 'stage')->dropDownList($model->getStage(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'order_id')->dropDownList($model->getClientList(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'other')->textarea(['rows' => 4]) ?>
        </div>
        <div class="col-md-10" style="margin-top: 20px">
            <?= $form->field($model, 'ptichka')->checkbox(); ?>
        </div>
        <div class="col-md-2" style="margin-top: 20px">
            <?= $form->field($model, 'arxiv')->checkbox(); ?>
        </div>
    </div>
    <div style="display: none;">
        
        <?= $form->field($model, 'date_application_created')->textInput(['value' => date('H:i d-m-Y')]) ?>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
