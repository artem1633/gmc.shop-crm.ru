<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;  
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */ 

$this->title = "Логистика";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);  

?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="panel panel-inverse settings-index leftindex">
    <div class="panel-heading" style="background: #28292b">
        <div class="btn-group pull-right" >
            <?php if($archive == 0){?>
             <button type="button" class="btn btn btn-xs arxiv">Показать все</button>
            <?php } elseif($archive == 1){?>
             <button type="button"  class="btn btn btn-xs arxiv" >Показать</button>
            <?php } elseif($archive == 2){?>
             <button type="button"  class="btn btn btn-xs arxiv" >Скрыть </button>
            <?php }else{?>
            <button type="button" class="btn btn btn-xs arxiv">Сортировать</button>
            <?php }?>
            <button type="button" class="btn btn btn-xs dropdown-toggle dropdown1" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu " >
                <li class="rabsize2" style="width :200px;text-align-last: left;"> <?= $archive == 0 ? Html::a('Показать все', ['index', 'archive' => 0],[ 'data-pjax' => 0,]) : Html::a('Показать все', ['index','archive' => 0],[ 'data-pjax' => 0,])?></li>
                <li class="rabsize2" style="width :200px;text-align-last: left;"> <?= $archive == 1 ? Html::a('Показать', ['index', 'archive' => 1],[ 'data-pjax' => 0,]) : Html::a('Показать', ['index','archive' => 1],[ 'data-pjax' => 0,])?></li>
                <li class="rabsize2" style="width :200px;text-align-last: left;"> <?= $archive == 2 ? Html::a('Скрыть', ['index', 'archive' => 2],[ 'data-pjax' => 0,]) : Html::a('Скрыть', ['index','archive' => 2],[ 'data-pjax' => 0,])?></li>
            </ul>
        </div>
        <h4 class="panel-title rabsize3 left1">Логистика</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 25px','class'=>'btn btn']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px',  'class'=>'btn btn', 'title'=>'Обновить']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>
</body>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
