<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Logistics */
?>
<div class="logistics-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
