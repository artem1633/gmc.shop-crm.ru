<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TenderFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "asdas";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
            <?=GridView::widget([
            'id'=>'file',
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            ])?>
