<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TenderFile */
?>
<div class="tender-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
