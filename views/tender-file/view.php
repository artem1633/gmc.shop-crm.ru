<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TenderFile */
?>
<div class="tender-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tender_id',
            'name',
            'path',
            'created_at',
        ],
    ]) ?>

</div>
