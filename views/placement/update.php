<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Placement */
?>
<div class="placement-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
