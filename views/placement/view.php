<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Placement */
?>
<div class="placement-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
