<?php
use yii\helpers\Url;
use yii\helpers\Html;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'application_date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->application_date, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'check_date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->check_date, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'devices_id',
        'content' => function ($data) {
            return $data->devices->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'series',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_next_check',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_next_check, 'php:d.m.Y');
        },
    ],
    [
        'attribute'=>'status',
        'format'=> 'raw',
        'value'=> function($data){
            return \yii\helpers\ArrayHelper::map([
                ['id'=> 1,'title'=>'Новая'],
                ['id'=> 2,'title'=>'В работе'],
                ['id'=> 3,'title'=>'Выполнена'],
            ],'id','title')[$data->status];
        }
    ],
    [ 
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/verification/update-verification', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/verification/delete-verification', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного поверки?'
                ]);
            },
        ]
    ]

];   