<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'contact_id',
        'content' => function ($data) {
            return $data->contact->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_call_id',
        'content' => function ($data) {
            return $data->statusCall->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'users_id',
        'content' => function ($data) {
            return $data->users->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'next_contact',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->next_contact, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'visible' => Yii::$app->user->identity->type == 1 ? true : false,
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                if(Yii::$app->user->identity->type == 1){
                    $url = Url::to(['/call/update-call', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                }
            },
            'leadDelete' => function ($url, $model) {
                if(Yii::$app->user->identity->type == 1){
                    $url = Url::to(['/call/delete-call', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить данного звонку?',
                    ]);
                }
            },
        ]
    ]

];   