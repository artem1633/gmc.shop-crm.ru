<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn', 
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_expand-order-goods', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->created_date, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->payment_date, 'php:d.m.Y');
        },
    ],
    [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Не оплачено',],
                        ['id' => 2,
                            'title' => 'Оплачено',],
                    ], 'id', 'title')[$data->status];
                }
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'discount',
    ],  
    [
                'attribute' => 'base_id',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Счет',],
                        ['id' => 2,
                            'title' => 'Коммерческое предложения',],
                    ], 'id', 'title')[$data->base_id];
                }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'responsible',
        'content' => function ($data) {
            return $data->responsible0->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_sum',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'total_nds_sum',
    ], 
    [
        'attribute'=>'product_create',
        'class'=>'\kartik\grid\DataColumn',
        'content' => function ($data) 
        {
            return '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-size:10px" class="btn btn-primary" role="modal-remote" href="'.Url::toRoute(['/goods/add', 'order_id' => $data->id]).'"><i class="fa fa-plus"></i></a>';
        },
    ],
    // [
    //     'attribute'=>'product_create',
    //     'label' => 'Печатать',
    //     'class'=>'\kartik\grid\DataColumn',
    //     'content' => function ($data) 
    //     {
    //         return Html::a( '<b>Создать счёт</b>', ['/client/print?id='.$data->client_id.'&document=invoice'], [ 'style' => 'font-size:12px;', 'target' => 'blank', 'data-pjax' => 0]) . ' / ' . Html::a( '<b>Договор поставки</b>', ['/client/print2?id='.$data->id.'&document=invoice'], ['style' => 'font-size:12px;', 'target' => 'blank', 'data-pjax' => 0]);
    //         return '
    //         <ul style="margin-left:-30px;">
    //             <li>' . 
    //                 Html::a( '<b>Создать счёт</b>', ['/client/print?client_id='.$data->client_id.'&order_id='.$data->id.'&document=invoice'], ['target' => 'blank', 'data-pjax' => 0]) . 
    //             '</li>
    //             <li>' . 
    //                 Html::a( '<b>Договор поставки</b>', ['/client/print2?client_id='.$data->client_id.'&order_id='.$data->id.'&document=invoice'], ['target' => 'blank', 'data-pjax' => 0]) . 
    //             '</li>                                                             
    //         </ul>';
    //     },
    // ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{leadUpdate} {leadDelete}',
        'buttons'  => [
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/orders/update-orders', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/orders/delete-orders', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного заказы?'
                ]);
            },
        ]
    ]

];   