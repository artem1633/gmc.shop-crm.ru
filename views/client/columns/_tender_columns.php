<?php
use yii\helpers\Url;
use yii\helpers\Html;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'date',
//        'content' => function ($data) {
//            return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
//        },
//    ],
    [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Актуальная',],
                        ['id' => 2,
                            'title' => 'Архив',],
                    ], 'id', 'title')[$data->status];
                }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'procedure_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'customer',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'term',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->term, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'users_id',
        'content' => function ($data) {
            return $data->users->fio;
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/tender/update-tender', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/tender/delete-tender', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного тендера?'
                ]);
            },
        ]
    ]

];   