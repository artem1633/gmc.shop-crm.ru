<?php
use yii\helpers\Url;
use yii\helpers\Html;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'date',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date,'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'time',
        'content' => function ($time) {
            return \Yii::$app->formatter->asTime($time->time,'php: H:i');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'who_users_id',
        'content' => function ($data) {
            return $data->whoUsers->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'to_users_id',
        'content' => function ($data) {
            return $data->toUsers->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'terms',
    ],
    [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Выполнено',],
                        ['id' => 2,
                            'title' => 'На проверке',],
                        ['id' => 3,
                            'title' => 'Есть уточнение',],
                        ['id' => 4,
                            'title' => 'Новая',],
                    ], 'id', 'title')[$data->status];
                }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'attachment',
         'content' => function ($data) {
            return Html::a($data->attachment, ['/client/send-file', 'file' => $data->attachment],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/tasks/update-tasks', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/tasks/delete-tasks', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного задачи?'
                ]); 
            },
        ]
    ]

];   