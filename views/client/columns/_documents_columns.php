<?php
use yii\helpers\Url;
use yii\helpers\Html;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'date', 
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documents_type_id',
        'content' => function ($data) {
            return $data->documentstype->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'content' => function ($data) {
            return Html::a($data->file, ['/client/send-document-file', 'file' => $data->file],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ownership',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'working',
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'stage_id',
        'content' => function ($data) {
            return $data->stage->name;
        },
    ],
    
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'users_id',
        'content' => function ($data) {
            return $data->users->fio;
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} {leadDelete}',
        'buttons'  => [
            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/documentation/update-documentation', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/documentation/delete-documentation', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного документы?'
                ]);
            },
        ]
    ]

];   