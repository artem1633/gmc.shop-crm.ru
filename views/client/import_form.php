<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
 <?php $form = ActiveForm::begin(['options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>
    <div class="row">
        <img style="width: 100%;margin-top: -60px;" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-12">
            <?= $form->field($model, 'file')->fileInput() ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>