<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;  
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */ 
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 
$this->title = "Клиенты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
    
</head>
<body >
<div class="panel panel-inverse client-index leftindexclient">
    <div class="panel-heading" style="background: #28292b">
        <div class="btn-group pull-right">
            <button type="button" class="btn btn btn-xs arxiv">Импорт</button>
            <button type="button" class="btn btn btn-xs dropdown-toggle dropdown1" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu rabsize2" role="menu">
                <li><?= Html::a('Компания', ['/client/import-company'], ['role'=>'modal-remote','class'=>'top','style'=>'width :200px;text-align: left;'])?></li>
                <li><?= Html::a('Звонки', ['/client/import-calls'], ['role'=>'modal-remote','class'=>'top','style'=>'width :200px;text-align: left;'])?></li>
            </ul>
        </div>
        <h4 class="panel-title rabsize3 left1">Клиенты</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
//            'pager' => [
//                'class' => \kop\y2sp\ScrollPager::className(),
//                'container' => '.grid-view tbody',
//                'item' => 'tr',
//                'paginationSelector' => '.grid-view .pagination',
//                'triggerTemplate' => '<tr class="ias-trigger"><td colspan="100%" style="text-align: center"><a style="display:none; cursor: pointer">{text}</a></td></tr>',
//                'eventOnScroll' => 'function() {$(\'.ias-trigger a\').trigger(\'click\')}',
//             ],
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 25px','class'=>'btn btn save']).'&nbsp;'.
                Html::a('<i class="fa fa-repeat"></i>', [''],
                    ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px','class'=>'btn btn save', 'title'=>'Обновить']),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>'',
            ]
            ])?>
        </div>
    </div>
</div>
</body>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>