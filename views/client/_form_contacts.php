<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\Zayavka;
use app\models\Contacts;
use unclead\multipleinput\MultipleInput;
use kartik\tabs\TabsX;
?>

    <?php $form = ActiveForm::begin(['id' => 'contacts-form']); ?> 
      <?php $contacts = contacts::find()->where(['client_id'=> $model->id])->all();
        $array = [];
        foreach ($contacts as $value) {
            $array [] = 
            [
                'fio'    => $value->fio,
                'position'   => $value->position,
                'phone'  => $value->phone,
                'email' => $value->email,
            ];
        }

        $model->contacts = $array;
       
            echo $form->field($model, 'contacts')->widget(MultipleInput::className(), [
                    'id' => 'my_id',
                    'columns' => [
                        [
                            'name'  => 'fio',
                            'title' => 'ФИО',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'position',
                            'title' => 'Должность',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                            ]
                        ],
                        [
                            'name'  => 'phone',
                            'title' => 'Телефон',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'Email',
                            'title' => 'Email',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],              
                    ]
                ])->label('Контакты');?>

    

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group pull-right" style="margin: 0;">
                <?= Html::submitButton($model->isNewRecord ? 'Далее' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>


    
    <?php ActiveForm::end(); ?>