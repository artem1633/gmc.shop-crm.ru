<?php
use yii\helpers\Url; 
use app\models\Users;
use app\models\City;
use app\models\RelevanceClient;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\helpers\Html; 
use dosamigos\datepicker\DatePicker;

return [
   /* [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],*/
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'working_title',
        'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
        'width' => '30%',
        'content' => function($model){
            $contacts = \app\models\Contacts::find()->where(['client_id' => $model->id])->all();
            if(count($contacts) != 0){
                $title = '';
                foreach ($contacts as $contact){
                    /** @var $contact \app\models\Contacts */
                    $title .= "{$contact->fio}\t{$contact->position}\t{$contact->phone}\t{$contact->email}\n";
                }
            } else {
                $title = '(нет контактов)';
            }
            return Html::tag('p',$model->working_title, [
                'title' => $title,
            ]);
        }
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city_id',
        'filter' => ArrayHelper::map(City::find()->all(),'id','name'),
        'content' => function ($data) {
             return $data->city->name;
         },
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tin_cat',
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'curator',
        'filter' => ArrayHelper::merge([
            -1 => 'Все',
        ], ArrayHelper::map(Users::find()->all(),'id','fio')),
        'content' => function ($data) {
             return $data->curator0->fio;
         },
          'filterInputOptions' => [
              'prompt' => 'Выберите куратора',
              'class' => 'form-control',
          ],
          'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'poslednego_zvonka',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->poslednego_zvonka, 'php:d.m.Y');
        },
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
        'filter' => DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'poslednego_zvonka',
            'language' => 'ru',
                'clientOptions' => [
            'language' => 'ru',
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
        ]),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sleduyushchego_zvonka',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->sleduyushchego_zvonka, 'php:d.m.Y');
        },
         'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
        'filter' => DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'sleduyushchego_zvonka',
                'clientOptions' => [
            'language' => 'ru',
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
        ]),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'relevance',
        'filter' => ArrayHelper::map(RelevanceClient::find()->all(),'id','name'),
        'content' => function ($data) {
             return $data->relevance0->name;
         },
          'contentOptions' => function ($model, $key, $index, $column) {
            return ['style' => 'color:black;background-color:'.$model->relevance0->color ];
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{add-call}&nbsp;&nbsp;&nbsp;{view}&nbsp;&nbsp;&nbsp;{leadDelete}',
        'viewOptions'=>['data-pjax'=>0,'title'=>'','data-toggle'=>'tooltip'],
        'buttons'  => [
            'add-call' => function($url, $model){
                return Html::a('<span class="glyphicon glyphicon-phone"></span>', ['call/add', 'client_id' => $model->id, 'pjax_container' => 'crud-datatable-pjax'], [
                    'role' => 'modal-remote',
                ]);
            },
            'leadDelete' => function ($url, $model) {
                if(Yii::$app->user->identity->type == 1){
                    $url = Url::to(['/client/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить данного клиента?',
                    ]);
                }
            },
        ]
    ]

];   