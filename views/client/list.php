<?=yii\widgets\ListView::widget([
                  'dataProvider' => $dataProvider,
                  'itemOptions' => ['class' => 'item'],
                  'itemView' =>'view2',
                  'pager' => [
                    'class' => \kop\y2sp\ScrollPager::className(),
                    'negativeMargin' => '200',
                    'triggerText' => 'Load More news',
                    'triggerOffset' => 500,
                    'noneLeftText' => '',
                    ],
                    'summary' => '',
                ]);?>