<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Client;
use yii\widgets\Pjax;
use app\models\Users;
use kartik\grid\GridView;
use yii\helpers\HtmlPurifier; 
use rmrevin\yii\module\Comments; 


\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Просмотр/Изменение';

$client = Client::find()->where(['id'=>$model->id])->one();
$fio = Users::find()->where(['id' =>$client->curator])->one();
$fio2 = Users::find()->all();
$fio3 = Users::find()->where(['id' =>\Yii::$app->user->identity->id])->one();
$client2 = Client::find()->all();
?>
 <head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >   
<div class="panel panel-inverse ">
    <div class="panel-heading" style="background: #28292b" >
        <h2 class="panel-title rabsize3 left3" style="height: 20px">
            <?=$model->working_title?> <p style="text-align: right;margin-top: -20px;"><?=$fio->fio?></p>
        </h2>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs rabsize3" style="background: #ffffff;margin-left: 110px;">
                    <li class="active" >
                        <a class="colortab" href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs" >Информация</span>
                            <span class="hidden-xs" >Информация</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="colortab " href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs" style="color: #fff;">Звонки</span>
                            <span class="hidden-xs" >Звонки</span>
                        </a>
                    </li>
                    <?php if($fio->fio==$fio3->fio||$fio3->type=="1"){ ?>
                    <li class="">
                        <a class="colortab" href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Документы</span>
                            <span class="hidden-xs">Документы</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="colortab" href="#default-tab-4" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Задачи</span>
                            <span class="hidden-xs">Задачи</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="colortab" href="#default-tab-5" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Заказы</span>
                            <span class="hidden-xs">Заказы</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="colortab" href="#default-tab-6" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Поверки</span>
                            <span class="hidden-xs">Поверки</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="colortab" href="#default-tab-7" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Тендера</span>
                            <span class="hidden-xs">Тендера</span>
                        </a>
                    </li> 
                    <li class="">
                        <a class="colortab" href="#default-tab-8" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Комментарии </span>
                            <span class="hidden-xs">Комментарии </span>
                        </a>
                    </li>
                    <?php }?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="default-tab-1">
                        <div class="row">
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'personal-pjax']) ?> 
                                 
                                <div class="col-md-7 col-sm-12">
                                    <div class="table-responsive">                                    
                                        <table class="table table-bordered">
                                            <h3 class="rabsize">Информация <a class="btn btn iconcolor" role="modal-remote" href="<?=Url::toRoute(['client/update', 'id' => $model->id])?>"><i class="fa fa-pencil" ></i></a></h3>
                                                    <tbody class="rabsize1">
                                                        <tr>
                                                            <th style="width:200px"><b><?=$model->getAttributeLabel('working_title')?></b></th>
                                                            <td style="width:200px"><?=Html::encode($model->working_title)?></td>
                                                            <th style="width:200px"><b><?=$model->getAttributeLabel('k_c')?></b></th>
                                                            <td style="width:200px"><?=Html::encode($model->k_c)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('kpp')?></b></th>
                                                            <td><?=Html::encode($model->kpp)?></td>  
                                                            <th><b><?=$model->getAttributeLabel('ogrn')?></b></th>
                                                            <td><?=Html::encode($model->ogrn)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('name_floor')?></b></th>
                                                            <td><?=Html::encode($model->name_floor)?></td>
                                                            <th><b><?=$model->getAttributeLabel('name_juice')?></b></th>
                                                            <td><?=Html::encode($model->name_juice)?></td> 
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('head_of')?></b></th>
                                                            <td><?=Html::encode($model->head_of)?></td>
                                                            <th><b><?=$model->getAttributeLabel('phiz_address')?></b></th>
                                                            <td><?=Html::encode($model->phiz_address)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('jur_address')?></b></th>
                                                            <td><?=Html::encode($model->jur_address)?></td>
                                                            <th><b><?=$model->getAttributeLabel('bic')?></b></th>
                                                            <td><?=Html::encode($model->bic)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('bank')?></b></th>
                                                            <td><?=Html::encode($model->bank)?></td>
                                                            <th><b><?=$model->getAttributeLabel('r_c')?></b></th>
                                                            <td><?=Html::encode($model->r_c)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('poslednego_zvonka')?></b></th>
                                                            <td><?= $model->poslednego_zvonka != null ? Html::encode(\Yii::$app->formatter->asDate($model->poslednego_zvonka, 'php:d.m.Y')) : '' ?></td>
                                                            <th><b><?=$model->getAttributeLabel('sleduyushchego_zvonka')?></b></th>
                                                            <td><?= $model->sleduyushchego_zvonka != null ? Html::encode(\Yii::$app->formatter->asDate($model->sleduyushchego_zvonka, 'php:d.m.Y')) : '' ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('relevance')?></b></th>
                                                            <td><?=Html::encode($model->relevance0->name)?></td>
                                                            <td><?=Html::encode($model->tin_cat)?></td>
                                                            <th><b><?=$model->getAttributeLabel('tin_cat')?></b></th>                                           
                                                        </tr> 
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('curator')?></b></th>
                                                            <td><?=Html::encode($model->curator0->fio)?></td>
                                                            <th><b><?=$model->getAttributeLabel('city_id')?></b></th>
                                                            <td><?=Html::encode($model->city->name)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2"><b><?=$model->getAttributeLabel('comment')?></b></th>
                                                            <td colspan="2"><?=Html::encode($model->comment)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('gender_occupation')?></b></th>
                                                            <td><?=Html::encode($model->gender_occupation)?></td>
                                                            <th><b><?=$model->getAttributeLabel('creator')?></b></th>
                                                            <td><?=Html::encode($model->creator0->fio)?></td>
                                                        </tr>
                                                        <tr>
                                                            <th><b><?=$model->getAttributeLabel('date_creation')?></b></th>
                                                            <td><?= $model->date_creation != null ? Html::encode(\Yii::$app->formatter->asDate($model->date_creation, 'php:H:i d.m.Y')) : '' ?></td> 
                                                            <th><b><?=$model->getAttributeLabel('site')?></b></th>
                                                            <td><?=Html::encode($model->site)?></td>
                                                        </tr>
                                                    </tbody>
                                        </table>  
                                    </div>
                                </div>
                                
                                <?php if($fio->fio==$fio3->fio||$fio3->type=="1"){ ?>
                                <div class="col-md-5 col-sm-12 rabsize1">
                                            <h3 class="rabsize">Контакты <a class="btn btn iconcolor" role="modal-remote" href="<?=Url::toRoute(['/contacts/add',  'client_id' => $model->id])?>"><i class="fa fa-plus"></i></a>
                                            </h3>
                                                    <?=GridView::widget([
                                                    'dataProvider' => $contactsdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_contacts_columns.php'),
                                                    'panelBeforeTemplate' => '',
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'showPageSummary' => false,
                                                    ])?>
                                </div>
                            <?php }?>
                                <?php Pjax::end() ?> 
                        </div>
                    </div>
                    <div class="tab-pane fade " id="default-tab-2">
                       
                        <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'calls-pjax']) ?>
                                    <div class="col-md-12">  

                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'calls-datatable',
                                                    'dataProvider' => $callsdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_calls_columns.php'),
                                                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['/call/add', 'client_id' => $model->id], ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 71px','class'=>'btn btn']),
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>'',
                                                    ]
                                                    ])?> 
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                         
                    </div>
                    <div class="tab-pane fade" id="default-tab-3">
                         <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'documentation-pjax']) ?>
                                    <div class="col-md-12">  

                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'documentation-datatable',
                                                    'dataProvider' => $documentationdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_documents_columns.php'),
                                                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['/documentation/add', 'client_id' => $model->id],
                                                            ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 71px','class'=>'btn btn']),
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>'',
                                                    ]
                                                    ])?>
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-4">
                           <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'tasks-pjax']) ?>
                                    <div class="col-md-12">  

                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'tasks-datatable',
                                                    'dataProvider' => $tasksdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_tasks_columns.php'),
                                                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['/tasks/add', 'client_id' => $model->id],
                                                            ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 71px','class'=>'btn btn']),
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>'',
                                                    ]
                                                    ])?>
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="default-tab-5">
                        <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'orders-pjax']) ?>
                                    <div class="col-md-12">  
                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'orders-datatable',
                                                    'dataProvider' => $ordersdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_orders_columns.php'),
                                                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['/orders/add', 'client_id' => $model->id],
                                                             ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 71px','class'=>'btn btn']) . '&nbsp',
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>'',
                                                    ]
                                                    ])?>
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="default-tab-6">
                         <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'verification-pjax']) ?>
                                    <div class="col-md-12">  
                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'verification-datatable',
                                                    'dataProvider' => $verificationdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_verification_columns.php'),
                                                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['/verification/add', 'client_id' => $model->id],
                                                            ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 71px','class'=>'btn btn']),
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>'',
                                                    ]
                                                    ])?>
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="default-tab-7">
                         <div class="row">
                            <div class="col-md-12" >
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'tender-pjax']) ?>
                                    <div class="col-md-12">  

                                            <div class="panel-body">
                                                <div id="ajaxCrudDatatable">
                                                    <?=GridView::widget([
                                                    'id'=>'tender-datatable',
                                                    'dataProvider' => $tenderdataProvider,
                                                    'pjax'=>true,
                                                    'columns' => require(__DIR__.'/columns/_tender_columns.php'),
                                                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['/tender/add', 'client_id' => $model->id],
                                                            ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 71px','class'=>'btn btn']),
                                                    'striped' => true,
                                                    'condensed' => true,
                                                    'responsive' => true,
                                                    'panel' => [
                                                    'headingOptions' => ['style' => 'display: none;'],
                                                    'after'=>'',
                                                    ]
                                                    ])?>
                                                </div>
                                            </div>                                
                                    </div>
                                <?php Pjax::end() ?> 
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-8">
                        <?php echo Comments\widgets\CommentListWidget::widget(['entity' => (string) $model->id,]);?>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
</body>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>