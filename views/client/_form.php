<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use app\models\Contacts; 
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body > 
<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>


     <div class="row" > 
         <div class="row">
             <div class="col-md-12">
                 <p style="padding: 0 11px; margin-bottom: 5px; margin-bottom: 15px;"><b>Название компании</b> пишется без формы коммерческой организации и без "". Наример: Вместо ООО “ЗЕМЛЕДЕЛ” просто: ЗЕМЛЕДЕЛ</p>
             </div>
         </div>
        <div class="col-md-3">
            <?= $form->field($model, 'working_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'gender_occupation')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name_floor')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name_juice')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row"> 
        <div class="col-md-4">
            <?= $form->field($model, 'city_id')->dropDownList($model->getCitys(), ['prompt' => 'Выберите город']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'poslednego_zvonka')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'sleduyushchego_zvonka')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'tin_cat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
             <?= $form->field($model, 'kpp')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999999999',
                'options' => ['class'=>'form-control','placeholder' => '999999999']
                ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'k_c')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '99999999999999999999',
                'options' => ['class'=>'form-control','placeholder' => '99999999999999999999']
                ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ogrn')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999999999999',
                'options' => ['class'=>'form-control','placeholder' => '9999999999999']
                ]) ?>
        </div>
        
    </div>

     <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'jur_address')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phiz_address')->textarea(['rows' => 3]) ?>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'site')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
        </div>
    </div> 
     <div class="row">
        <div class="col-md-4">
             <?= $form->field($model, 'bic')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999999999',
                'options' => ['class'=>'form-control','placeholder' => '999999999']
                ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'bank')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'r_c')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '99999999999999999999',
                'options' => ['class'=>'form-control','placeholder' => '99999999999999999999']
                ]) ?>
        </div>
    </div>
     
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'curator')->dropDownList($model->getUsers(), ['prompt' => 'Выберите куратора']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'relevance')->dropDownList($model->getRelevance(), ['prompt' => 'Выберите актуальность клиента']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'head_of')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php        
            echo $form->field($model, 'contacts')->widget(MultipleInput::className(), [
                    'id' => 'my_id',
                    'columns' => [
                        [
                            'name'  => 'fio',
                            'title' => 'ФИО',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'position',
                            'title' => 'Должность',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority',
                            ]
                        ],
                        [
                            'name'  => 'phone',
                            'title' => 'Телефон',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],
                        [
                            'name'  => 'email',
                            'title' => 'Email',
                            'enableError' => true,
                            'options' => [
                                'class' => 'input-priority'
                            ]
                        ],              
                    ]
                ])->label('Контакты');?>
        </div>
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</body>


<?php

$script = <<< JS
    $('#client-tin_cat').change(function(){
        var value = $(this).val();
        $.get('/client/search-by-inn?s='+value, function(response){
            if(response.suggestions.length > 0){
                var data = response.suggestions[0].data;
                $('#client-name_floor').val(data.name.full_with_opf);
                $('#client-name_juice').val(data.name.short_with_opf);
                $('#client-kpp').val(data.kpp);
                $('#client-ogrn').val(data.ogrn);
                $('#client-jur_address').val(data.address.value);
            }
        });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>