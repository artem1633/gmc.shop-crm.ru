<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use app\models\Contacts;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */
$permission = Yii::$app->user->identity->type;
?>
 
<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="row"> 
        <img style="width: 100%;margin-top: -60px;" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3">
            <?= $form->field($model, 'working_title')->textInput(['maxlength' => true, 'disabled' => ($model->working_title != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'gender_occupation')->textInput(['maxlength' => true, 'disabled' => ($model->gender_occupation != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name_floor')->textInput(['maxlength' => true, 'disabled' => ($model->name_floor != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name_juice')->textInput(['maxlength' => true, 'disabled' => ($model->name_juice != null && $permission == 4) ? true : false ]) ?>
        </div>
    </div>
     <div class="row"> 
         <div class="col-md-4">
            <?= $form->field($model, 'city_id')->dropDownList($model->getCitys(), ['prompt' => 'Выберите город', 'disabled' => ($model->city_id != null && $permission == 4) ? true : false ]) ?>
        </div>
         <div class="col-md-4">
            <?= $form->field($model, 'poslednego_zvonka')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'disabled' => ($model->poslednego_zvonka != null && $permission == 4) ? true : false],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'sleduyushchego_zvonka')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'disabled' => ($model->sleduyushchego_zvonka != null && $permission == 4) ? true : false],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'tin_cat')->textInput(['maxlength' => true, 'disabled' => ($model->tin_cat != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-3">
             <?= $form->field($model, 'kpp')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999999999',                
                'options' => [
                    'class'=>'form-control',
                    'disabled' => ($model->kpp != null && $permission == 4) ? true : false,
                    'placeholder' => '9999999999'
                ]
                ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'k_c')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '99999999999999999999',                
                'options' => [
                    'class'=>'form-control',
                    'disabled' => ($model->k_c != null && $permission == 4) ? true : false,
                    'placeholder' => '99999999999999999999'
                ]
                ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ogrn')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999999999999',                
                'options' => [
                    'class'=>'form-control',
                    'disabled' => ($model->ogrn != null && $permission == 4) ? true : false,
                    'placeholder' => '9999999999999'
                ]
                ]) ?>
        </div>
        
    </div>

     <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'jur_address')->textarea(['rows' => 3, 'disabled' => ($model->jur_address != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'phiz_address')->textarea(['rows' => 3, 'disabled' => ($model->phiz_address != null && $permission == 4) ? true : false ]) ?>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'site')->textarea(['rows' => 3, 'disabled' => ($model->site != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'comment')->textarea(['rows' => 3, 'disabled' => ($model->comment != null && $permission == 4) ? true : false ]) ?>
        </div>
    </div> 
     <div class="row">
        <div class="col-md-4">
             <?= $form->field($model, 'bic')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999999999',                
                'options' => [
                    'class'=>'form-control',
                    'disabled' => ($model->bic != null && $permission == 4) ? true : false,
                    'placeholder' => '999999999'
                ]
                ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'bank')->textInput(['maxlength' => true,'disabled' => ($model->bank != null && $permission == 4) ? true : false,]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'r_c')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '99999999999999999999',
                'options' => [
                    'class'=>'form-control',
                    'disabled' => ($model->r_c != null && $permission == 4) ? true : false,
                    'placeholder' => '99999999999999999999'
                ]
                ]) ?>
        </div>
    </div>
     
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'curator')->dropDownList($model->getUsers(), ['prompt' => 'Выберите куратора', 'disabled' => ($model->curator != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'relevance')->dropDownList($model->getRelevance(), ['prompt' => 'Выберите актуальность клиента', 'disabled' => ($model->relevance != null && $permission == 4) ? true : false ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'head_of')->textInput(['maxlength' => true, 'disabled' => ($model->head_of != null && $permission == 4) ? true : false ]) ?>
        </div>
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
