<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 06.05.2017
 * Time: 13:38
 */

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'dataProvider' => $model->orderGoodsData,
    'showPageSummary' => true,
    'columns' => [
        [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'name',
        'content' => function ($data) {
            return $data->product->name;
            },
        ],
        [
            'class' => \kartik\grid\EditableColumn::className(),
            'editableOptions' => [
//                'inputType' => \kartik\editable\Editable::INPUT_MONEY,
                'formOptions' => [
                    'action' => \yii\helpers\Url::to(['edit'])
                ]
            ],
//        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
            'attribute' => 'count',
        ],
        [
            'class' => \kartik\grid\EditableColumn::className(),
            'editableOptions' => [
//                'inputType' => \kartik\editable\Editable::INPUT_MONEY,
                'formOptions' => [
                    'action' => \yii\helpers\Url::to(['edit'])
                ]
            ],
//        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
            'attribute' => 'cost',
            'format' => ['currency', 'rub'],
            'pageSummary' => true,
        ],
//        [
//            'attribute' => 'cost',
//            'format' => ['currency', 'rub'],
//            'pageSummary' => true,
//        ],
        [
            'attribute' => 'provider',
        ],
        [
            'attribute' => 'provide_time',
        ],
        [
            'attribute' => 'contract_time',
        ],
        [
            'class' => \kartik\grid\EditableColumn::className(),
            'attribute' => 'price_one_client',
            'editableOptions' => [
                'formOptions' => [
                    'action' => \yii\helpers\Url::to(['edit'])
                ]
            ],
            'format' => ['currency', 'rub'],
            'pageSummary' => true,
        ],
        [
            'class' => \kartik\grid\EditableColumn::className(),
            'attribute' => 'price_client',
            'editableOptions' => [
                'formOptions' => [
                    'action' => \yii\helpers\Url::to(['edit'])
                ]
            ],
            'format' => ['currency', 'rub'],
            'pageSummary' => true,
        ],
        [
            'attribute' => 'profit',
            'format' => ['currency', 'rub'],
            'pageSummary' => true,
        ],
        [
            'attribute' => 'sum_additional',
            'format' => ['currency', 'rub'],
            'pageSummary' => true,
        ],
        [
            'class'    => 'kartik\grid\ActionColumn',
            'template' => '{leadUpdate} {leadDelete}',
            'buttons'  => [
                
                'leadUpdate' => function ($url, $model) {
                    $url = Url::to(['/goods/update-goods', 'id' => $model->id, 'pjaxContainer' => '#crud-datatable-pjax']);
                    return Html::a('<span class="fa fa-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                },
                'leadDelete' => function ($url, $model) {
                    $url = Url::to(['/goods/delete-goods', 'id' => $model->id, 'pjaxContainer' => '#crud-datatable-pjax']);
                    return Html::a('<span class="fa fa-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить данного товар?'
                    ]);
                },
            ]
        ]
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
