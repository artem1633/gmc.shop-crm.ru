<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
?>
<div class="client-update">

    <?= $this->render('update_from', [
        'model' => $model,
    ]) ?>

</div>
