<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tender */
?>
<div class="tender-view">
  
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'client_id',
                'value' => function ($data) {
                   return $data->client->working_title;
                },
            ],
            [
                'attribute' => 'users_id',
                'value' => function ($data) {
                  return $data->users->fio;
                },
            ],
//            [
//                    'attribute' => 'date',
//                    'value' => function ($data) {
//                     if($data->date != null )  return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
//                    },
//            ],
            [
                    'attribute' => 'term',
                    'value' => function ($data) {
                     if($data->term != null )  return \Yii::$app->formatter->asDate($data->term, 'php:d.m.Y');
                    },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                   if($data->status == 1) return 'Актуальная';
                   if($data->status == 2) return 'Архив';
                },
            ],
            'number',
            'procedure_name',
            'customer',
        ],
    ]) ?>


    <div class="row">
        <div class="col-md-12">
            <h4 style="margin-top: 10px;">Файлы</h4>
            <?= $this->render('@app/views/tender-file/index', [
                'searchModel' => $filesSearchModel,
                'dataProvider' => $filesDataProvider,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= \kato\DropZone::widget([
                'id'        => 'dzImage', // <-- уникальные id
                'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'tender_id' => $model->id ]),
                'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                'options' => [
                    'maxFilesize' => '2',
                ],
                'clientEvents' => [
                    'complete' => "function(file){ $.pjax.reload('#file-pjax'); }",
                ],
            ]);?>
        </div>
        </div>
    </div>

</div>
