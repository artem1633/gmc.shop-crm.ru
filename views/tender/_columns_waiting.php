<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Client;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $model->techTasks,
            ]);
            return \Yii::$app->controller->renderPartial('@app/views/tech-task/index', ['dataProvider'=>$dataProvider, 'tenderId' => $model->id]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'attribute' => 'datetime',
        'label' => 'Дата',
        'format' => ['date', 'php:d.m.Y'],
    ],
    [
        'attribute' => 'datetime',
        'label' => 'Время',
        'format' => ['date', 'php:H:i'],
    ],
    [
        'attribute' => 'link',
        'content' => function($model){
            if($model->link != null){
                return \yii\helpers\Html::a($model->link, $model->link);
            }

            return Yii::$app->formatter->asText($model->link);
        }
    ],
    [
        'attribute' => 'description',
    ],
    [
        'attribute' => 'comment',
    ],
    [
        'attribute'=>'term',
        'format' => ['date', 'php:m.Y'],
    ],
    [
        'attribute' => 'waiting_stage_id',
        'label' => 'Статус'
    ],
    [
        'attribute' => 'auction_price_id',
    ],
    [
        'attribute' => 'provision',
    ],
    [
        'attribute' => 'provision_contract',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'procedure_name',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'client_id',
//        'filter' => ArrayHelper::map(Client::find()->all(),'id','working_title'),
//        'content' => function ($data) {
//            return $data->client->working_title;
//        },
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'date',
//        'content' => function ($data) {
//            return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
//        },
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'users_id',
//        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
//        'content' => function ($data) {
//            return $data->users->fio;
//        },
//    ],
//    [
//        'attribute' => 'status',
//        'filter' => array('1' => 'Актуальная' , '2' => 'Архив', '3' => 'Разбор тендер'),
//        'format' => 'raw',
//        'value' => function ($data) {
//            return \yii\helpers\ArrayHelper::map([
//                ['id' => 1,
//                    'title' => 'Актуальная',],
//                ['id' => 2,
//                    'title' => 'Архив',],
//            ], 'id', 'title')[$data->status];
//        }
//    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'customer',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'term',
//        'content' => function ($data) {
//            return \Yii::$app->formatter->asDate($data->term, 'php:d.m.Y');
//        },
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'number',
//    ],
    [
        'label' => 'Добавить ТЗ',
        'content' => function($model, $key){
            return \yii\helpers\Html::a('<i class="fa fa-plus"></i>', ['tech-task/create', 'tender_id' => $key], ['class' => 'btn btn-primary', 'role' => 'modal-remote', 'style' => 'font-size: 10px;']);
        }
    ],
    [
        'label' => 'Создать заказ',
        'content' => function($model, $key){
            return \yii\helpers\Html::a('<i class="fa fa-arrow-right"></i>', ['orders/create-by-tender', 'tender_id' => $key], ['class' => 'btn btn-primary', 'data-pjax' => 0, 'style' => 'font-size: 10px;']);
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view}{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key, 'pjaxContainer' => '#waiting-crud-datatable']);
        },
        'viewOptions'=>['role'=>'modal-remote','data-pjax'=>0,'title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Подтвердите действие',
            'data-confirm-message'=>'Вы уверены что хотите удалить данного тендера?'
        ],
    ],
];