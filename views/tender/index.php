<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;



$this->title = "Тендера";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >

<ul class="nav nav-tabs nav-justified" style="margin-bottom: 10px;">
    <li><a href="#tab-working" data-toggle="tab">Разбор тендеров</a></li>
    <li><a href="#tab-wait" data-toggle="tab">Подались ждем</a></li>
    <li><a href="#tab-won" data-toggle="tab">Выиграли</a></li>
    <li><a href="#tab-archive" data-toggle="tab">Архив</a></li>
</ul>

<div class="tab-content">
    <div id="tab-working" class="tab-pane fade">
        <div class="panel panel-inverse tender-index leftindex">
            <div class="panel-heading " style="background: #28292b">
                <h4 class="panel-title rabsize3 left1">Тендера</h4>
            </div>
            <div class="panel-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'working-crud-datatable',
                        'dataProvider' => $dataProviderWorking,
//                        'filterModel' => $searchModelWorking,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns_working.php'),
                        'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa"></i>', ['create', 'pjaxContainer' => '#working-crud-datatable'],
                                ['role'=>'modal-remote','title'=> 'Добавить','style'=>'color: #ffffff;background:#74b916;margin-left: 25px','class'=>'btn btn']).'&nbsp;'.
                            Html::a('<i class="fa fa-repeat"></i>', [''],
                                ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px', 'class'=>'btn btn', 'title'=>'Обновить']),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after'=>'',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
    <div id="tab-wait" class="tab-pane fade">
        <div class="panel panel-inverse tender-index leftindex">
            <div class="panel-heading " style="background: #28292b">
                <h4 class="panel-title rabsize3 left1">Тендера</h4>
            </div>
            <div class="panel-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'waiting-crud-datatable',
                        'dataProvider' => $dataProviderWait,
//                        'filterModel' => $searchModelWait,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns_waiting.php'),
                        'panelBeforeTemplate' =>
                            Html::a('<i class="fa fa-repeat"></i>', [''],
                                ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px', 'class'=>'btn btn', 'title'=>'Обновить']),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after'=>'',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
    <div id="tab-won" class="tab-pane fade">
        <div class="panel panel-inverse tender-index leftindex">
            <div class="panel-heading " style="background: #28292b">
                <h4 class="panel-title rabsize3 left1">Тендера</h4>
            </div>
            <div class="panel-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'won-crud-datatable',
                        'dataProvider' => $dataProviderWon,
//                        'filterModel' => $searchModelWon,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns_won.php'),
                        'panelBeforeTemplate' =>
                            Html::a('<i class="fa fa-repeat"></i>', [''],
                                ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px', 'class'=>'btn btn', 'title'=>'Обновить']),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after'=>'',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
    <div id="tab-archive" class="tab-pane fade">
        <div class="panel panel-inverse tender-index leftindex">
            <div class="panel-heading " style="background: #28292b">
                <h4 class="panel-title rabsize3 left1">Тендера</h4>
            </div>
            <div class="panel-body">
                <div id="ajaxCrudDatatable">
                    <?=GridView::widget([
                        'id'=>'archive-crud-datatable',
                        'dataProvider' => $dataProviderArchive,
//                        'filterModel' => $searchModelArchive,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/_columns_archive.php'),
                        'panelBeforeTemplate' =>
                            Html::a('<i class="fa fa-repeat"></i>', [''],
                                ['data-pjax'=>1,'style'=>'color: #ffffff;background:#74b916;margin-left: 12px', 'class'=>'btn btn', 'title'=>'Обновить']),
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'headingOptions' => ['style' => 'display: none;'],
                            'after'=>'',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
$(function(){
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop() || $('html').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
  });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>