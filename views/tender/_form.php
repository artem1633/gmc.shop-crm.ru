<?php
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Tender */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tender-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-4">
            <?= $form->field($model, 'datetime')->widget(\kartik\datetime\DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату и время', 'value' => date('d.m.Y H:i:s')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy HH:ii:ss',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'term')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'curator_id')->dropDownList(ArrayHelper::map(Users::find()->all(), 'id', 'fio'), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'procedure_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'customer_id')->widget(\kartik\select2\Select2::class, [
                'data' => $model->getClients(),
                'pluginOptions' => ['allowClear' => true, 'placeholder' => "Выберите"],
            ]) ?>
        </div>
        <div class="col-md-3">
           <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'form_action_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\FormAction::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'placement_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Placement::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'company_name_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\CompanyNames::find()->all(), 'id', 'name')) ?>
        </div>
<!--        <div class="col-md-3">-->
            <?php
//            echo $form->field($model, 'waiting_stage_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\WaitingStages::find()->all(), 'id', 'name'))
            ?>
<!--        </div>-->
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'provision')->input('number') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'provision_contract')->input('number') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'take_price')->input('number') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'time_delivery')->input('number');
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_payment')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea() ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'users_id')->textInput(['value' => \Yii::$app->user->identity->id]) ?>        
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
