<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tender */

?>
<div class="tender-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
