<?php

use yii\helpers\Html;

?>
<div class="tender-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
