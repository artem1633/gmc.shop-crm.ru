<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Users;
use dosamigos\datepicker\DatePicker;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'who',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function ($data) {
            return '<span style="margin-right: 80px;" >'.$data->who0->fio.'</span >';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'whom',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','fio'),
        'content' => function ($data) {
            return '<span style="margin-right: 80px;" >'.$data->whom0->fio.'</span >';
        },
    ],
    [
                'attribute' => 'status',
                'filter' => array('1' => 'Выполнено' , '2' => 'Нe выполнено'),
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span style="margin-right: 80px;" >'.\yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Выполнено',],
                        ['id' => 2,
                            'title' => 'Нe выполнено',],
                    ], 'id', 'title')[$data->status].'</span >';
                }
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date',
        'content' => function ($data) {
            return '<span style="margin-right: 70px;" >'.\Yii::$app->formatter->asDate($data->date, 'php:d.m.Y').'</span >';
        },
    ],
    [
        'attribute'=>'dobavit',
        'class'=>'\kartik\grid\DataColumn',
        'content' => function ($data) 
        {
            return '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-size:11px"  class="btn btn-primary" role="modal-remote" data-toggle="tooltip" href="'.Url::toRoute(['/organizer/add','id' => $data->id]).'">Перенести</a>';
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данного oрганайзер?'], 
    ],

];   