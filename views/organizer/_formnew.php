<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="organizer-form">

    <?php $form = ActiveForm::begin(); ?>
     <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-12">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'who')->textInput(['value' => \Yii::$app->user->identity->id]) ?>  
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
