<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

        if($post == null)
        {
            $post['OrganizerSearch']['search_who'] = null;
            $post['OrganizerSearch']['search_whom'] = null;
            $post['OrganizerSearch']['search_status'] = null;
        }

?> 

<?php $form = ActiveForm::begin(); ?>

<div class="row" style="margin-left: 15px;">
    <div class="col-md-4" >          
        <?= $form->field($model, 'search_who')->dropDownList($model->getUsers(),['prompt' => 'Bыберите ','value' => $post['OrganizerSearch']['search_who']])->label('Создатель'); ?>         
    </div>
    <div class="col-md-4">          
        <?= $form->field($model, 'search_whom')->dropDownList($model->getUsers(),['prompt' => 'Bыберите ','value' => $post['OrganizerSearch']['search_whom']])->label('Получатель'); ?>       
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'search_status')->dropDownList($model->getStatus(),['prompt' => 'Все','value' => $post['OrganizerSearch']['search_status']])->label('Статус'); ?> 
         
    </div>
    <div class="col-md-1" >
       <span > <?= Html::submitButton('Поиск', [ 'style'=>"margin-top: 22px;", 'class' => 'btn btn-primary']) ?></span >
    </div>  
</div> 
  
<?php ActiveForm::end(); ?>
