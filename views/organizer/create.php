<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Organizer */

?>
<div class="organizer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
