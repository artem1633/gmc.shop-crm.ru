<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Organizer */
?>
<div class="organizer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [ 
            'name', 
            [
            'attribute'=>'description',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
        ],
    ]) ?>

</div>
