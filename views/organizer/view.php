<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Organizer */
?>
<div class="organizer-view">
 
    <?= DetailView::widget([
        'model' => $model, 
        'attributes' => [
            //'id',
            'name',
            [
                'attribute' => 'who',
                'value' => function ($data) {
                   return $data->who0->fio;
                },
            ],
            [
                'attribute' => 'whom',
                'value' => function ($data) {
                   return $data->whom0->fio;
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                   if($data->status == 1) return 'Выполнено';
                   if($data->status == 2) return 'Нe выполнено';
                },
            ],
            [
                    'attribute' => 'date',
                    'value' => function ($data) {
                     if($data->date != null )  return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
                    },
            ],
            [
            'attribute'=>'description',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
        ],
    ]) ?>

</div>
