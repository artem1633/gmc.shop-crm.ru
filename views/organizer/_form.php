<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Organizer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organizer-form">

    <?php $form = ActiveForm::begin(); ?>
     <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>           
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>  
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'whom')->dropDownList($model->getUsers(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите', 'id' => 'type']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
        </div>
    </div>
    <div style="display: none;">
        <?= $form->field($model, 'verification')->textInput(['value' => 1]) ?>
        <?= $form->field($model, 'who')->textInput(['value' => \Yii::$app->user->identity->id]) ?>  
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
