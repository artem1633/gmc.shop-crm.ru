<?php

/**
 * @var \app\models\Goods $model
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>
  
<div class="call-form">

    <?php $form = ActiveForm::begin(); ?>
     <div class="row">
        <img style="width: 100%;margin-top: -60px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-6" >
            <?= $form->field($model, 'product_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getProducts(),
                'id' => 'type',
                'options' => ['placeholder' => 'Выберите'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
           <?= $form->field($model, 'count')->input('number') ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'provider')->textInput() ?>
        </div>

         <div class="col-md-3">
             <?= $form->field($model, 'provide_time')->textInput() ?>
         </div>

         <div class="col-md-3">
             <?= $form->field($model, 'contract_time')->textInput() ?>
         </div>

         <div class="col-md-6">
             <?= $form->field($model, 'provide_time')->textInput() ?>
         </div>

         <div class="col-md-6">
             <?= $form->field($model, 'cost')->input('number') ?>
         </div>

         <div class="col-md-6">
             <?= $form->field($model, 'price_one_client')->input('number') ?>
         </div>

         <div class="col-md-6">
             <?= $form->field($model, 'price_client')->input('number') ?>
         </div>

         <div class="col-md-6">
             <?= $form->field($model, 'profit')->input('number') ?>
         </div>

         <div class="col-md-6">
             <?= $form->field($model, 'sum_additional')->input('number') ?>
         </div>
    </div>
     <div style="display: none;">
        <?= $form->field($model, 'order_id')->textInput() ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>
