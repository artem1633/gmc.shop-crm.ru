<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Goods */
?>
<div class="goods-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'name',
            'count',
            'cost',
            'sum',
            'nds_protsent',
            'nds_sum',
            'total_sum',
        ],
    ]) ?>

</div>
