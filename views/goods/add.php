<?php

use yii\helpers\Html;

?>
<div class="goods-create">
    <?= $this->render('add_form', [
        'model' => $model,
    ]) ?>
</div>
