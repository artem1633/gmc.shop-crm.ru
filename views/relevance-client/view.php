<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RelevanceClient */
?>
<div class="relevance-client-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>
