<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RelevanceClient */
?>
<div class="relevance-client-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
