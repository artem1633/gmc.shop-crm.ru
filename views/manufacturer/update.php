<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manufacturer */
?>
<div class="manufacturer-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
