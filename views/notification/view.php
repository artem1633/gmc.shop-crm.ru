<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notification */
?>
<div class="notification-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'table_name',
            'field',
            'status',
            'user_id',
            'description',
        ],
    ]) ?>

</div>
