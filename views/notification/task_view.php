<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
?>
<div class="tasks-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'client_id',
                'value' => function ($data) {
                  return $data->client->working_title;
                },
            ],
            'title',
            [
                    'attribute' => 'date',
                    'value' => function ($data) {
                     if($data->date != null )  return \Yii::$app->formatter->asDate($data->date, 'php:d.m.Y');
                    },
            ],
            //'time',
            [
                'attribute' => 'who_users_id',
                'value' => function ($data) {
                  return $data->whoUsers->fio;
                },
            ],
            [
                'attribute' => 'to_users_id',
                'value' => function ($data) {
                  return $data->toUsers->fio;
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 1,
                            'title' => 'Выполнено',],
                        ['id' => 2,
                            'title' => 'На проверке',],
                        ['id' => 3,
                            'title' => 'Есть уточнение',],
                        ['id' => 4,
                            'title' => 'Новая',],
                    ], 'id', 'title')[$data->status];
                }
              ],
            [
            'attribute'=>'description',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
            'terms',
            //'attachment',

            ],
    ]) ?>

</div>
