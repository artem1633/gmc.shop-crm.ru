<?php
use yii\helpers\Url;
use yii\helpers\Html; 
use app\models\Orders;
use app\models\Client; 
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn', 
        'attribute'=>'manufacture',
        'content' => function ($data) {
            return $data->manufacture0->name;
        },
    ],
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_creation_application',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->date_creation_application, 'php:d.m.Y');
        },
    ],
    
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_number',
        'content' => function ($data) {
             return ('№: '.$data->order->id." Клиент: ".$data->order->client->working_title);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'documents',
        'content' => function ($data) {
            return Html::a($data->documents, ['/news/send-file', 'file' => $data->documents],['title'=> 'Скачать', 'data-pjax' => 0]);
        },
    ],
    [
                'attribute' => 'ptichka',
                'format' => 'raw',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 0,
                            'title' => 'Нет',],
                        ['id' => 1,
                            'title' => 'Да',],
                    ], 'id', 'title')[$data->ptichka];
                }
    ], 
    [ 
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'delivery_time',
        'content' => function ($data) {
            return \Yii::$app->formatter->asDate($data->delivery_time, 'php:d.m.Y');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'execution_phase',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить данную закупку у поставщиков?'],
    ],

];   