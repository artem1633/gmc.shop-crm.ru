<?php

use yii\widgets\DetailView;
use yii\helpers\Html; 
/* @var $this yii\web\View */
/* @var $model app\models\PurchasingSuppliers */
?>
<div class="purchasing-suppliers-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                    'attribute' => 'date_creation_application',
                    'value' => function ($data) {
                     if($data->date_creation_application != null )  return \Yii::$app->formatter->asDate($data->date_creation_application, 'php:d.m.Y');
                    },
            ],
            [
                'attribute' => 'ptichka',
                'value' => function ($data) {
                    return \yii\helpers\ArrayHelper::map([
                        ['id' => 0,
                            'title' => 'Нет',],
                        ['id' => 1,
                            'title' => 'Да',],
                    ], 'id', 'title')[$data->ptichka];
                }
             ], 
            [
                'attribute'=>'manufacture',
                'value' => function ($data) {
                    return $data->manufacture0->name;
                },
            ],
            'order_number',
            [
                'attribute'=>'order_number',
                'value' => function ($data) {
                     return ('№: '.$data->order->id." Клиент: ".$data->order->client->working_title);
                },
            ],
            'delivery_time',
            'execution_phase',
            [
            'attribute'=>'notes_order',
            'format'=>'ntext',   
            'contentOptions' => [
                'style'=>'max-width:150px; min-height:100px; overflow: auto; word-wrap: break-word;'
            ],
            ],
            [
                'attribute'=>'documents',
                'format'=>'html',  
                'value' => function ($data) {
                    return Html::a($data->documents, ['/news/send-file', 'file' => $data->documents],['title'=> 'Скачать', 'data-pjax' => 0]);
                },
            ],
        ],
    ]) ?>

</div>
