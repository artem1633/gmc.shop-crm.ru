<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Client;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\PurchasingSuppliers */ 
/* @var $form yii\widgets\ActiveForm */
?>
<head>
    <link href="/theme/assets/css/new.css" rel="stylesheet" />
</head>
<body >
<div class="purchasing-suppliers-form"> 

    <?php $form = ActiveForm::begin(); ?> 
    <div class="row">
        <img style="width: 100%;margin-top: -20px" src="/theme/assets/img/chiziq95.png" alt="">
        <div class="col-md-4 label1">
            <?=$model->getAttributeLabel('manufacture')?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'manufacture')->dropDownList($model->getManufacturerList(), [])->label(""); ?>      
        </div>
       
    </div>
    <div class="row">
        <div class="col-md-4 label1 lable3"  style="margin-top: 0px">
            <?=$model->getAttributeLabel('delivery_time')?>
        </div>
        <div class="col-md-4 lable3">
            <?= $form->field($model, 'delivery_time')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату', 'value' => date('d.m.Y')],
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true, 
                    'format' => 'dd.mm.yyyy',
                ]
            ])->label(""); 
            ?>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 label1 lable3"  style="margin-top: 0px">
            <?=$model->getAttributeLabel('execution_phase')?>
        </div>

        <div class="col-md-4 lable3" >
            <?= $form->field($model, 'execution_phase')->textInput(['maxlength' => true])->label(""); ?>        
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 label1 lable3"  style="margin-top: 0px">
            <?=$model->getAttributeLabel('order_number')?>
        </div>
        <div class="col-md-4 lable3">
            <?= $form->field($model, 'order_number')->dropDownList($model->getClientList(), ['prompt' => 'Выберите', 'id' => 'type'])->label(""); ?>
        </div>
        <div class="col-md-4" style="margin-top: -158px;">
            <?= $form->field($model, 'notes_order')->textarea(['rows' => 11])->label('<span class="label1">Примечание</span>'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" style="margin-top: -25px">
            <?= $form->field($model, 'ptichka')->checkbox(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label1" style="margin-top: 15px;">
            <?=$model->getAttributeLabel('other_file')?>
        </div>
         <div class="col-md-8">
            <?= $form->field($model, 'other_file')->fileInput()->label(""); ?>            
        </div>
         <div class="col-md-2" style="margin-top: 15px">
            <?= $form->field($model, 'arxiv')->checkbox(); ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'date_creation_application')->textInput(['value' => date('d-m-Y')])?>
        <?= $form->field($model, 'status')->textInput() ?>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
</body>