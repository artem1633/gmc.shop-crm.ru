<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

        if($post == null)
        {
            $post['PurchasingSuppliersSearch']['search_manufacture'] = null;
            $post['PurchasingSuppliersSearch']['search_notes_order'] = null;
            $post['PurchasingSuppliersSearch']['search_delivery_time'] = null;
       }

?> 

<?php $form = ActiveForm::begin(); ?>
 
<div class="row" style="margin-left: 30px;">
    <div class="col-md-4" >          
        <?= $form->field($model, 'search_manufacture')->textInput(['value' => $post['PurchasingSuppliersSearch']['search_manufacture'], 'placeholder' => 'Производитель/Поставщик'])->label(''); ?>         
    </div>
    <div class="col-md-3" style="margin-left: 31px;">          
        <?= $form->field($model, 'search_delivery_time')->textInput(['value' => $post['PurchasingSuppliersSearch']['search_delivery_time'], 'placeholder' => 'Срок поставки'])->label(''); ?>         
    </div>
    <div class="col-md-3" style="margin-left: 30px;">
        <?= $form->field($model, 'search_notes_order')->dropDownList($model->getClientList(),['prompt' => 'Bыберите номер заказа','value' => $post['PurchasingSuppliersSearch']['search_notes_order']])->label(''); ?> 
         
    </div>
    <div class="col-md-1" style="margin-left: 28px;">
       <span > <?= Html::submitButton('Поиск', [ 'style'=>"margin-top: 17px;", 'class' => 'btn btn-primary']) ?></span >
    </div>  
</div> 
  
<?php ActiveForm::end(); ?>
