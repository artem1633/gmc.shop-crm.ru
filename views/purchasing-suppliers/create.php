<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PurchasingSuppliers */

?>
<div class="purchasing-suppliers-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
