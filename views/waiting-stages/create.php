<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WaitingStages */

?>
<div class="waiting-stages-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
