<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WaitingStages */
?>
<div class="waiting-stages-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
