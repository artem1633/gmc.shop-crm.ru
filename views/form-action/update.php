<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FormAction */
?>
<div class="form-action-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
