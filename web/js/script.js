$(document).ready(function () {
	var page;
	var param = location.
				search.
				slice(location.search.indexOf('?')+1).
				split('&');
	
	var result = [];
	for(var i = 0; i < param.length;i++) {
		var res = param[i].split('=');
		result[res[0]] = res[1];
	}
	
	if(result['page']) {
		page = result['page'];
	}
	else {
		page = 1;
	}
	$(".pager").show().text(page);
	
	var block = false;
	$(window).scroll(function () {
		
		if($(window).height() + $(window).scrollTop() >= $(document).height() && !block) {
			block = true;
							//alert(page);
			$(".load").fadeIn(500, function () {
				page++;
				$.ajax({
					url:"data",
					type:"GET",
					data:"page="+page,
					success:function(html) {
						if(html) {
							//alert(html);
							//$(html).appendTo($("#ajaxCrudDatatable")).hide().fadeIn(1000);
							$(".pager").text(page);
						}
						$(".load").fadeOut(500);
						block = false;
					}
				});
			});
		}
	});
});