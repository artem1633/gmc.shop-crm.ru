var modalBody = $('body .modal-body');

//кнопка создания новой доски
$('#create-board-sprint').click(function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var modalContainer = $('#board-sprint-modal');
    var modalBody = modalContainer.find('.modal-body');

    if(url) {
        $.ajax({ type: "POST",
            url: url,
            success: function(data){
                $(modalBody).html(data);
                modalContainer.modal('show');
            }
        });
    }
});

//форма создания новой доски
modalBody.on('beforeSubmit', $('#ajax-board-order-sprint-form'), function() {

    var clientForm = $('#ajax-board-order-sprint-form');
    if(clientForm.find('.has-error').length) {
        return false;
    }

    var url =  clientForm.attr('action');
    var dataSend = clientForm.serialize();

    $.ajax({
        url: url,
        type: 'post',
        data: dataSend,
        success: function(data) {

            if(data.id) {
                $.ajax({ type: "POST",
                    url: '/ajax-board-order/index3',
                    data: { 
                        'id': data.id, 
                       
                    },
                    success: function(data){
                        $('#boards-sprint-list-ajax').html(data);
                    }
                });
                $('#board-sprint-modal').modal('hide');
            } else {
                $('#error-msg').html('Ошибка создания этапа');
            }
        },
        error: function (error) {
            $('#error-msg').html(error);
        }
    });

}).on('submit', function(e){
    e.preventDefault();
});

function updateBoardsOrderList(id) {
    $.ajax({ type: "POST",
        url: '/ajax-board-order/index3?id='.id,
        success: function(data){
            $('#boards-sprint-list-ajax').html(data);
        }
    });
}