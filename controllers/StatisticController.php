<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Client;
use app\models\Call;

/**
 * ApiController
 */
class StatisticController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 1) 
        {
            $this->enableCsrfValidation = true;
            return parent::beforeAction($action);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Main API Method
     * @return mixed
     */
    public function actionInterval()
    {
        $post = Yii::$app->request->post();

        if($post['date_time_from'] != null) $datefrom = $post['date_time_from'];
        else { $datefrom = date('d.m.Y'); $post['date_time_from'] = date('d.m.Y');}
        if($post['date_time_to'] != null) $dateto = $post['date_time_to'];
        else { $dateto = date('d.m.Y'); $post['date_time_to'] = date('d.m.Y');}

        $managers = Users::find()->where(['type' => 4])->all();

        $result = [];
        foreach ($managers as $manager) {
            $count = Call::find()->where(['between', 'date', \Yii::$app->formatter->asDate($datefrom, 'php:Y-m-d 00:00:00'), \Yii::$app->formatter->asDate($dateto, 'php:Y-m-d 23:59:59')])->andWhere(['users_id' => $manager->id])->count();
            $result [] = [
                'name' => $manager->fio,
                'count' => $count,
            ];
        }       

        return $this->render('interval', [
            'post' => $post,
            'result' => $result,
        ]);
    }

    public function actionManager()
    {
        $post = Yii::$app->request->post();
        /*echo "<pre>";
        print_r($post);
        echo "</pre>";
        die;*/

        if($post['date_time_from'] != null) $datefrom = $post['date_time_from'];
        else { $datefrom = date('d.m.Y'); $post['date_time_from'] = date('d.m.Y');}
        if($post['date_time_to'] != null) $dateto = $post['date_time_to'];
        else { $dateto = date('d.m.Y'); $post['date_time_to'] = date('d.m.Y');}

        $begin = strtotime($datefrom);
        $end = strtotime($dateto);
        $result = [];
        for ($i = $begin; $i <= $end; $i = $i + 86400) 
        { 
            $call = Call::find()->where(['between', 'date', \Yii::$app->formatter->asDate(date('Y-m-d', $i), 'php:Y-m-d 00:00:00'), \Yii::$app->formatter->asDate(date('Y-m-d', $i), 'php:Y-m-d 23:59:59')])->andWhere(['users_id' => $post['manager'] ])->count();
            $client = Client::find()->where(['between', 'date_creation', \Yii::$app->formatter->asDate(date('Y-m-d', $i), 'php:Y-m-d 00:00:00'), \Yii::$app->formatter->asDate(date('Y-m-d', $i), 'php:Y-m-d 23:59:59')])->andWhere(['creator' => $post['manager'] ])->count();
            $result [] = [
                'date' => date('d.m.Y', $i),
                'call' => $call,
                'client' => $client,
            ];
        }  

        return $this->render('manager', [
            'post' => $post,
            'result' => $result,
        ]);
    }
}
