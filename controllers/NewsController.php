<?php

namespace app\controllers;

use Yii;
use app\models\News; 
use app\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile; 

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models. 
     * @return mixed
     */
    public function actionIndex($archive = 0)
    {    
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $archive);
        $dataProvider->pagination = ['pageSize' => 10,];
        return $this->render('index', [
           // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'archive' => $archive
        ]);
    }


    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Просмотреть. Новости",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    //Новости ni birinchi qilib qo'yish
    public function actionUp($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        $min = News::find()->max('fixing'); 
        if($min == null) $min = 0;
        $model->fixing = $min + 1;
        $model->save();

        if($request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }
        else
        {
            return $this->redirect(['index']);
        }
    }
    //Fileni saqlash
    public function actionSendFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }

    /**
     * Creates a new News model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new News();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить. Новость ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate() ){
                if($model->fixing == true)
                {
                    $model->other_image = UploadedFile::getInstance($model, 'other_image');
                    $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                    $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                    $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                    if (!empty($model->other_image)) $model->image = $imageName;
                    if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                    $min = News::find()->max('fixing'); 
                    if($min == null) $min = 0;
                    $model->fixing = $min + 1;
                    $model->save();

                    if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                    if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);
                    return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Новости",
                    'size' => 'normal',

                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];     
                }else{
                    $model->other_image = UploadedFile::getInstance($model, 'other_image');
                    $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                    $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                    $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                    if (!empty($model->other_image)) $model->image = $imageName;
                    if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                    $model->save();

                    if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                    if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);
                    return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Новости",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];     
                }
                              

                    
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if($model->load($request->post()) && $model->validate() ){
                if($model->fixing == true)
                {
                $model->other_image = UploadedFile::getInstance($model, 'other_image');
                $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                if (!empty($model->other_image)) $model->image = $imageName;
                if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                $min = News::find()->max('fixing'); 
                if($min == null) $min = 0;
                $model->fixing = $min + 1;
                $model->save();

                if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);

                return $this->redirect(['view', 'id' => $model->id]);
                }else{
                $model->other_image = UploadedFile::getInstance($model, 'other_image');
                $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                if (!empty($model->other_image)) $model->image = $imageName;
                if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                $model->save();

                if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);

                return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing News model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) ){

               if($model->fixing == true)
                {
                    $model->other_image = UploadedFile::getInstance($model, 'other_image');
                    $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                    $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                    $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                    if (!empty($model->other_image)) $model->image = $imageName;
                    if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                    $min = News::find()->max('fixing'); 
                    if($min == null) $min = 0;
                    $model->fixing = $min + 1;
                    $model->save();

                    if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                    if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);
                    return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Новости",
                    'size' => 'normal',

                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];     
                }else{
                    $model->other_image = UploadedFile::getInstance($model, 'other_image');
                    $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                    $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                    $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                    if (!empty($model->other_image)) $model->image = $imageName;
                    if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                    $model->save();

                    if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                    if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);
                    return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Новости",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];     
                }
                              
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {

               if($model->fixing == true)
                {
                $model->other_image = UploadedFile::getInstance($model, 'other_image');
                $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                if (!empty($model->other_image)) $model->image = $imageName;
                if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                $min = News::find()->max('fixing'); 
                if($min == null) $min = 0;
                $model->fixing = $min + 1;
                $model->save();

                if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);

                return $this->redirect(['view', 'id' => $model->id]);
                }else{
                $model->other_image = UploadedFile::getInstance($model, 'other_image');
                $imageName = $model->other_image->baseName . '.' . $model->other_image->extension;

                $model->other_attached_file = UploadedFile::getInstance($model, 'other_attached_file');
                $fileName = $model->other_attached_file->baseName . '.' . $model->other_attached_file->extension;

                if (!empty($model->other_image)) $model->image = $imageName;
                if (!empty($model->other_attached_file)) $model->attached_file = $fileName;
                $model->save();

                if (!empty($model->other_image)) $model->other_image->saveAs('uploads/' . $imageName);
                if (!empty($model->other_attached_file)) $model->other_attached_file->saveAs('uploads/' . $fileName);

                return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing News model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = News::findOne($id);
        $imageName = $model->image;
        Yii::$app->db->createCommand()->delete('news', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);

        $imageName = $model->attached_file;
        Yii::$app->db->createCommand()->delete('news', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
