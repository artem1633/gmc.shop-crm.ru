<?php

namespace app\controllers;

use Yii;
use app\models\Documents;
use app\models\TemplateFields;
use app\models\Templates;
use app\models\User;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Client;
use app\models\Call;
use app\models\Settings;

/**
 * ApiController
 */
class ApiController extends Controller
{

    const API_KEY = 'WUUXLbHScg813mShY5BMfL0KDsv9B0RK';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Main API Method
     * @return mixed
     */
    public function actionCurator()
    {
        $clients = Client::find()->all();
        $setting = Settings::find()->where(['key' => 'curator_day_count'])->one();
        foreach ($clients as $client) 
        {
            if($client->curator != null)
            {
                $call = Call::find()->where(['client_id' => $client->id])->orderBy(['date' => SORT_DESC])->one();
                if($call != null)
                {
                    if($setting != null)
                    {
                        $now = strtotime(date('Y-m-d'));
                        $date = strtotime($call->date);
                        $dif = ($now - $date)/(60 * 60 * 24 );
                        if($dif > $setting->value)
                        {
                            $client->curator = null;
                            $client->save();
                        }
                    }
                }                
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
}
