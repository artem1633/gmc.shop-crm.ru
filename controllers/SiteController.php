<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Project; 
use app\models\Board;
use app\models\Sprints;
use app\models\Calling;
use app\models\Pays;
use app\models\Users;
use app\models\Clients;
use app\models\Tasks;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $wrapperClass;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [ 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) { 
            return $this->redirect('client/index');
        }else
        {
            return $this->redirect(['site/login']);
        }
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Рабочий стол
     * @return Response|string
     */
// через этот екшн заходим на Рабочий стол 
    public function actionDashboard()
    {
      if (Yii::$app->user->isGuest) {
        $this->redirect(['login']);
      }
      $tip = Yii::$app->user->identity->type;
      $type = Users::find()->where(['id' =>\Yii::$app->user->identity->id,'type'=>'2'])->one();
      $user_id = Yii::$app->user->identity->id;
      $user = Users::findOne($user_id);
      /*if($tip=="1") 
      {
        //$all_tasks = Tasks::find()->where(['date' => date('Y-m-d')])->all();
      }
      else */$all_tasks = Tasks::find()->where(['date' => date('Y-m-d'),'to_users_id' =>$user_id])->all();

      $pays = [
        'all' => '',
        'month' => '',
        'week' => '',
        'day' => '',
      ];;
      $calling = [
        'all' => '',
        'month' => '',
        'week' => '',
        'day' => '',
      ];;
      $clients = [
        'all' => '',
        'month' => '',
        'week' => '',
        'day' => '',
      ];

      return $this->render('dashboard', [  
          'pays' => $pays,
          'calling' => $calling,
          'clients' => $clients,
          'tasks' => $all_tasks,
      ]);
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
         Yii::$app->user->logout();

        $this->redirect(['login']);
    }

    public function actionLogout1()
    {
         Yii::$app->user->logout();

        $this->redirect(['signup']);
    }
    public function actionAvtorizatsiya()
    {
      if(isset(Yii::$app->user->identity->id))
      {
        return $this->render('error');
      }        
       else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }

    }
    public function actionBoardsSprint($project_id)
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $tip = Yii::$app->user->identity->type;

        if($tip == 5)
        {
            $tasks = Tasks::find()->where(['executor' => Yii::$app->user->identity->id , 'project' => $project_id])->all();
            $result = [];
            foreach ($tasks as $value) {
                $result [] = $value->sprint;
            }
            $result = array_unique($result);
            
            $boards = Sprints::find()->where(['id' => $result, 'is_delete' => 0])->all();
        }

        if($tip == 4)
        {
            $boards = Sprints::find()->where(['communications_project' => $project_id, 'is_delete' => 0])->all();
        }

        if($tip == 3)
        {
            $boards = Sprints::find()->where(['communications_project' => $project_id, 'is_delete' => 0])->all();
        }

        if($tip == 1 | $tip == 6) 
        {
            $user = Users::findOne(Yii::$app->user->identity->id);
            if($user->creator != null) $boards = Sprints::find()->where(['communications_project' => $project_id, 'company' => $user->creator, 'is_delete' => 0 ])->all();
            else $boards = Sprints::find()->where(['communications_project' => $project_id, 'company' => Yii::$app->user->identity->id, 'is_delete' => 0 ])->all();
        }

        return $this->render('boards-sprint', [
            'boards' => $boards,
            'project_id' => $project_id,
        ]);
    }
// через этот екшн можно войти на инструкцию
    public function actionInstruksion()
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        return $this->render('instruksion', []);        
    }

}
