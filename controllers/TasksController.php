<?php

namespace app\controllers;

use Yii;
use app\models\Tasks; 
use app\models\TasksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response; 
use yii\helpers\Html; 
use yii\web\UploadedFile;
use app\models\Notification;
/**
 * TasksController implements the CRUD actions for Tasks model.
 */ 
class TasksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** 
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'status' => 0,
        ]);
    }

    public function actionArchive()
    {    
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->searchArchive(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'status' => 1,
        ]);
    }

    public function actionChat()
    {    
        return $this->render('chat', [
        ]);
    }


    /**
     * Displays a single Tasks model.
     * @param integer $id 
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> $this->findModel($id)->title,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $model = new Tasks(); 
        $model->client_id = $client_id;
        if(Yii::$app->user->identity->type == 2) $model->who_users_id = Yii::$app->user->identity->id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            
            $model->other_file = UploadedFile::getInstance($model, 'other_file');

            $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
            if ($model->other_file && $model->validate()) {
                $model->other_file->saveAs('uploads/' . $fileName);
                $model->attachment =  $fileName;
                $model->save();
            }
            
            Notification::setNotification('tasks', $model->id, $model->to_users_id, $model->title);
            
            return [
                'forceReload'=>'#tasks-datatable-pjax',
                'size' => 'large',
                'title'=> "Задачи",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/tasks/add', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [ 
                'title'=> "Добавить. Задачи",
                'size' => 'large',
                'content'=>$this->renderAjax('/tasks/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }
    /**
     * Creates a new Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddrab($client_id)
    {
        $request = Yii::$app->request;
        $model = new Tasks(); 
        $model->client_id = $client_id;
        if(Yii::$app->user->identity->type == 2) $model->who_users_id = Yii::$app->user->identity->id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            
            $model->other_file = UploadedFile::getInstance($model, 'other_file');

            $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
            if ($model->other_file && $model->validate()) {
                $model->other_file->saveAs('uploads/' . $fileName);
                $model->attachment =  $fileName;
                $model->save();
            }
            
            Notification::setNotification('tasks', $model->id, $model->to_users_id, $model->title);
            
            return [
                'forceReload'=>'#tasks-datatable-pjax',
                'size' => 'large',
                'title'=> "Задачи",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/tasks/add_rab', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [ 
                'title'=> "Добавить. Задачи",
                'size' => 'large',
                'content'=>$this->renderAjax('/tasks/add_rab', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }
    /**
     * Updates an existing Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateTasks($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return ['forceClose'=>true,'forceReload'=>'#tasks-datatable-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/tasks/update', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTasks($id)
    {
        $request = Yii::$app->request;
        $model = Tasks::findOne($id);
        $imageName = $model->attachment;
        Yii::$app->db->createCommand()->delete('tasks', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);

       // $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#tasks-datatable-pjax'];
        }
    }
    /**
     * Creates a new Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Tasks();  
        if(Yii::$app->user->identity->type != 1) $model->who_users_id = Yii::$app->user->identity->id; 

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->attachment = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                //Notification::setNotification('tasks', $model->id, $model->who_users_id, 'Создано новая задача');
                Notification::setNotification('tasks', $model->id, $model->to_users_id, $model->title);
                
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Задачи",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                    'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate()){

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->attachment = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Задачи",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
