<?php

namespace app\controllers;

use Yii;
use app\models\Verification;
use app\models\VerificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter; 
use \yii\web\Response;
use yii\helpers\Html; 
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use app\models\Template;
/**
 * VerificationController implements the CRUD actions for Verification model.
 */
class VerificationController extends Controller
{
    /**
     * @inheritdoc 
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 7 || $type == 9 || $type == 10) 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
            /*if($action->id != 'view' && $action->id != 'list' && $action->id != 'change') return $this->redirect(['/site/errors', 'type' => 0 ]);*/
        }

        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }
    /**
     * Lists all Verification models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new VerificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Verification model.
     * @param integer $id
     * @return mixed
     */
    public function actionSendFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Поверки ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Verification model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Verification();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate() ){
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate() ) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }
    /**
     * Creates a new Verification model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $model = new Verification(); 
        $model->client_id = $client_id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load($request->post()) && $model->validate() ) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
            return [
                'forceReload'=>'#verification-datatable-pjax',
                'size' => 'normal',
                'title'=> "Поверки",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/verification/add', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [ 
                'title'=> "Добавить. Поверки",
                'size' => 'large',
                'content'=>$this->renderAjax('/verification/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }
    /**
     * Updates an existing Verification model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateVerification($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load($request->post()) && $model->validate() ) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
            return ['forceClose'=>true,'forceReload'=>'#verification-datatable-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/verification/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Verification model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteVerification($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#verification-datatable-pjax'];
        }
    }

    public function actionCreate1($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        if ($model->load($request->post()) && $model->validate() ) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
            return [
                'forceReload'=>'#parameter-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn save btn-sm','type'=>"submit"])
            ];
        }
       
    }
    /**
     * Updates an existing Verification model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate() ){
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Поверки ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate() ) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;

                if (!empty($model->other_file)) {
                    $model->documents = $imageName;
                    $model->other_file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Verification model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        
        $model = Verification::findOne($id);
        $imageName = $model->documents;
        Yii::$app->db->createCommand()->delete('tasks', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);
        $this->findModel($id)->delete();
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Verification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Verification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Verification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
