<?php

namespace app\controllers;

use Yii;
use app\models\Contacts;
use app\models\ContactsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ContactsController implements the CRUD actions for Contacts model.
 */
class ContactsController extends Controller
{
    /**
     * @inheritdoc
     */ 
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Contacts model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $model = new Contacts(); 
        $model->client_id = $client_id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>'#personal-pjax',
                'size' => 'normal',
                'title'=> "Контакты",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/contacts/add', 'client_id' => $client_id],['class'=>'btn btn-primary','role'=>'modal-remote'])        
            ];         
        }else{           
            return [ 
                'title'=> "Добавить",
                'size' => 'normal',
                'content'=>$this->renderAjax('/contacts/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])        
            ];         
        }     
    }

    /**
     * Updates an existing Contacts model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateContacts($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return ['forceClose'=>true,'forceReload'=>'#personal-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                //'size' => 'large',
                'size' => 'normal',
                'content'=>$this->renderAjax('/contacts/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    /**
     * Delete an existing Contacts model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteContacts($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
            $contacts = \app\models\Call::find()->where(['contact_id'=> $id])->one();             
            if( $contacts != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger" style="color:red; font-size:20px;"><b>Вы не можете удалить эту контакты. Потому что он связано с звонки!!!</b></span></center>',                              
                ];
            }
            else 
            {
                $this->findModel($id)->delete();
                return ['forceClose'=>true,'forceReload'=>'#personal-pjax'];              
            }
        }
    }

    /**
     * Finds the Contacts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contacts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
