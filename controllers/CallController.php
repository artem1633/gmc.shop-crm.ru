<?php

namespace app\controllers;

use Yii;
use app\models\Call;
use app\models\CallSearch; 
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response; 
use yii\helpers\Html;
use app\models\Contacts;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\Users;
use app\models\Template;

class CallController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    //Печатаем шаблон
    public function actionPrint($key)
    {
        $template = Template::find()->where(['key' => $key])->one();
        $html = $template->text;
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->SetWatermarkText('Гмц');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;

        $mpdf->Output("phpflow.pdf", 'F');
        $mpdf->Output();
    }

    //Импортируем файл
    public function actionImport() 
    {
        $model = new UploadForm();
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = $model->file->baseName . '.' . $model->file->extension;

                if ($model->load($request->post())) {
                    $model->file = UploadedFile::getInstance($model, 'file');
                    $model->file->saveAs('uploads/' . $fileName);
                }
                Call::getValuesFromExcel($fileName);
            }
            catch (Exception $exception) {
                return $exception;
            }
        }
        else
        { 
            return [
                'title'=> "Импорт",
                'size' => 'small',
                'content'=>$this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Импорт',['class'=>'btn btn-primary','type'=>"submit"])
            ]; 

        }

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }
    
    /**
     * Creates a new Call model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionAdd($client_id, $pjax_container = 'calls-datatable-pjax')
    {
        $request = Yii::$app->request;
        $model = new Call(); 
        $model->client_id = $client_id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>"#{$pjax_container}",
                'size' => 'normal',
                'title'=> "Звонки",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/call/add', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];          
        }else{           
            return [ 
                'title'=> "Добавить",
                'size' => 'large',
                'content'=>$this->renderAjax('/call/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }

    /**
     * Updates an existing Call model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCall($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);   
        if(Yii::$app->user->identity->type != 1) return ['forceClose'=>true,'forceReload'=>'#calls-datatable-pjax'];

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return ['forceClose'=>true,'forceReload'=>'#calls-datatable-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/call/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Call model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteCall($id)
    {
        $request = Yii::$app->request;
        if(Yii::$app->user->identity->type == 1) $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#calls-datatable-pjax'];
        }
    }

    public function actionCreate1($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#parameter-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn save btn-sm','type'=>"submit"])
            ];
        }
       
    } 
    /**
     * Delete an existing Call model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if(Yii::$app->user->identity->type == 1) $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }


     /**
     * Delete multiple existing Call model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    protected function findModel($id)
    {
        if (($model = Call::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
