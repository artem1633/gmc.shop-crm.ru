<?php

namespace app\controllers;

use Yii;
use app\models\Stock;
use app\models\StockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response; 
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\Category;

/**
 * StockController implements the CRUD actions for Stock model.
 */
class StockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 5 || $type == 9 || $type == 10) 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
            /*if($action->id != 'view' && $action->id != 'list' && $action->id != 'change') return $this->redirect(['/site/errors', 'type' => 0 ]);*/
        }

        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }
    /**
     * Lists all Stock models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Stock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Склад ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{ 
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    public function actionListProducts($id)
    {  
        $datas = Category::find()->where(['id' => $id])->one();
        $product = \app\models\Products::find()->where(['category_id' => $datas->id])->all();
        $text = '';
        foreach ($product as $value) { 
            echo "<option value = '".$value->id."'>".$value->name."</option>" ;            
        }
        //return $text;
    }
    public function actionListagent1($id)
    {
      $tekst = "";
      $agents = \app\models\Products::find()->where(['category_id' => $id])->all();
        foreach ($agents as $value) {
            //$tekst .= $value->name."; ";              
          echo "<option value = '".$value->id."'>".$value->name."</option>" ;    
        } 
      //return $tekst;         
    }
    /**
     * Creates a new Stock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Stock();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }
    // Загрузить файл на сервер
    public function upload()
    {

        if ($this->validate()) {
            $this->product_image->saveAs('uploads/' . $this->product_image->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
    //Скачать файл из сервера
    public function actionSendFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) ){

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->product_image = $imageName;
                $model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);
                return [
                    'forceReload'=>'#crud-datatable-pjax','forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->product_image = $imageName;
                $model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Stock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        Yii::$app->db->createCommand()->delete('stock', ['id' => $id])->execute();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Stock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
