<?php

namespace app\controllers;

use app\models\TenderFile;
use Yii;
use app\models\Products;
use app\models\Goods; 
use app\models\ProductsSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /** 
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ], 
        ]; 
    }
    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex($archive = 0) 
    {    
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $archive);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'archive' => $archive
        ]);
    }


    /**
     * Displays a single Products model.
     * @param integer $id
     * @param string $answer
     * @return mixed
     */
    public function actionView($id, $answer = '')
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($answer == Response::FORMAT_JSON){
                return $this->findModel($id);
            } else {
                return [
                    'title'=> "Продукты ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                ];
            }
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    
    //В продуктах сделать кнопку подтвердить в таблице. Она доступна только админу. После подтверждения не кто редактировать не может кроме админа
    public function actionClose($id)
    {
        $model= Products::findOne($id);
        if($model->closeopen == 0||$model->closeopen ==null)
        {
            $model->closeopen = 1;
            $model->save();
        }
        elseif($model->closeopen == 1)
        {
            $model->closeopen = 0;
            $model->save(); 
        }

        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            //'title'=> "Пользователь #".$id,
            'forceClose'=>true,
            'forceReload'=>'#crud-datatable-pjax'
        ];    
    }
    /**
     * Creates a new Products model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPrice($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        if($model->datecreate != null ) $model->datecreate = date('Y-m-d H:i:s', strtotime($model->datecreate));
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить Цена",
                    'size' => 'small',
                    'content'=>$this->renderAjax('price', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate())
            {   
                $old = Products::findOne($id);
                $model->copyprice = $old->cost;
                $model->datecreate = date('Y-m-d H:i:s');
                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить Цена",
                    'size' => 'small',
                    'content'=>$this->renderAjax('price', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) ) {
                $old = Products::findOne($id);
                $model->copyprice = $old->cost;  
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('price', [
                    'model' => $model,
                ]);
            }
        }
    }

     /**
     * Creates a new Products model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Products();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');
                $imageName = $model->file->baseName . '.' . $model->file->extension;

                if (!empty($model->file)) {
                    $model->product_image = $imageName;
                    $model->file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $old = $this->findModel($id);
        if($model->datecreate != null ) $model->datecreate = date('Y-m-d H:i:s', strtotime($model->datecreate));
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate())
            {                   
                if($old->cost != $model->cost) 
                {
                    $model->copyprice = $old->cost;
                    $model->datecreate = date('Y-m-d H:i:s');
                }
                $model->file = UploadedFile::getInstance($model, 'file');
                $imageName = $model->file->baseName . '.' . $model->file->extension;

                if (!empty($model->file)) {
                    $model->product_image = $imageName;
                    $model->file->saveAs('uploads/' . $imageName);
                }

                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Продукты ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                $old = Products::findOne($id);
                if($old->cost != $model->cost) 
                {
                    $model->copyprice = $old->cost;
                    $model->datecreate = date('Y-m-d H:i:s');
                }
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Products model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */ 
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
            $calls = \app\models\Goods::find()->where(['product_id'=> $id])->one();             
            if( $calls != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger" style="color:red; font-size:20px;"><b>Вы не можете удалить этот элемент. Потому что он связан с заказами!!!</b></span></center>',                              
                ];
            }
            else 
            {
                unlink(getcwd().'/uploads/'.Products::findOne($id)->product_image);
                $this->findModel($id)->delete();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];              
            }
        }
        else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }
     
    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
