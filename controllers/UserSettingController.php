<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use app\models\UserSetting;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class UserSettingController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [ 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param string $name
     * @param string $value
     * @return mixed
     */
    public function actionSetSetting($name, $value)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        /** @var UserSetting $model */
        $model = $user->setting;

        $result = false;

        if($model->hasAttribute($name)){
            $model->$name = $value;
            $result = $model->save(false);
        }

        return ['result' => $result];
    }
}