<?php

namespace app\controllers;

use GuzzleHttp\Psr7\Request;
use Yii;
use app\models\Goods;
use app\models\GoodsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response; 
use yii\helpers\Html;
use app\models\Products;
use app\models\Orders;
/**
 * GoodsController implements the CRUD actions for Goods model.
 */
class GoodsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Goods models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Goods model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($order_id, $pjaxContainer = '#orders-pjax')
    {
        $request = Yii::$app->request;
        $model = new Goods();  
        $model->order_id = $order_id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->validate()){
            $model->name = $model->product->name;
            $model->cost = $model->product->completeness;
            $model->nds_protsent = $model->product->nds_protsent;

            $orders=Orders::findOne($model->order_id);
                $orders->total_sum = $orders->total_sum+$model->count*$model->cost;
                if($model->nds_protsent==1)
                {
                    $orders->total_nds_sum += 0;
                }
                if($model->nds_protsent==2)
                {
                   $orders->total_nds_sum += ($model->count*$model->cost)*10/100;
                }
                if($model->nds_protsent==3)
                {
                    $orders->total_nds_sum += ($model->count*$model->cost)*18/100;
                }

            $product=Products::findOne($model->product_id);
            $product->completeness=$product->completeness-$model->count;
            $orders->save();
            $product->save();
            $model->save();
            return [
                'forceReload'=>$pjaxContainer,
                'size' => 'normal',
                'title'=> "Продукты",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/goods/add', 'order_id' => $order_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [
                'title'=> "Добавить продукты",
                'size' => 'normal',
                'content'=>$this->renderAjax('/goods/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }
     /**
     * Updates an existing Goods model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateGoods($id, $pjaxContainer = '#orders-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return ['forceClose'=>true,'forceReload'=>$pjaxContainer];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/goods/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Goods model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteGoods($id, $pjaxContainer = '#orders-pjax')
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
            $orders=Orders::findOne($model->order_id);
                $orders->total_sum = $orders->total_sum-$model->count*$model->cost;
                if($model->nds_protsent==1)
                {
                    $orders->total_nds_sum =$orders->total_nds_sum - 0;
                }
                if($model->nds_protsent==2)
                {
                   $orders->total_nds_sum = $orders->total_nds_sum -($model->count*$model->cost)*10/100;
                }
                if($model->nds_protsent==3)
                {
                    $orders->total_nds_sum = $orders->total_nds_sum -($model->count*$model->cost)*18/100;
                }
            $orders->save();
        
        $product=Products::findOne($model->product_id);
        $product->count=$product->count+$model->count;
        $product->save();
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>$pjaxContainer];
        }
    }

    /**
     * Delete an existing Goods model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }
    /**
     * Finds the Goods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Goods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Goods::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
