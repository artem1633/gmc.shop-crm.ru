<?php

namespace app\controllers;

use Yii;
use app\models\Documentation;
use app\models\DocumentationSearch;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
  
/**
 * DocumentationController implements the CRUD actions for Documentation model.
 */
class DocumentationController extends Controller
{
    /**
     * @inheritdoc
     */ 
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Documentation models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new DocumentationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Documentation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Документы ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    public function upload()
    {

        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $this->file->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
    /**
     * Creates a new Documentation model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $model = new Documentation(); 
        $model->client_id = $client_id;  
            
        Yii::$app->response->format = Response::FORMAT_JSON;
       
        if($model->load($request->post()) && $model->save()){

            $model->other_file = UploadedFile::getInstance($model, 'other_file');

            $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
            if ($model->other_file && $model->validate()) {
                $model->other_file->saveAs('uploads/' . $fileName);
                $model->file =  $fileName;
                $model->save();
            }

            return [
                'forceReload'=>'#documentation-datatable-pjax',
                'size' => 'normal',
                'title'=> "Документы",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/documentation/add', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [ 
                'title'=> "Добавить. Документ",
                'size' => 'large',
                'content'=>$this->renderAjax('/documentation/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }

    //Загрузить файл на сервер
    public function actionSendDocumentFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    
    /**
     * Updates an existing Documentation model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateDocumentation($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            $model->other_file = UploadedFile::getInstance($model, 'other_file');

            $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
            if ($model->other_file && $model->validate()) {
                $model->other_file->saveAs('uploads/' . $fileName);
                $model->file =  $fileName;
                $model->save();
            }
            return ['forceClose'=>true,'forceReload'=>'#documentation-datatable-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/documentation/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Documentation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteDocumentation($id)
    {
        $request = Yii::$app->request;
        $model = Documentation::findOne($id);
        $imageName = $model->file;
        Yii::$app->db->createCommand()->delete('documentation', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#documentation-datatable-pjax'];
        }
    }
    /**
     * Creates a new Documentation model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Documentation();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){

                    $model->other_file = UploadedFile::getInstance($model, 'other_file');

                    $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
                    if ($model->other_file && $model->validate()) {
                        $model->other_file->saveAs('uploads/' . $fileName);
                        $model->file =  $fileName;
                        $model->save();
                    }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Создания документы успешно завершено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Documentation model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                $model->other_file = UploadedFile::getInstance($model, 'other_file');

                    $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
                    if ($model->other_file && $model->validate()) {
                        $model->other_file->saveAs('uploads/' . $fileName);
                        $model->file =  $fileName;
                        $model->save();
                    }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Документы ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');

                    $fileName = $model->other_file->baseName . '.' . $model->other_file->extension;
                    if ($model->other_file && $model->validate()) {
                        $model->other_file->saveAs('uploads/' . $fileName);
                        $model->file =  $fileName;
                        $model->save();
                    }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Documentation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         $request = Yii::$app->request;
        $model = Documentation::findOne($id);
        $imageName = $model->file;
        Yii::$app->db->createCommand()->delete('documentation', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Documentation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documentation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documentation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
