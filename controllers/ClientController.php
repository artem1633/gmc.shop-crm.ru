<?php

namespace app\controllers;

use app\components\DadataInnSearch;
use Yii;
use app\models\Client;
use app\models\ClientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Contacts;
use app\models\CallSearch;
use app\models\TenderSearch;
use app\models\VerificationSearch;
use app\models\Call; 
use app\models\Tender;
use app\models\Verification;
use app\models\ContactsSearch;
use app\models\TasksSearch;
use app\models\Tasks;
use app\models\Orders;
use app\models\DocumentationSearch;
use app\models\Documentation;
use app\models\OrdersSearch;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\City;
use app\models\Users;
/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends Controller
{
    /** 
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 5 || $type == 7 || $type == 9) 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
            /*if($action->id != 'view' && $action->id != 'list' && $action->id != 'change') return $this->redirect(['/site/errors', 'type' => 0 ]);*/
        }

        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }
    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {   
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $s
     * @return mixed
     */
    public function actionSearchByInn($s)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = DadataInnSearch::search($s);

        return $data;
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    { 
            $callssearchModel = new CallSearch();
            $callsdataProvider = $callssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $tendersearchModel = new TenderSearch();
            $tenderdataProvider = $tendersearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $verificationsearchModel = new VerificationSearch();
            $verificationdataProvider = $verificationsearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $contactssearchModel = new ContactsSearch();
            $contactsdataProvider = $contactssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $taskssearchModel = new TasksSearch();
            $tasksdataProvider = $taskssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $orderssearchModel = new OrdersSearch();
            $ordersdataProvider = $orderssearchModel->searchByClient(Yii::$app->request->queryParams,$id);

            $documentationsearchModel = new DocumentationSearch();
            $documentationdataProvider = $documentationsearchModel->searchByClient(Yii::$app->request->queryParams,$id);
            
        Yii::$app->session['client_id'] = $id;
        return $this->render('view', [
            'model' => $this->findModel($id),
            'callssearchModel' => $callssearchModel,
            'callsdataProvider' => $callsdataProvider,

            'tendersearchModel' => $tendersearchModel,
            'tenderdataProvider' => $tenderdataProvider,

            'verificationsearchModel' => $verificationsearchModel,
            'verificationdataProvider' => $verificationdataProvider,

            'contactssearchModel' => $contactssearchModel,
            'contactsdataProvider' => $contactsdataProvider,

            'taskssearchModel' => $taskssearchModel,
            'tasksdataProvider' => $tasksdataProvider,

            'orderssearchModel' => $orderssearchModel,
            'ordersdataProvider' => $ordersdataProvider,

            'documentationsearchModel' => $documentationsearchModel,
            'documentationdataProvider' => $documentationdataProvider,
        ]);
        
    }

    //Импортировать компании из excell файла
    public function actionImportCompany() 
    {
        $model = new UploadForm();
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = $model->file->baseName . '.' . $model->file->extension;

                if ($model->load($request->post())) {
                    $model->file = UploadedFile::getInstance($model, 'file');
                    $model->file->saveAs('uploads/' . $fileName);
                }
                Client::getValuesFromExcel($fileName);
            }
            catch (Exception $exception) {
                return $exception;
            }
        }
        else
        { 
            return [
                'title'=> "Импорт",
                'size' => 'small',
                'content'=>$this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Импорт',['class'=>'btn btn save','type'=>"submit"])
            ]; 

        }

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    //Импортировать звонки из excell файла
    public function actionImportCalls() 
    {
        $model = new UploadForm();
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {
            try {
                $model->file = UploadedFile::getInstance($model, 'file');
                $fileName = $model->file->baseName . '.' . $model->file->extension;

                if ($model->load($request->post())) {
                    $model->file = UploadedFile::getInstance($model, 'file');
                    $model->file->saveAs('uploads/' . $fileName);
                }
                Client::getCallsFromExcel($fileName);
            }
            catch (Exception $exception) {
                return $exception;
            }
        }
        else
        { 
            return [
                'title'=> "Импорт",
                'size' => 'small',
                'content'=>$this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Импорт',['class'=>'btn btn save','type'=>"submit"])
            ]; 

        }

        return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
    }

    /**
     * Creates a new Client model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Client();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
               $post = Yii::$app->request->post();
                $contact = $post['Client']['contacts'];
               
                foreach ($contact as $value) {
                    $relative = new Contacts();
                    $relative->client_id = $model->id; 
                    $relative->fio = $value['fio'];
                    $relative->position = $value['position'];
                    $relative->phone = $value['phone'];
                    $relative->email = $value['email'];
                    $relative->save();
                    $error = $relative->errors;
                   

                }
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

     /**
     * Updates an existing Contact model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEditContacts($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        

        if ($post != null) {

            $relation = \app\models\Contacts::find()->where(['client_id'=> $model->id])->all();

            foreach ($relation as $value) {
                if (($rel = \app\models\Contacts::findOne($value->id)) !== null) {
                   $rel->delete();
                }
            }

            $rodstvenniki = $post['Client']['contacts'];              
            foreach ($rodstvenniki as $value) {
                $contacts = new Contacts();
                $contacts->client_id = $model->id; 
                $contacts->fio = $value['fio'];
                $contacts->position = $value['position'];
                $contacts->phone = $value['phone'];
                $contacts->email = $value['email'];                                       
                $contacts->save();
                $error = $contacts->errors;

            }
            return [
                'forceReload'=>'#personal-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Изменить ",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_contacts', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn save btn-sm','type'=>"submit"])
            ];
        }
    }
    /**
     * Updates an existing Client model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        $model->contacts = $model->contacts;
        if($model->poslednego_zvonka != null ) $model->poslednego_zvonka = \Yii::$app->formatter->asDate($model->poslednego_zvonka, 'php:d.m.Y');
        if($model->sleduyushchego_zvonka != null ) $model->sleduyushchego_zvonka = \Yii::$app->formatter->asDate($model->sleduyushchego_zvonka, 'php:d.m.Y');
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
              
                return [
                    'forceReload'=>'#personal-pjax',
                    'forceClose'=>true,
                ];
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    //Печатать заказ клиента
    public function actionPrint($client_id, $order_id, $document)
    {
        $model = $this->findModel($client_id);
        $order = Orders::findOne($order_id);
        $mpdf = new \Mpdf\Mpdf();
        if($document == 'invoice') $mpdf->WriteHTML($model->getInvoiceHtmlText($model,$order));
        
        //call watermark content and image
        $mpdf->SetWatermarkText('');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;

        //save the file put which location you need folder/filname
        $mpdf->Output("phpflow.pdf", 'F');
        //out put in browser below output function
        $mpdf->Output();
    }

     //Печатать заказ клиента (черновек)
    public function actionPrint2($id, $document)
    {
        $model = $this->findModel($id);
        $mpdf = new \Mpdf\Mpdf();
        if($document == 'invoice') $mpdf->WriteHTML($model->getInvoiceHtmlText2($model));
        
        //call watermark content and image
        $mpdf->SetWatermarkText('');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;

        //save the file put which location you need folder/filname
        $mpdf->Output("phpflow.pdf", 'F');
        //out put in browser below output function
        $mpdf->Output(); 
    }
    //Скачать документы
    public function actionSendDocumentFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    //загрузить документы
    public function actionSendFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    /**
     * Delete an existing Client model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        $this->findModel($id)->delete();
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }
    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
