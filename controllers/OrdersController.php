<?php

namespace app\controllers;

use app\models\Products;
use app\models\TechTask;
use app\models\Tender;
use kartik\grid\EditableColumnAction;
use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Goods;
/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /** 
     * @inheritdoc
     */
    public function behaviors() 
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'edit' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Goods::className(),
                'showModelErrors' => true,
                'outputValue' => function($model, $attribute, $key, $index) {
//                    if($attribute == 'status'){
//                        return Candidate::statusLabels()[$model->$attribute];
//                    }

                    if(in_array($attribute, ['cost', 'price_one_client', 'price_client'])){
                        return Yii::$app->formatter->asCurrency($model->$attribute, 'rub');
                    }

                    return $model->$attribute;
                },
                'errorOptions' => ['header' => ''],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 5 || $type == 9 || $type == 10) 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Заказы ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } 
    }
     /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewLogistics($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Заказы ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view_logistics', [
                        'model' => $this->findModel($id),
                    ]),
                ];    
        }else{
            return $this->render('view_logistics', [
                'model' => $this->findModel($id),
            ]);
        } 
    }
     /**
     * Creates a new Orders model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $model = new Orders();  
        $model->client_id = $client_id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->validate()){
               $model->save();
            return [
                'forceReload'=>'#orders-datatable-pjax',
                'size' => 'large',
                'title'=> "Заказы",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/orders/add', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [
                'title'=> "Добавить",
                'size' => 'large',
                'content'=>$this->renderAjax('/orders/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }
    /**
     * Updates an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateOrders($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return ['forceClose'=>true,'forceReload'=>'#orders-datatable-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/orders/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteOrders($id)
    {
        $goods = Goods::find()->where(['order_id' => $id])->all();
        foreach ($goods as $good) {
           $good->delete();
        }
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#orders-datatable-pjax'];
        }
    }
    /**
     * Creates a new Orders model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Orders();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save() && $model->validate()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionCreateByTender($tender_id)
    {
        $model = Tender::findOne($tender_id);

        if($model == null){
            throw new NotFoundHttpException();
        }
        /** @var TechTask[] $techTasks */
        $techTasks = TechTask::find()->where(['tender_id' => $tender_id])->all();

        $order = new Orders([
            'client_id' => $model->client_id,
            'status' => '1',
            'base_id' => '1',
            'responsible' => Yii::$app->user->getId(),
        ]);
        $order->save(false);

        foreach ($techTasks as $task)
        {
            $good = new Goods([
                'order_id' => $order->id,
                'product_id' => $task->product_id,
                'count' => $task->count,
                'sum' => $task->sum,
                'cost' => $task->price
            ]);
            $good->save(false);
        }

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Заказы ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        $goods = Goods::find()->where(['order_id' => $id])->all();
        foreach ($goods as $good) {
           $good->delete();
        }

        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
