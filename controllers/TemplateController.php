<?php

namespace app\controllers;

use Yii;
use app\models\Template;
use app\models\TemplateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class TemplateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Template models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new TemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAllTemplates()
    {   
        $request = Yii::$app->request;
        $templates = Template::find()->all();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Шаблоны",
                'size' => 'normal',
                'content'=>$this->renderAjax('view-all', [ 
                    'templates' => $templates,
                ]),
            ];    
        }else{
            return $this->render('view-all', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    //Печатать пдф документы
    public function actionPrintTemplate($id)
    {
        $template = $this->findModel($id);
        $html = $template->text;
        $html = str_replace ("{logo}", Html::img("images/logo-cu.jpg", ["style" => "width: 80px;"]), $html);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->SetWatermarkText('Гмц');
        $mpdf->showWatermarkText = true;
        $mpdf->watermarkTextAlpha = 0.1;

        $mpdf->Output("phpflow.pdf", 'F');
        $mpdf->Output();
    }
    //Shablonlarni Pdf ga chop etish
    public function actionPrint($key)
    {
        $request = Yii::$app->request;
        $model = Template::find()->where(['key' => $key])->one();
        $model->data_validate = date('Y-m-d');
        $model->date_calibr = date('Y-m-d');
        $model->data_poverki = date('Y-m-d');

        if ($model->load($request->post()) ) {

            $post = $request->post();
            $html = $model->text;
            $html = str_replace ("{logo}", Html::img("images/logo-cu.jpg", ["style" => "width: 80px;"]), $html);
            $html = str_replace ("{number}", $post['Template']['number'], $html);
            $html = str_replace ("{reg_number}", $post['Template']['reg_number'], $html);
            $html = str_replace ("{koleso_dorojnoe}", $post['Template']['koleso_dorojnoe'], $html);
            $html = str_replace ("{plant_number}", $post['Template']['plant_number'], $html);
            $html = str_replace ("{dalnomer}", $post['Template']['dalnomer'], $html);
            $html = str_replace ("{data_validate}", Template::getDateDescription($post['Template']['data_validate']), $html);
            $html = str_replace ("{date_calibr}", Template::getDateDescription($post['Template']['date_calibr']), $html);
            $html = str_replace ("{data_poverki}", Template::getDateDescription($post['Template']['data_poverki']), $html);
            $html = str_replace ("{basiz}", $post['Template']['basiz'], $html);
            $html = str_replace ("{length}", $post['Template']['length'], $html);
            $html = str_replace ("{width}", $post['Template']['width'], $html);
            $html = str_replace ("{predel}", $post['Template']['predel'], $html);
            $html = str_replace ("{lineyka}", $post['Template']['lineyka'], $html);
            $html = str_replace ("{nivelir}", $post['Template']['nivelir'], $html);
            $html = str_replace ("{complekt}", $post['Template']['complekt'], $html);
            $html = str_replace ("{ugol}", $post['Template']['ugol'], $html);
            $html = str_replace ("{naklon}", $post['Template']['naklon'], $html);
            $html = str_replace ("{shtrix}", $post['Template']['shtrix'], $html);
            $html = str_replace ("{stansiya}", $post['Template']['stansiya'], $html);
            $html = str_replace ("{km_xod}", $post['Template']['km_xod'], $html);
            $html = str_replace ("{pribor_verticalnogo_proyekta}", $post['Template']['pribor_verticalnogo_proyekta'], $html);
            $html = str_replace ("{luch}", $post['Template']['luch'], $html);
            $html = str_replace ("{reyka_dorojnaya}", $post['Template']['reyka_dorojnaya'], $html);
            $html = str_replace ("{uklon}", $post['Template']['uklon'], $html);
            $html = str_replace ("{zalojenie}", $post['Template']['zalojenie'], $html);
            $html = str_replace ("{reyka}", $post['Template']['reyka'], $html);
            $html = str_replace ("{ruletka}", $post['Template']['ruletka'], $html);
            $html = str_replace ("{deleniya}", $post['Template']['deleniya'], $html);
            $html = str_replace ("{taxeometr}", $post['Template']['taxeometr'], $html);
            $html = str_replace ("{gorizontal}", $post['Template']['gorizontal'], $html);
            $html = str_replace ("{vertical}", $post['Template']['vertical'], $html);
            $html = str_replace ("{teodolit}", $post['Template']['teodolit'], $html);
            $html = str_replace ("{uroven}", $post['Template']['uroven'], $html);
            $html = str_replace ("{pogreshnost}", $post['Template']['pogreshnost'], $html);
            $html = str_replace ("{shtangensirkul}", $post['Template']['shtangensirkul'], $html);
            
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->SetWatermarkText('Гмц');
            $mpdf->showWatermarkText = true;
            $mpdf->watermarkTextAlpha = 0.1;

            $mpdf->Output("phpflow.pdf", 'F');
            $mpdf->Output();

        } else {
            return $this->render('/template/forms/' . $key, [
                'model' => $model,
            ]);
        }
    }


    /**
     * Displays a single Template model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Template #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Updates an existing Template model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> $model->name,
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEmptyTemplate($key)
    {
        $request = Yii::$app->request;
        $model = Template::find()->where(['key' => $key])->one();       

        if ($model->load($request->post())) {

            $html = $model->text;
            $html = str_replace ("{logo}", Html::img("images/logo-cu.jpg", ["style" => "width: 80px;"]), $html);
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->SetWatermarkText('Гмц');
            $mpdf->showWatermarkText = true;
            $mpdf->watermarkTextAlpha = 0.1;

            $mpdf->Output("phpflow.pdf", 'F');
            $mpdf->Output();
            
        } else {
            return $this->render('_empty_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Template model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Template the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Template::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
