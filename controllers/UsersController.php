<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response; 
use yii\helpers\Html;
use app\models\Client;
use yii\web\ForbiddenHttpException;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 1) 
        {
            $this->enableCsrfValidation = true;
            return parent::beforeAction($action);
        }
        else
        {
            if($action->id == 'change')
            {
                $this->enableCsrfValidation = true;
                return parent::beforeAction($action);
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Пользователь ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    //Foydalanuvchiga yangi parol login berish
    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [ 'forceClose' => true ];    
        }else{
             return [
                'title'=> "Изменить. Профиль",
                'size' => 'normal',
                //'forceReload'=>'#crud-datatable-pjax',
                'content'=>$this->renderAjax('change', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пользователь ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON; 
            $calls = \app\models\Call::find()->where(['users_id'=> $id])->one();
            $client = \app\models\Client::find()->where(['curator'=> $id])->one();
            $tender = \app\models\Tender::find()->where(['users_id'=> $id])->one(); 
            $documentation = \app\models\Documentation::find()->where(['users_id'=> $id])->one(); 
            $tasks1 = \app\models\Tasks::find()->where(['who_users_id'=> $id])->one(); 
            $tasks2 = \app\models\Tasks::find()->where(['to_users_id'=> $id])->one();              
            if( $calls != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger deluser"><b>Вы не можете удалить эту пользователи. Потому что он связано с звонки!!!</b></span></center>',                              
                ];
            }
            else if( $client != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger deluser"><b>Вы не можете удалить эту пользователи. Потому что он связано с клиенты!!!</b></span></center>',                              
                ];
            }
            else if( $tender != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger deluser"><b>Вы не можете удалить эту пользователи. Потому что он связано с тендера!!!</b></span></center>',                              
                ];
            }
            else if( $documentation != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger deluser"><b>Вы не можете удалить эту пользователи. Потому что он связано с документы!!!</b></span></center>',                              
                ];
            }
            else if( $tasks1 != null||$tasks2 != null) 
            {          
                return [
                    'title'=> "".'<b>Error</b>',
                    'content'=>'<center><span class="text-danger deluser"><b>Вы не можете удалить эту пользователи. Потому что он связано с задачи!!!</b></span></center>',                              
                ];
            }
            else 
            {
                $this->findModel($id)->delete();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];              
            }
        }
        else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }
    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
