<?php

namespace app\controllers;

use app\models\TenderFile;
use app\models\TenderFileSearch;
use Yii;
use app\models\Tender;
use app\models\TenderSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * TenderController implements the CRUD actions for Tender model.
 */
class TenderController extends Controller
{
    /** 
     * @inheritdoc 
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 5 || $type == 7 || $type == 9) 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
            /*if($action->id != 'view' && $action->id != 'list' && $action->id != 'change') return $this->redirect(['/site/errors', 'type' => 0 ]);*/
        }

        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }
    /**
     * Lists all Tender models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModelWorking = new TenderSearch();
        $dataProviderWorking = $searchModelWorking->search(Yii::$app->request->queryParams);
        $dataProviderWorking->query->andWhere(['status' => Tender::STATUS_WORKING]);

        $searchModelWait = new TenderSearch();
        $dataProviderWait = $searchModelWait->search(Yii::$app->request->queryParams);
        $dataProviderWait->query->andWhere(['status' => Tender::STATUS_WAIT]);

        $searchModelWon = new TenderSearch();
        $dataProviderWon = $searchModelWon->search(Yii::$app->request->queryParams);
        $dataProviderWon->query->andWhere(['status' => Tender::STATUS_WON]);

        $searchModelArchive = new TenderSearch();
        $dataProviderArchive = $searchModelArchive->search(Yii::$app->request->queryParams);
        $dataProviderArchive->query->andWhere(['status' => Tender::STATUS_ARCHIVE]);

        return $this->render('index', [
            'searchModelWorking' => $searchModelWorking,
            'dataProviderWorking' => $dataProviderWorking,
            'searchModelWait' => $searchModelWait,
            'dataProviderWait' => $dataProviderWait,
            'searchModelWon' => $searchModelWon,
            'dataProviderWon' => $dataProviderWon,
            'searchModelArchive' => $searchModelArchive,
            'dataProviderArchive' => $dataProviderArchive,
        ]);
    }


    /**
     * Displays a single Tender model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $filesSearchModel = new TenderFileSearch();
        $filesDataProvider = $filesSearchModel->search([]);
        $filesDataProvider->query->andWhere(['tender_id' => $id]);
        $filesDataProvider->pagination = false;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Тендера ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'filesSearchModel' => $filesSearchModel,
                        'filesDataProvider' => $filesDataProvider,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'filesSearchModel' => $filesSearchModel,
                'filesDataProvider' => $filesDataProvider,
            ]);
        }
    }
    /**
     * Creates a new Tender model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($client_id)
    {
        $request = Yii::$app->request;
        $model = new Tender();  
        $model->client_id = $client_id; 
            
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>'#tender-datatable-pjax',
                'size' => 'normal',
                'title'=> "Тендера",
                'content'=>'<span class="text-success">Успешно добавлено</span>',
                'footer'=> Html::button('Ок',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['/tender/add', 'client_id' => $client_id],['class'=>'btn btn save','role'=>'modal-remote'])        
            ];         
        }else{           
            return [
                'title'=> "Добавить",
                'size' => 'large',
                'content'=>$this->renderAjax('/tender/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])        
            ];         
        }     
    }
    /**
     * Updates an existing Tender model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateTender($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return ['forceClose'=>true,'forceReload'=>'#tender-datatable-pjax'];
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('/tender/add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
            ];        
        }
    }
    /**
     * Delete an existing Tender model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTender($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#tender-datatable-pjax'];
        }
    }

    public function actionCreate1($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload'=>'#parameter-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать",
                'size' => 'large',
                'content'=>$this->renderAjax('_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn otmena btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn save btn-sm','type'=>"submit"])
            ];
        }
       
    }

    public function actionUploadFile($tender_id)
    {
        $fileName = Yii::$app->security->generateRandomString();
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }
        $uploadPath = 'uploads';

        if(isset($_FILES['file'])) {
            $file = UploadedFile::getInstanceByName('file');
            $path = $uploadPath.'/'.$fileName.'.'.$file->extension;

            if($file->saveAs($path)) {
                $tenderFile = new TenderFile([
                    'tender_id' => $tender_id,
                    'name' => $_FILES['file']['name'],
                    'path' => $path
                ]);
                $tenderFile->save(false);

                return Json::encode($file);
            }
        }

        return false;
    }

    public function actionDownloadFile($id)
    {
        $tenderFile = TenderFile::findOne($id);

        if($tenderFile == null){
            throw new NotFoundHttpException();
        }

        if(file_exists($tenderFile->path)){
            Yii::$app->response->sendFile($tenderFile->path, $tenderFile->name);
        }
    }

    /**
     * Creates a new Tender model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($pjaxContainer = null)
    {
        $request = Yii::$app->request;
        $model = new Tender();  

        if($pjaxContainer == null){
            $pjaxContainer = '#crud-datatable-pjax';
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$pjaxContainer,
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Создания тендера успешно завершено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
                ];         
            }else{
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Tender model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $pjaxContainer = null)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($pjaxContainer == null){
            $pjaxContainer = '#crud-datatable-pjax';
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>$pjaxContainer,
                    'title'=> "Тендера ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Tender model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $pjaxContainer = null)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($pjaxContainer == null){
            $pjaxContainer = '#crud-datatable-pjax';
        }


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>$pjaxContainer];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Tender model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tender the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tender::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
