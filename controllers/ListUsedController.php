<?php

namespace app\controllers;

use Yii;
use app\models\ListUsed;
use app\models\ListUsedSearch;
use yii\web\Controller; 
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use \yii\web\Response; 
use yii\helpers\Html;  

/**
 * ListUsedController implements the CRUD actions for ListUsed model.
 */
class ListUsedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if($type == 5 || $type == 7 ||$type == 9 || $type == 10) 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);
    }

    /**
     * Lists all ListUsed models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ListUsedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
           // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ListUsed model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Список б/у ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    //Просмотр картинку
    public function actionViewImg($id)
    {   
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;

        return [
            //'title'=> '<h4 style="color:red;font-weight:bold;"><center>Цель звонка</center></h4>',
            'title'=> "Фото оборудования",
            'size'=>'normal',
            'content'=> $this->renderAjax('view_img', [
                'model' => $this->findModel($id),
            ]),
        ];
    }

    //Просмотр причина звонка
    public function actionShowPurposeCall($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        return [
            'title'=> "Описание",
            'size'=>'normal',
            'content'=> '<div class="table-responsive" style="font-size:16px; margin-left:15px;">'.$model->description.'</div>',
        ];
    }

    //Загрузить файл на сервер
    public function upload()
    {
        if ($this->validate()) {
            $this->photo_equipment->saveAs('uploads/' . $this->photo_equipment->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }

    //Скачать файл из сервера
    public function actionSendFile($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }
    
    /**
     * Creates a new ListUsed model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ListUsed();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->validate() ){

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->photo_equipment = $imageName;
                $model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn save','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {
                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->photo_equipment = $imageName;
                $model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }
    /**
     * Updates an existing ListUsed model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->validate() ){

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->photo_equipment = $imageName;
                $model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Список б/у ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn save','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить ",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn otmena pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn save','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->validate()) {

                $model->other_file = UploadedFile::getInstance($model, 'other_file');
                $imageName = $model->other_file->baseName . '.' . $model->other_file->extension;


                if (!empty($model->other_file)) $model->photo_equipment = $imageName;
                $model->save();

                if (!empty($model->other_file)) $model->other_file->saveAs('uploads/' . $imageName);
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing ListUsed model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = ListUsed::findOne($id);
        $imageName = $model->photo_equipment;
        Yii::$app->db->createCommand()->delete('list_used', ['id' => $id])->execute();
        unlink(getcwd().'/uploads/'.$imageName);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the ListUsed model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ListUsed the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ListUsed::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
