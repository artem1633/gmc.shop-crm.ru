<?php

namespace app\components;

use yii\helpers\VarDumper;

/**
 * Class DadataInnSearch
 * @package app\components
 */
class DadataInnSearch
{
    /**
     * @param string $inn
     */
    public static function search($inn)
    {
        $ch = curl_init('https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Token 97522f145e718723e3c40fa7aac9fb4614ead96f',
        ]);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{ "query": "'.$inn.'" }');

        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, true);
    }
}